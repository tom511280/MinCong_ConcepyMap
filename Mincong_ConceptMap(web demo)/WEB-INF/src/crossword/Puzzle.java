package crossword;

import java.util.ArrayList;

public class Puzzle {
	public ArrayList<String> words;
	public ArrayList<String> clues;
	public ArrayList<Point> points;
	public char[][] char_puzzle;
	public double areaWeight = 0.8;
	public double ratioWeight = 0.2;
	public double solidWeight = -0.2;
	
	public Puzzle(ArrayList<String> words, ArrayList<String> clues, ArrayList<Point> points) {
		this.words = words;
		this.clues = clues;
		this.points = points;
	}
	
	public Point calOrigin() {
		Point origin = new Point(0,0,0);
		for(int i=0; i<points.size(); i++) {
			origin.x = (origin.x > points.get(i).x) ? points.get(i).x : origin.x;
			origin.y = (origin.y > points.get(i).y) ? points.get(i).y : origin.y;
		}
		//System.out.println("Origin="+origin);
		return origin;
	}
	
	public void shiftToOrigin() {
		Point origin = this.calOrigin();
		//System.out.println("prevPoints="+points);
		for(Point point: points) {
			point.x = point.x - origin.x;
			point.y = point.y - origin.y;
		}
		//System.out.println("afterPoints="+points);
	}
	
	public int calWidth() {
		int maxWidth=0;
		for(int i=0; i<points.size(); i++) {
			int right=points.get(i).x;
			if(points.get(i).axis==0) {
				right = right + words.get(i).length();
			}
			maxWidth = (maxWidth < right) ? right : maxWidth;
		}
		//System.out.println("Width="+maxWidth);
		return maxWidth+1;
	}
	
	public int calHeight() {
		int maxHeight=0;
		for(int i=0; i<points.size(); i++) {
			int buttom=points.get(i).y;
			if(points.get(i).axis==1) {
				buttom = buttom + words.get(i).length();
			}
			maxHeight = (maxHeight < buttom) ? buttom : maxHeight;
		}
		//System.out.println("Height="+maxHeight);
		return maxHeight+1;	
	}
	
	public int calArea() {
		return calWidth() * calHeight();
	}
	
	public double calRatio() {
		if(calHeight()==0 || calWidth()==0)
			System.out.println("ERROR: one of Width or Heigth is 0 !!");
		return (calWidth() < calHeight()) ? calWidth()/(double)calHeight() : calHeight()/(double)calWidth();
	}
	
	public int calSolid() {
		int solidNum = 0;
		for(int y=0;y<char_puzzle[0].length-1;y++) {
			for(int x=0;x<char_puzzle.length-1;x++) {
				if(char_puzzle[x][y]!='~' && char_puzzle[x+1][y]!='~' && char_puzzle[x][y+1]!='~' && char_puzzle[x+1][y+1]!='~')
					solidNum++;
			}
		}
		return solidNum;
	}
	
	public double calScore() {
		double areaScore = 15*15/(double)calArea() * areaWeight;
		double ratioScore = calRatio()*ratioWeight;
		double solidMinus = calSolid()*solidWeight;
		return 100*(areaScore+ratioScore+solidMinus);
	}
	
	public void generateCharPuzzle() {
		shiftToOrigin();
		if(char_puzzle==null) {
			char_puzzle = new char[calWidth()][calHeight()];
			for(int y=0;y<char_puzzle[0].length;y++) {
				for(int x=0;x<char_puzzle.length;x++)
					char_puzzle[x][y] = '~';
			}
			//System.out.println("===========");
			//System.out.println(this.words);
			//System.out.println(this.points);
			//System.out.println(char_puzzle.length+" "+char_puzzle[0].length);
			
			
			for(int i=0;i<words.size();i++) {
				String word = words.get(i);
				Point point = points.get(i);
				//System.out.println(word+" "+point);
				if(point.axis==0) {
					for(int j=0;j<word.length();j++)
						char_puzzle[point.x+j][point.y] = word.charAt(j);
				}
				
				else if(point.axis==1) {
					for(int j=0;j<word.length();j++)
						char_puzzle[point.x][point.y+j] = word.charAt(j);
				}
			}
		}
	}
	
	public String toString() {
		
		String str = "";
		//str = str + "word set= " + words + "\n" ;
		//str = str + "point set= " + points + "\n" ;
		
		for(int y=0;y<char_puzzle[0].length;y++) {
			for(int x=0;x<char_puzzle.length;x++) {
				str = str + char_puzzle[x][y] + " ";
			}
			str = str + "\n";
		}
		return str;
	}
}
