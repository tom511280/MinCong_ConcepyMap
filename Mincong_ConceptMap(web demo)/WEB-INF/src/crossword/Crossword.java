package crossword;
import java.util.ArrayList;
import java.util.Calendar;

public class Crossword {

	public ArrayList<String> word_list;
	public ArrayList<String> clue_list;
	public int total_words;
	public int limit = 1;
	public Puzzle puzzle;
	
	public Crossword(ArrayList<String> word_list, ArrayList<String> clue_list) {
		
		this.word_list = word_list;
		this.clue_list = clue_list;
		this.total_words = word_list.size();
		
		Puzzle bestPuzzle = createRandomPuzzle(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<Point>(), 0);
		bestPuzzle.generateCharPuzzle();
		int num = 0;
		long start_time = Calendar.getInstance().getTimeInMillis();
		while(Calendar.getInstance().getTimeInMillis()-start_time <= limit*1000) {
			// 開始產生 puzzle
			Puzzle puzzle = createRandomPuzzle(new ArrayList<String>(), new ArrayList<String>(), new ArrayList<Point>(), 0);
			puzzle.generateCharPuzzle();
			if(puzzle.calScore()>bestPuzzle.calScore()) {
				bestPuzzle = puzzle;
			}
			num++;
		}
		System.out.println(num);
		this.puzzle = bestPuzzle;
		System.out.println("=== BEST  PUZZLE ===");
		System.out.println(puzzle);
		//System.out.println(bestPuzzle.calSolid());
		//System.out.println(bestPuzzle.calSolid()*bestPuzzle.solidWeight);
		//System.out.println(bestPuzzle.calScore(minArea));
		//System.out.println(score);
	}
	
	@SuppressWarnings("unchecked")
	public Puzzle createRandomPuzzle(ArrayList<String> words, ArrayList<String> clues, ArrayList<Point> points, int index) {
		
		// 遞迴結束狀態: index 超過總共單字
		if(index>=total_words) {
			return new Puzzle(words, clues, points);
		}
		
		// 先找到該字在  puzzle 的交叉點
		String word = word_list.get(index);
		String clue = clue_list.get(index);
		ArrayList<Point> candidatePoints = findCandidatePoints(word, words, points);
		//System.out.println("word="+word+" candidatePoints="+candidatePoints);
		ArrayList<Point> tempPoints = (ArrayList<Point>)candidatePoints.clone();
		// 確定每個 candidate points 可以放入 puzzle
		for(Point point: tempPoints) {
			if(!canPutWord(word, point, words, points))
				candidatePoints.remove(point);
		}
		
		// 如果還有points可以造成cross, 隨機選一個去加入
		if(candidatePoints.size()>0) {
			int random = (int) (candidatePoints.size()*Math.random());
			points.add(candidatePoints.get(random));
		}	
		
		// 假如沒有任何交叉點, 隨機插在一個地方, 會往原點向外擴張尋找
		else {
			Point origin = new Point(0,0,(int) (2*Math.random())); //若為第一個字, 就會塞在原點
			Point randomPoint = randomPutWord(word, origin, words, points);
			points.add(randomPoint);
		}
		words.add(word);
		clues.add(clue);
		return createRandomPuzzle(words, clues, points, index+1);
	}
	
	private Point randomPutWord(String target_word, Point target_point, ArrayList<String> words, ArrayList<Point> points) {
		if(canPutWord(target_word, target_point, words, points)) {
			//System.out.println("random put word: "+target_word);
			//System.out.println("current words: "+words);
			return target_point;
		}
		else {
			// 四個方位, 隨機選一個去試
			int x = target_point.x, y = target_point.y, axis = target_point.axis;
			Point[] randomPoints = {
					new Point(x+1,y,axis),
					new Point(x-1,y,axis),
					new Point(x,y+1,axis),
					new Point(x,y-1,axis)};
			return randomPutWord(target_word, randomPoints[(int) (4*Math.random())], words, points);
		}
	}

	/**
	 * 找到該字在 puzzle 所有的交叉點
	 */
	public ArrayList<Point> findCandidatePoints(String target, ArrayList<String> words, ArrayList<Point> points) {
		ArrayList<Point> result = new ArrayList<Point>();
		//System.out.println("find words="+words);
		//System.out.println("find points="+points);
		
		for(int i=0;i<words.size();i++) {
			String word = words.get(i);
			Point point = points.get(i);
			
			for(int j=0;j<target.length();j++) {
				char letter = target.charAt(j);
				
				for(int k=0;k<word.length();k++) {
					if(word.charAt(k)==letter) {
						//System.out.println("Intersects!! word="+word+" target="+target+" letter="+letter);
						int newX=-1, newY=-1, newAxis=-1;
						if(point.axis==0) {
							newX = point.x + k;
							newY = point.y - j;
							newAxis = 1;
						}
						else if(point.axis==1) {
							newX = point.x - j;
							newY = point.y + k;
							newAxis = 0;
						}
						Point newPoint = new Point(newX, newY, newAxis);
						//System.out.println("Point="+newPoint);
						result.add(newPoint);
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * 是否可以在  puzzle 裡加入新字
	 */
	public boolean canPutWord(String target, Point target_point, ArrayList<String> words, ArrayList<Point> points) {
		// 對於每個在 puzzle 裡的字, 如果發生 overlap 而寫字母不一樣, 則回傳 false
		//System.out.println("word="+target+", point="+target_point);
		//System.out.println("words="+words+", points="+points);
		for(int i=0;i<words.size();i++) {
			String word = words.get(i);
			Point point = points.get(i);
			if(!isSameLetterOnOverlap(target, target_point, word, point))
				return false;
			if(isHeadOnOverlap(target, target_point, word, point))
				return false;
			if(isSameAxisConcatenation(target, target_point, word, point))
				return false;
		}
		return true;
	}



	/**
	 * 是否在 overlap 處有同樣的字母
	 */
	private boolean isSameLetterOnOverlap(String word1, Point point1, String word2, Point point2) {
		//System.out.println("word1="+word1+", point1="+point1);
		//System.out.println("word2="+word2+", point2="+point2);
		for(int i=0;i<word1.length();i++) {
			for(int j=0;j<word2.length();j++) {
				int x1 = (point1.axis==0) ? point1.x + i : point1.x;
				int y1 = (point1.axis==1) ? point1.y + i : point1.y;
				int x2 = (point2.axis==0) ? point2.x + j : point2.x;
				int y2 = (point2.axis==1) ? point2.y + j : point2.y;
				//System.out.println("x1="+x1+", y1="+y1);
				//System.out.println("x2="+x2+", y2="+y2);
				//System.out.println("char1="+word1.charAt(i)+", char2="+word2.charAt(j));
				if(x1==x2 && y1==y2 && word1.charAt(i) != word2.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * 是否有同樣 head
	 */
	private boolean isHeadOnOverlap(String word1, Point point1, String word2, Point point2) {
		if(point1.x==point2.x && point1.y==point2.y)
			return true;
		return false;
	}
	
	/**
	 * 是否在同方向會發生串接字 (會造成 puzzle 混亂)
	 */
	private boolean isSameAxisConcatenation(String word1, Point point1, String word2, Point point2) {
		//System.out.println("word1="+word1+", point1="+point1);
		//System.out.println("word2="+word2+", point2="+point2);
		
		//不一樣方向就不會串接在一起, 
		if(point1.axis!=point2.axis)
			return false;
		
		//相同方向時, 若 x,y軸均不一樣, 也不會串接在一起
		if(point1.axis==point2.axis && point1.x!=point2.x && point1.y!=point2.y)
			return false;
		
		//兩個字都是水平
		if(point1.axis==0) {
			/* 目標字的第一個字母 == 現有字的最後一個字母或最後一個字母後方,
			 * 相反的,
			 * 現有字的第一個字母 == 目標字的最後一個字母或最後一個字母後方 
			*/
			if(point1.x==point2.x+word2.length() || point1.x==point2.x+word2.length()+1 ||
					point2.x==point1.x+word1.length() || point2.x==point1.x+word1.length()+1
					)
				return true;
		}
			
			
		//兩個字都是鉛直
		else if(point1.axis==1) {
			if(point1.y==point2.y+word2.length() || point1.y==point2.y+word2.length()+1 ||
					point2.y==point1.y+word1.length() || point2.y==point1.y+word1.length()+1
					)
				return true;
		}

		//兩個都在相同方向, 也在同一軸上, 但是沒發生串接
		return false;
	}
}
