package crossword;
import java.util.Comparator;

public class NameLengthComparator implements Comparator<String> {
	public int compare(String stra, String strb) {
    	int lena = stra.length();
    	int lenb = strb.length();
    	return ( lena >= lenb ) ? -1 : 1;
    }
}
