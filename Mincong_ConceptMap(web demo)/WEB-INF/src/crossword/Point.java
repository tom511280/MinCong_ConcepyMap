package crossword;

public class Point implements Cloneable{
	public int x;
	public int y;
	public int axis;
	
	public Point(int x, int y, int axis) {
		this.x = x;
		this.y = y;
		this.axis = axis;
	}
	
	public String toString() {
		String str = "("+x+","+y+","+ ((axis==0) ? "-" : "|") +")";
		return str;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
