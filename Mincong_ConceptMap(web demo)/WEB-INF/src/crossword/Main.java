package crossword;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
	
	//public static ArrayList<String> word_list = new ArrayList<String>(Arrays.asList("through", "high", "hair", "stares", "sea", "scent", "meet", "pair", "there", "heel", "right", "too", "blue", "close", "knew"));
	//public static ArrayList<String> clue_list = new ArrayList<String>(Arrays.asList("through", "high", "hair", "stares", "sea", "scent", "meet", "pair", "there", "heel", "right", "too", "blue", "close", "knew"));
	public static ArrayList<String> word_list = new ArrayList<String>(Arrays.asList("ambulance", "development", "spokesman", "information", "dispatch", "sports", "hospital", "several", "golfer", "record", "influence", "blood", "defense", "magazine", "publish"));
	public static ArrayList<String> clue_list = new ArrayList<String>(Arrays.asList("ambulance", "development", "spokesman", "information", "dispatch", "sports", "hospital", "several", "golfer", "record", "influence", "blood", "defense", "magazine", "publish"));
	
	public static void main(String[] args) {
		
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<String> clues = new ArrayList<String>();
		while(words.size()<10) {
			int ranNum = (int) (10*Math.random());
			if(!words.contains(word_list.get(ranNum))) {
				words.add(word_list.get(ranNum));
				clues.add(clue_list.get(ranNum));
			}
		}
		System.out.println(words);
		Crossword crossword = new Crossword(words, clues);
	}
}
