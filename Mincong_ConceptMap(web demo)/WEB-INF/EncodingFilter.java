import java.io.IOException;
import javax.servlet.Filter;   
import javax.servlet.FilterChain;   
import javax.servlet.FilterConfig;   
import javax.servlet.ServletException;   
import javax.servlet.ServletRequest;   
import javax.servlet.ServletResponse;   
  

public class EncodingFilter implements Filter {   
  
    public void init(FilterConfig arg0) throws ServletException {   
       // throw new UnsupportedOperationException("Not supported yet.");   
    }   
  
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {   
//        throw new UnsupportedOperationException("Not supported yet.");   
          request.setCharacterEncoding("UTF-8");   
	  chain.doFilter(request, response);    
    }   
  
    public void destroy() {   
      //  throw new UnsupportedOperationException("Not supported yet.");   
    }   
    public static void main(String[] arg){
    }
   
} 