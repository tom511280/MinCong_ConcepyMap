<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>無標題文件</title>
	<!-- include the Tools --> 
		<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> 
	<!-- standalone page styling (can be removed) --> 
		<link rel="stylesheet" type="text/css" href="http://static.flowplayer.org/tools/css/standalone.css"/>	
	<!-- same styling as in minimal setup --> 
		<link rel="stylesheet" type="text/css" href="http://static.flowplayer.org/tools/demos/validator/css/form.css"/> 
	<!-- override it to have a columned layout --> 
		<link rel="stylesheet" type="text/css" href="http://static.flowplayer.org/tools/demos/validator/css/columns.css"/>
<style>
/* pure CSS arrow */
.error em {
	display:block;
	width:0;
	height:0;
	border:10px solid;
	border-color:#FFFE36 transparent transparent;

	/* positioning */
	position:absolute;
	bottom:-17px;
	left:60px;
}
</style>
</head>

<body>

    <form id="myform" class="cols"> 
	<h3>HTML5 form with custom inputs</h3> 
	<fieldset> 
		<label> email * <input type="email" required="required" minlength="9" /> </label> 
		<label> username * <input type="text" name="username" minlength="5" /> </label> 
		<label> Password <input type="password" name="password" minlength="4" /> </label> 
		<label> Password check <input type="password" name="check" data-equals="password" /> </label> 
	</fieldset>	
	<fieldset> 
		<label> website * <input type="url" required="required" /> </label> 
		<label> name * <input type="text" name="name" required="required" maxlength="30" /> </label>  
		<label> age <input type="number" name="age" size="4" maxlength="3" /> </label> 
		<label> time <input type="time" name="time" maxlength="8" /> </label> 
	</fieldset> 
	<div class="clear"></div> 
	<button type="submit">Submit form</button> 
	
	</form> 

<script> 
$("#myform").validator({ 
		position: 'top left', 
		offset: [-12, 0],
		message: '<div><em/></div>' // em element is the arrow
		});
	// Regular Expression to test whether the value is valid
	$.tools.validator.fn("[type=time]", "Please supply a valid time", function(input, value) { 
		return /^\d\d:\d\d$/.test(value);
		});
	$.tools.validator.fn("[data-equals]", "Value not equal with the $1 field", function(input) {
	    var name = input.attr("data-equals"),
		field = this.getInputs().filter("[name=" + name + "]"); 
	    return input.val() == field.val() ? true : [name]; 
	    });
    $.tools.validator.fn("[minlength]", function(input, value) {
		var min = input.attr("minlength");
		return value.length >= min ? true : {     
		en: "Please provide at least " +min+ " character" + (min > 1 ? "s" : ""),
		fi: "Kentän minimipituus on " +min+ " merkkiä" };
		});
	$.tools.validator.localizeFn("[type=time]", {
		en: 'Please supply a valid time',
		fi: 'Virheellinen aika'		
		});
	
</script> 
 
</body>
</html>