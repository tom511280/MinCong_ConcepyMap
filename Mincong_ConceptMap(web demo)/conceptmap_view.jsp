<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.io.File" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Concept Map</title>
<%! 
	public int fileNum(File f, String article_id, String realPath){
		int size = 0; int num = 0;		
		File flist[] = f.listFiles();
		size=flist.length;
		String[] fileName = new String[size];
		for (int i = 0; i < flist.length; i++) {
			fileName[i] = flist[i].toString().replace(realPath,"");			
		}
		
		for (int i = 0; i < fileName.length; i++) {
			if (fileName[i].indexOf(article_id)>=0) {
				num++;
			}
		}
		return num;
	 }
  %>
<style type="text/css">
iframe#iframe1{
background-color:#ffcc99;
}
</style>
<link rel="stylesheet" href="css/jqpagination.css" />
<script src="jquery/jquery-1.7.2.min.js"></script>
<script src="jquery/jquery.jqpagination.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script language="javascript"> 
var times=6;
function clock() 
{ 
	window.setTimeout('clock()',1000);//在執行时,它從载入後,每隔指定的時間就執行一次表達示
	times=times-1; 
	time.innerHTML ="( "+times+" 秒)";
	if(times==0)
		window.close();
}
 
function PageNum(page) 
{ 
	if(page<1)
		page = 1;
	else if(page>$("#fileNum").val())
		page = $("#fileNum").val();
	$('#pageNum').val(page);
	form1.submit();
} 
</script>
</head>
<body>
<center>
<%
String title = request.getParameter("title");
    //String title = "199901022";
	//out.print("<center>");
	String root1 = title+"\\"+title+"_conceptMap.pdf";
	
	
	File file = new File(application.getRealPath("/")+"conceptMap"+"\\"+title);

    int fileNum = 0;
	String realPath = application.getRealPath("/")+"ConceptMap\\"+title+"\\";
	if (file.isDirectory()) { //如果路徑是資料夾的時候
		fileNum = fileNum(file,title,realPath);
	}

	int pageNum = 1;
	if(request.getParameter("pageNum")!=null){
		pageNum = Integer.parseInt(request.getParameter("pageNum"));					
	}	
	String root2 = title+"\\"+title+"_conceptMap-"+pageNum+".pdf";//
	


	String location=getServletContext().getRealPath("/")+"ConceptMap\\";
		

	//得到web應用的根路徑
	File f1 = new File(location+root1);
	if(f1.exists()){
		//out.print("yes");	
%>
<iframe id="iframe1" src="ConceptMap\<%=root1%>" frameborder="0" width="780" height="720"></iframe>
<%
	}else if(fileNum>1){	
%>
<iframe id="iframe1" src="ConceptMap\<%=root2%>" frameborder="0" width="780" height="720"></iframe>
<input id="fileNum" name="fileNum" type="hidden" value="<%=fileNum%>" />
<form action="conceptmap_view.jsp" method="get" id="form1">
<input id="title" name="title" type="hidden" value="<%=title%>" />
<input id="pageNum" name="pageNum" type="hidden" value="<%=pageNum%>" />
</form>
<div class="gigantic pagination" >
	<a href='javascript:void(0)' OnClick='PageNum(<%=1%>)' class="first" data-action="first">&laquo;</a>
	<a href='javascript:void(0)' OnClick='PageNum(<%=pageNum-1%>)' class="previous" data-action="previous">&lsaquo;</a>
	<input name="page" type="text" width="780" id="page" value="Page <%=pageNum%> of <%=fileNum%>" readonly />
	<a href='javascript:void(0)' OnClick='PageNum(<%=pageNum+1%>)' class="next" data-action="next">&rsaquo;</a>
	<a href='javascript:void(0)' OnClick='PageNum(<%=fileNum%>)' class="last" data-action="last">&raquo;</a>
</div>
<%	
	}else{		
		out.print("<h2><span style=\"color: red\">此文章尚未建立concept map.</span></h2>");
		out.print("<span style=\"color: blue\">5秒鐘後網頁自動關閉。</span><br>");
		
	%>
<script>clock();</script>
    <div id= "time" style="color:blue">( 5 秒)</div>
	<%
	}
		//out.print("</center>");	
%>
</center>
</body>
</html>