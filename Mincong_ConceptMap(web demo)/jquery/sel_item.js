
var oLearnLogSelItem = {};
	
$(function (){
	
	$(document).ready(function(){
	   
        $("#cp_learn_log_panel").tabs();
       
        $('#cp_to_search').table_image_link();
        $('#cp_to_back').table_image_link();
		
		//預設全選
		//oLearnLogSelItem.sel_checkbox(document.getElementById("cp_sel_all"), Array(), true);
	});			
	
	function sel_checkbox(oObj, aSel, bIsDef)
	{
		if (bIsDef)
		{
			oObj.checked = true;
		}
		
		if (aSel.length > 0)
		{
			for (var i = 0; i < aSel.length; i++)
			{
				$("#cp_log_id_" + aSel[i]).prop("checked", oObj.checked);
			}
		}
		else
		{
			//如果傳入陣列是空的話,則代表全選

			$("input[id^='cp_log_id_']").each( function (i, selected) {
	
				$(selected).prop("checked", oObj.checked);
			});		
			
			$("input[value='group_xx']").each( function (i, selected) {
			
				$(selected).prop("checked", oObj.checked);
			});						
		}
	}
	
	function to_trace_log()
	{
		var sStartDate = $.trim ($("#during_start").val());
		var sEndDate = $.trim ($("#during_end").val());
		var aSelItem = Array();
		
		var dUid = $("#cp_uid").val();
		var dCid = $("#cp_cid").val();

		if (dUid != null && dCid > 0)
		{
			//檢查日期是否正確
			
			if (sStartDate != "")
			{
				sStartDate += " 00:00:00";
			}
			
			if (sEndDate != "")
			{
				sEndDate += " 23:59:59";
			}
			
			//if (sStartDate == "" || sEndDate == "" || Date.parse(sEndDate.replace("-", "/")) >= Date.parse(sStartDate.replace("-", "/")))
			if (sStartDate == "" || sEndDate == "" || Date.parse(sEndDate.replace("-", "/")).valueOf() >= Date.parse(sStartDate.replace("-", "/")).valueOf())
			{
				//檢查歷程項目勾選是否正確
			
				$("input[id^='cp_log_id_']").each( function (i, selected) {
				
					if ($(selected).prop("checked"))
					{
						aSelItem.push($(selected).attr("value"));
					}
				});		
			
				if (aSelItem.length > 0)
				{
//					document.location = "home.php?func=RM051&cid=" + dCid + "&uid=" + dUid + "&startdate=" + encodeURIComponent(sStartDate) + "&enddate=" + encodeURIComponent(sEndDate) + "&items=" + encodeURIComponent(aSelItem.join(","));
					document.location = "trace_result_statistics_index_tsai.jsp?cid=" + dCid + "&uid=" + dUid + "&startdate=" + sStartDate + "&enddate=" + sEndDate + "&items=" + aSelItem;
					//alert(aSelItem);
				}
				else
				{
					alert ("請至少選擇一項歷程項目!");	
				}
			}
			else
			{
				alert ("請確認時間範圍!");	
			}
		}
		else
		{
			alert ("cid, uid 指定錯誤!請重新選擇班級與學生!");	
		}
	}
	
	oLearnLogSelItem.to_trace_log = to_trace_log;
	oLearnLogSelItem.sel_checkbox = sel_checkbox;
	
})();