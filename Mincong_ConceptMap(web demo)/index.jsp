<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Topic ConceptMap Create</title>
</head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
//進入畫初始使設定
$(document).ready(function() { 
$("#waiting").hide();
});


//$("#waiting").hide();
function detectclick(){
	//var title = $("#title").val();
	//var title = Math.random();
	var title = Math.floor(Math.random() * 100000) + 1;
	var content = $("#content").val();
	
	$("#waiting").show();
	var create_information = 
	{"title":title,
	 "content":content};
	 
			$.ajax({
			type: "post",
			url: 'create.jsp',
			data:create_information,
			dataType : "json", 
			error: function(error){
				alert(error);
			},
			success: function(data){
				var Status = data.Status;
				$("#waiting").hide();
				alert(data.Status);
				if(data.Status=="success")
				window.open('conceptmap_view.jsp?title='+title,'Concept Map','width=850,height=850,resizable=no,toolbar=no,Status= no,scrollbars=yes,Menubar= no,Location= no;');
				
			}
	});
}
</script>

<!--java bean-->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<link href="dist/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="dist/js/bootstrap.min.js"></script>
<body>
<center>
<div class="page-header">
<h1>Topic ConceptMap Create</h1>
</div>
<div class="panel panel-primary" style="width:1000px;height:800px">
  <div class="panel-heading">
    <h3 class="panel-title">Input</h3>
  </div>
  <div class="panel-body">
<form class="form-inline" id="form1" name="form1" method="post" action=""> 
<!--<input name="title"  id="title" type="text" style="width:900px" class="form-control" placeholder="Title">-->
<p>&nbsp;</p>
<textarea name="content" id="content" class="form-control" rows="3" style="width:900px;height:440px" placeholder="Content"></textarea>  
<p>&nbsp;</p>


<button name="Start" id="Start" type="button" class="btn btn-success"  / onclick="detectclick();"/>Create</button>&nbsp;
<input type="reset" value="Clean" class="btn btn-danger"/>




<p>&nbsp;</p>
<div id="waiting" name="waiting" class="alert alert-warning" role="alert">
<div class="progress" style="width:300px">
  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
    <span class="sr-only">100% Complete</span>
  </div>
</div>
Please waiting some time...</div>
<p>&nbsp;</p>

</form>
</div>
</div>


Copyright © 2015 Min-Cong Wu.

</center>
</body>
</html>