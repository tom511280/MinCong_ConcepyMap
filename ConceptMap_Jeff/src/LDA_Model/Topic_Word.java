package LDA_Model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class Topic_Word extends Probability_Distributed{
	@SuppressWarnings("unchecked")
	public Topic_Word(String File_Path){
		super(File_Path);
		Probability = new ArrayList<HashMap<String, Double>>();
		if(document.exists()){
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"MS950"));
				String text = null;
				int topic_count = 0;
				//HashMap<String,Double> each_topic= new HashMap<String,Double>();	
				while ((text = reader.readLine()) != null) {//readLine()方法會在讀取到使用者的換行字元時，再一次將整行字串傳入。
					if(text.equals("Topic "+String.valueOf(topic_count)+"th:"))
						{
						    HashMap<String, Double> single_topic = new HashMap<String, Double>();
						    ((ArrayList<HashMap<String, Double>>) Probability).add(single_topic);
						  topic_count++;
						}
						else
						{
						  String[] tmp = text.replace("’", "'").trim().split(" ");
						  if(tmp.length>1)//避免空白
							  ((ArrayList<HashMap<String, Double>>) Probability).get(topic_count-1).put(tmp[0].trim(), Double.valueOf(tmp[1]));
						}
					
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
	
			} catch (IOException e) {
				System.out.println("檔案路徑錯誤！");
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
