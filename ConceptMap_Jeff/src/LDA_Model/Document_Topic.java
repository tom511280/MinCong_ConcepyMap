package LDA_Model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Document_Topic extends Probability_Distributed{
	@SuppressWarnings("unchecked")
	public Document_Topic(String File_Path){
		super(File_Path);
		Probability = new HashMap<Integer,ArrayList<Double>>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"MS950"));
			String text = null;

			int count = 0;
			while ((text = reader.readLine()) != null) {
				ArrayList<Double>single_article_topic = new ArrayList<Double>();
				String[]tmp = text.split(" ");
				for(int i = 0;i < tmp.length;i++)
				{
					single_article_topic.add(Double.valueOf(tmp[i]));
				}
				count++;
				((HashMap<Integer, ArrayList<Double>>) Probability).put(count,single_article_topic);
			}
				
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void getTopicOrder(){//放置文件內主題隸屬程度的排序 ，放置在陣列越前面，代表主題對文章的隸屬機率越大
		
		
//		
//		ArrayList<Entry<String, Double>> arrayList = new ArrayList<Map.Entry<String,Double>>(hash.entrySet());
//		Collections.sort(arrayList, new Comparator<Map.Entry<String, Double>>(){
//		    public int compare(Map.Entry<String, Double> map1, Map.Entry<String,Double> map2) {
//		        return ((map2.getValue() - map1.getValue() == 0) ? 0 : (map2.getValue() - map1.getValue() > 0) ? 1 : -1);
//		    }
//		});  
//		  for (Entry<String, Double> entry : arrayList) 
//		  {
//			  Sresult.add(entry.getKey());
//			  Dresult.add(entry.getValue());
//		  }
		
		HashMap<Integer,ArrayList<Integer>> Tatal_orderhash = new HashMap<Integer,ArrayList<Integer>>();
		for(int i : ((HashMap<Integer, ArrayList<Double>>) Probability).keySet()){
			ArrayList<Double> each_document = ((HashMap<Integer, ArrayList<Double>>) Probability).get(i);
			HashMap<Integer,Double> hash = new HashMap<Integer,Double>();
			ArrayList<Integer> order = new ArrayList<Integer>();
			ArrayList<Double> value = new ArrayList<Double>();
			
			for(int j = 1;j < each_document.size();j++){
				hash.put(j,each_document.get(j));
			}
			ArrayList<Entry<Integer, Double>> arrayList = new ArrayList<Map.Entry<Integer,Double>>(hash.entrySet());
			Collections.sort(arrayList, new Comparator<Map.Entry<Integer, Double>>(){
			    public int compare(Map.Entry<Integer, Double> map1, Map.Entry<Integer,Double> map2) {
			        return ((map2.getValue() - map1.getValue() == 0) ? 0 : (map2.getValue() - map1.getValue() > 0) ? 1 : -1);
			    }
			});  
			  for (Entry<Integer, Double> entry : arrayList) 
			  {
				  order.add(entry.getKey());
				  value.add(entry.getValue());
//				  Sresult.add(entry.getKey());
//				  Dresult.add(entry.getValue());
			  }
			System.out.println(order);
			System.out.println(value);
			Tatal_orderhash.put(i, order);
		}
	}
}
