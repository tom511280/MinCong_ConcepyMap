package LDA_Model;

import java.io.File;

public abstract class Probability_Distributed<T> {
	public T Probability;
	public File document;
	public Probability_Distributed(String File_Path){
		document = new File(File_Path);
	}
    public T getProbability(){
    	return Probability;
    }
}
