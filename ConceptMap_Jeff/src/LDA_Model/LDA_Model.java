package LDA_Model;

public class LDA_Model {
	private Topic_Word tw;
	private Document_Topic dt;
	public LDA_Model(String TWord_Path,String Theta_Path){
		tw = new Topic_Word(TWord_Path);
		dt = new Document_Topic(Theta_Path);
		dt.getTopicOrder();
	}
	public Topic_Word getTopic_Word(){
		return tw;	
	}
	public Document_Topic getDocument_Topic(){
		return dt;	
	}
}
