package file_read;

import object.Read_object;

public interface RModel_Interface {
	public abstract void Read(Read_object read_object);
	public abstract void Show_result();
	
}
