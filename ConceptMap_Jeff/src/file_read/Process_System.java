package file_read;

import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;

//處理系統萃取出來的關鍵詞
public class Process_System {
	private ArrayList<Keyphrase> systemkeyphrase_list = new ArrayList<Keyphrase>();
	public void Process(ArrayList<String> text) {
	int count = 0;
	for(String line : text)
	{
		count++;
		String[] tmp = line.split(":");
		Keyphrase keyphrase = new Keyphrase();
		keyphrase.setOr(tmp[0].replace(String.valueOf(count) + ".", "").trim());
		systemkeyphrase_list.add(keyphrase);
	}
	}
    public ArrayList<Keyphrase> getsystem_phrase() {
		
		return systemkeyphrase_list;
	}
	public void Show_result() {
		int answer_count = 0;
		for(Keyphrase answer : systemkeyphrase_list){
			answer_count++;
			System.out.println("標準答案*******************");
			System.out.println(answer_count+". "+answer.getOr());
		}
		
	}
}
