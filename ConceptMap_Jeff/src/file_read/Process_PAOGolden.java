package file_read;

import java.util.ArrayList;

import object.Keyphrase;

public class Process_PAOGolden {
	private ArrayList<Keyphrase> golden_phrase = new ArrayList<Keyphrase>();
	public void Process(ArrayList<String> text) {
		for(String line : text){
			String[] tmp =line.split(";");
			for (int i = 0; i < tmp.length; i++) {
				Keyphrase keyphrase = new Keyphrase();
				keyphrase.setOr(tmp[i]);
				golden_phrase.add(keyphrase);
			}
		}
	}
	public ArrayList<Keyphrase> getgolden_phrase()
	{
		return golden_phrase;
	}
}
