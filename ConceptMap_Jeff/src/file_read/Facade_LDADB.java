package file_read;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.tartarus.snowball.ext.PorterStemmer;

import Preprocess.StopWord;
import object.Keyphrase;
import object.Read_object;

public class Facade_LDADB {
	private HashMap<String,String> article_hash = new HashMap<String,String>();
	public Facade_LDADB(Read_object read_object) throws IOException
	{
		//找到資料夾底下的檔案路徑
				RModel_Multiple folder_multiple = new RModel_Multiple();
				folder_multiple.Read(read_object);
				HashMap<Integer,ArrayList<Keyphrase>> single_article = new HashMap<Integer,ArrayList<Keyphrase>>();
				for(String sub_path : folder_multiple.getfileList().keySet())
				{
					
					System.out.println(folder_multiple.getfileList().get(sub_path));
					//設定單一檔案的讀檔物件
					Read_object read_Sobject = new Read_object();
					read_Sobject.setpath(folder_multiple.getfileList().get(sub_path));
						
					//單一文件讀檔
					RModel_Single read_single = new RModel_Single();
					read_single.Read(read_Sobject);
						
						
//					System.out.println("*******************************");	
					int i = 0;//段落記數 預設格式只有2行:title+內文
					String content = "";//要加入的內文
					for(String line : read_single.getcontent())
					{	
						if(i>0 || read_single.getcontent().size()==1)//不取title
						{
//							System.out.println(line+"*******************************");
							ArrayList<String>clean_content = new ArrayList<String>();//放置去除停止字後的內文
							StopWord stopword = new StopWord();		
							PorterStemmer porterstemmer = new PorterStemmer();
							String[]tmp = line.split(" ");
//							ArrayList<String>clean_content =  new ArrayList<String>(Arrays.asList(tmp));
							for(int j = 0;j < tmp.length;j++)
							{
								clean_content.add(tmp[j].trim());
							}
							clean_content = stopword.removeStopWord(clean_content);
							for(String word : clean_content)
							{
//								System.out.println(word.replaceAll("\\pP", ""));
								porterstemmer.setCurrent(word.replaceAll("\\pP", "").toLowerCase());
								porterstemmer.stem();
	            	        	String stemWord = porterstemmer.getCurrent();  
								content+=stemWord+" ";
							}
						}
						i++;
					}
					
					
					
//					
//							
//					clean_content = stopword.removeStopWord(read_single.getcontent());
					article_hash.put(sub_path, content);
					
						
	            }
	}
	public HashMap<String,String> getResult()
	{
		return article_hash;
	}
}
