package file_read;

import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;
//***********************
//使用Facade外觀模式
//***********************
public class Facade_System {

	private HashMap<String,ArrayList<Keyphrase>> article_keyphrase = new HashMap<String,ArrayList<Keyphrase>>();
	public Facade_System(Read_object read_Mobject)
	{
		//找到資料夾底下的檔案路徑
		RModel_Multiple rmodel_multiple = new RModel_Multiple();
		rmodel_multiple.Read(read_Mobject);
		
		for(String article : rmodel_multiple.getfileList().keySet())
		{
			//設定單一檔案的讀檔物件
			Read_object read_Sobject = new Read_object();
			read_Sobject.setpath(rmodel_multiple.getfileList().get(article));
			
			//單一文件讀檔
			RModel_Single read_single = new RModel_Single();
			read_single.Read(read_Sobject);
			
			//單一文件處理內部資料
			Process_System process_system = new Process_System();
			process_system.Process(read_single.getcontent());
			
			//添加關鍵詞
			article_keyphrase.put(article,process_system.getsystem_phrase());
		}
	}
	public HashMap<String,ArrayList<Keyphrase>> getResult()
	{
		return article_keyphrase;
	} 
}
