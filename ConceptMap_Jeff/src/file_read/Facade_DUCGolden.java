package file_read;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;
//***********************
//使用Facade外觀模式
//***********************
public class Facade_DUCGolden {
	private HashMap<String, ArrayList<Keyphrase>> golden_phrase = new HashMap<String, ArrayList<Keyphrase>>();
	public Facade_DUCGolden(Read_object read_object) throws IOException
	{
		//讀取黃金標準 DUC2001LabeledKeyphrase.txt 檔案
		RModel_Single read_single = new RModel_Single();
		read_single.Read(read_object);
		
		//處理
		Process_DUCGolden process_DUCGolden = new Process_DUCGolden();
		process_DUCGolden.Process(read_single.getcontent());
		
		//給值
		this.golden_phrase = process_DUCGolden.getgolden_phrase();
		
	}
	public HashMap<String, ArrayList<Keyphrase>> getResult()
	{
		return golden_phrase;
	}
}
