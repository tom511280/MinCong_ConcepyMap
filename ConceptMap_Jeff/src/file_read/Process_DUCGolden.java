package file_read;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;

public class Process_DUCGolden{
	private HashMap<String, ArrayList<Keyphrase>> golden_phrase = new HashMap<String, ArrayList<Keyphrase>>();
	public void Process(ArrayList<String> text) {
		
			for(String line : text)
			{
				ArrayList<Keyphrase> goldenkeyphrase_list = new ArrayList<Keyphrase>();
				String[] tmp = line.split("@");
				String[] tmp2 = tmp[1].split(";");
				for (int i = 0; i < tmp2.length; i++) {
					Keyphrase keyphrase = new Keyphrase();
					keyphrase.setOr(tmp2[i]);
					goldenkeyphrase_list.add(keyphrase);
				}
				golden_phrase.put(tmp[0], goldenkeyphrase_list);
			}
	}

	public HashMap<String, ArrayList<Keyphrase>> getgolden_phrase() {
		
		return golden_phrase;
	}
	public void Show_result() {
		for(String article : golden_phrase.keySet()){
			System.out.println("文章: "+article);
			int answer_count = 0;
			System.out.println("標準答案*******************");
			for(Keyphrase answer : golden_phrase.get(article)){
				answer_count++;
				System.out.println(answer_count+". "+answer.getOr());
			}
		}
		
	}}
