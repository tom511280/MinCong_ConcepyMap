package file_read;

import java.io.FileWriter;

import java.io.IOException;
import java.util.HashMap;
import java.io.File;
import object.Read_object;
public class RModel_Multiple extends RModel_Base implements RModel_Interface {
	private HashMap<String, String> fileList = new HashMap<String, String>();
	@Override
	public void Read(Read_object read_object) {

		this.FolderPath = read_object.getFolderPath();
		f = new File(FolderPath); // 讀取"00這個資料夾"，要記得將此資料夾放置同個java file裡面
		if (f.isDirectory()) // 如果f讀到的是資料夾，就會執行
		{
			System.out.println("資料夾 : " + f.getName()); // 印出我們所讀到的資料夾
			String[] s = f.list(); // 宣告一個list
			System.out.println("共有 : " + s.length + "個文章"); // 印出資料夾裡的檔案個數
			for (int i = 0; i < s.length; i++) {
				fileList.put(s[i].replace(".txt", "").replace(".uncontr", ""), FolderPath + "\\" + s[i]);// (檔案名稱,檔案路徑)
			}
		}
	}
	public HashMap<String, String> getfileList() {
		return fileList;
	}
	@Override
	public void Show_result() {
		for(String article : fileList.keySet()){
			System.out.println("檔案: "+article+"**路徑: "+fileList.get(article));
		}

	}
}
