package file_read;

import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;

public class Facade_Para_System {
	private HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>> article_keyphrase = new HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>>();
	public Facade_Para_System(Read_object read_object)
	{
		//找到資料夾底下的檔案路徑
		RModel_Multiple folder_multiple = new RModel_Multiple();
		folder_multiple.Read(read_object);
				
		for(String sub_path : folder_multiple.getfileList().keySet())
		{
			//子資料夾
			Read_object Sfolder_object = new Read_object();
			Sfolder_object.setFolderPath(folder_multiple.getfileList().get(sub_path));
					
			RModel_Multiple Sfolder_multiple = new RModel_Multiple();
			Sfolder_multiple.Read(Sfolder_object);
					
			//int count = 0;
			HashMap<Integer,ArrayList<Keyphrase>> single_article = new HashMap<Integer,ArrayList<Keyphrase>>();
			for(String article : Sfolder_multiple.getfileList().keySet())
			{
				//設定單一檔案的讀檔物件
				Read_object read_Sobject = new Read_object();
				read_Sobject.setpath(Sfolder_multiple.getfileList().get(article));
						
				//單一文件讀檔
				RModel_Single read_single = new RModel_Single();
				read_single.Read(read_Sobject);
						
				//單一文件處理內部資料
				Process_System process_system = new Process_System();
				process_system.Process(read_single.getcontent());
				
				//添加關鍵詞
				single_article.put(Integer.valueOf(article),process_system.getsystem_phrase());
				}
			article_keyphrase.put(sub_path, single_article);
			}
	}
	public HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>> getResult()
	{
		return article_keyphrase;
	}
}
