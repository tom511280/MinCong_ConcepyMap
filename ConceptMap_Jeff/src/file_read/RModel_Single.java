package file_read;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import object.Read_object;

//讀取檔案
public class RModel_Single extends RModel_Base implements RModel_Interface {
	private ArrayList<String> content = new ArrayList<String>();
	@Override
	public void Read(Read_object read_object) {
		this.Path = read_object.getpath();
		f = new File(Path);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "MS950"));
			String text = null;
			int count = 0;
			while ((text = reader.readLine()) != null) {
				count++;
				content.add(text);
			}
		} catch (FileNotFoundException e) {
			// e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public ArrayList<String> getcontent()
	{
		return content;
	}
	public void Show_result() {
		int count = 0;
		for(String text : content){
			System.out.println("第"+count+"行: "+text);
		}
	}
}
