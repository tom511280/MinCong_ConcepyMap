package file_read;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;

public class Facade_hulGolden {
	private HashMap<String,ArrayList<Keyphrase>> golden_phrase = new HashMap<String,ArrayList<Keyphrase>>();
	public Facade_hulGolden(Read_object read_object) throws IOException
	{
		
		
		//找到資料夾底下的檔案路徑
		RModel_Multiple folder_multiple = new RModel_Multiple();
		folder_multiple.Read(read_object);
				
		for(String sub_path : folder_multiple.getfileList().keySet())
		{
//			//子資料夾
//			Read_object Sfolder_object = new Read_object();
//			Sfolder_object.setFolderPath(folder_multiple.getfileList().get(sub_path));
//					
//			RModel_Multiple Sfolder_multiple = new RModel_Multiple();
//			Sfolder_multiple.Read(Sfolder_object);
//					
			//int count = 0;
			
			for(String article : folder_multiple.getfileList().keySet())
			{
				//count++;
				//設定單一檔案的讀檔物件
				Read_object read_Sobject = new Read_object();
				read_Sobject.setpath(folder_multiple.getfileList().get(article));
						
				//單一文件讀檔
				RModel_Single read_single = new RModel_Single();
				read_single.Read(read_Sobject);
						
				//單一文件處理內部資料
				Process_PAOGolden process_paogolden = new Process_PAOGolden();
				process_paogolden.Process(read_single.getcontent());
						
				//添加關鍵詞
				golden_phrase.put(article, process_paogolden.getgolden_phrase());
			}
			
		}
		
	}
	public HashMap<String,ArrayList<Keyphrase>> getResult()
	{
		return golden_phrase;
	}
}
