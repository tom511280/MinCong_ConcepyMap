package file_read;

import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Read_object;

public class Facade_RemoveTitle {
	private HashMap<String,ArrayList<String>> Content_hash = new HashMap<String,ArrayList<String>>();
	public Facade_RemoveTitle(Read_object read_Mobject)
	{
		//找到資料夾底下的檔案路徑
		RModel_Multiple rmodel_multiple = new RModel_Multiple();
		rmodel_multiple.Read(read_Mobject);
		
		for(String article : rmodel_multiple.getfileList().keySet())
		{
			//設定單一檔案的讀檔物件
			Read_object read_Sobject = new Read_object();
			read_Sobject.setpath(rmodel_multiple.getfileList().get(article));
			
			//單一文件讀檔
			RModel_Single read_single = new RModel_Single();
			read_single.Read(read_Sobject);
			

			
			if(read_single.getcontent().size()>1)//移除title
			{
				ArrayList<String>tmp = read_single.getcontent();
				tmp.remove(0);
				Content_hash.put(article,tmp);
			}
			else//如果只有一行 代表沒title
			{
				Content_hash.put(article,read_single.getcontent());
			}
			
			//添加關鍵詞
			
		}
	}
	public HashMap<String,ArrayList<String>> getResult()
	{
		return Content_hash;
	} 
}
