package Wordnet;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import Preprocess.dataPreprocess;;

public class Wordnet {	
	
	WordNetSynsets synsets = new WordNetSynsets();
	
	ArrayList<String> word = new ArrayList<String>();
	ArrayList<String> word_hash = new ArrayList<String>();
	Set<String> set = new LinkedHashSet<String>();
	double[] countSynonyms = null;
	double[] countHyponyms = null;
	double[] countHypernyms = null;
	double[] countMeronyms = null;
	double[] countRelatedSynsets = null;
	double[] allCountWordnet = null;	
	
	//將每個段落重複和非重複的字拿到wordnet去做字權重的計算
	public ArrayList<ArrayList<Double>> wordnetScore(dataPreprocess preprocess) {
		ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();
		for(int i=0; i<preprocess.getParaNum();i++){//從1開始是為了不考慮title
			ArrayList<Double> score2 = new ArrayList<Double>();
			ArrayList<String> articleWord = preprocess.getSentenceToWord().get(i);//取得預處理後會重複的字
			ArrayList<String> articleWord_hash = preprocess.getSentenceToWord_hash().get(i);//取得預處理後不會重複的字
			wordnet(articleWord, articleWord_hash);
			for(int j=0;j<getAllCountWordnet().length;j++){
				score2.add(getAllCountWordnet()[j]);//取得字出現在wordnet的分數
			}
			score.add(score2);
		}
		return score;
	}
	
	//將每篇文章重複和非重複的字拿到wordnet去做字權重的計算
	public ArrayList<Double> wordnetScore2(dataPreprocess preprocess) {
		ArrayList<Double> score = new ArrayList<Double>();
		ArrayList<String> articleWord = preprocess.getArticleWord();//取得預處理後會重複的字
		ArrayList<String> articleWord_hash = preprocess.getArticleWord_hash();//取得預處理後不會重複的字
		wordnet(articleWord, articleWord_hash);
		for(int j=0;j<getAllCountWordnet().length;j++){
			score.add(getAllCountWordnet()[j]);//取得字出現在wordnet的分數
		}
		return score;
	}	
	
	public void wordnet(ArrayList<String> word, ArrayList<String> word_hash){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(3);//設定最大的小數位數，並將最後一位數4捨5入
		this.word = word;
		this.word_hash = word_hash;	
		
		allCountWordnet = new double[word_hash.size()];	
		findSynsets();
		for(int i=0;i<allCountWordnet.length;i++){
			allCountWordnet[i] = getCountSynonyms()[i]*1 + (getCountHyponyms()[i]+getCountHypernyms()[i])*0.7 + getCountMeronyms()[i]*0.4 +getCountRelatedSynsets()[i]*0.7;
		}		
	}
	
	//找出文章中的同義字出現次數
	public void findSynsets(){
		ArrayList<String> synonyms = new ArrayList<String>();
		ArrayList<String> hyponyms = new ArrayList<String>();
		ArrayList<String> hypernyms = new ArrayList<String>();
		ArrayList<String> meronyms = new ArrayList<String>();
		ArrayList<String> relatedSynsets = new ArrayList<String>();
		countSynonyms = new double[word_hash.size()];//儲存從文章中(有重複字)有出現同義字的次數
		countHyponyms = new double[word_hash.size()];//儲存從文章中(有重複字)有出現下義字的次數
		countHypernyms = new double[word_hash.size()];//儲存從文章中(有重複字)有出現上義字的次數
		countMeronyms = new double[word_hash.size()];//儲存從文章中(有重複字)有出現包含關係字詞的次數
		countRelatedSynsets = new double[word_hash.size()];//儲存從文章中(有重複字)有出現詞性關係字詞的次數

		for(int i=0;i<word_hash.size();i++){//文章字的清單(未重複)
			synsets.Semantics(word_hash.get(i));
			synonyms = wordnetHash(synsets.getSynonyms());//存放該字所有意義的同義字
			hyponyms = wordnetHash(synsets.getHyponyms());//存放該字所有意義的下義字
			hypernyms = wordnetHash(synsets.getHypernyms());//存放該字所有意義的上義字
			meronyms = wordnetHash(synsets.getMeronyms());//存放該字所有意義的包含關係字詞
			relatedSynsets = wordnetHash(synsets.getRelatedSynsets());//存放該字所有意義的詞性關係字詞						

			for(int j=0;j<word.size();j++){//文章字的清單(有重複)
				/*****計算同義字出現在文章的數量*****/
				for(int k=0;k<synonyms.size();k++){//同義字的清單
					if(word.get(j).equals(synonyms.get(k))){					
						countSynonyms[i]++;
					}
				}
				/*****計算下義字出現在文章的數量*****/
				for(int k=0;k<hyponyms.size();k++){//下義字的清單
					if(word.get(j).equals(hyponyms.get(k))){					
						countHyponyms[i]++;
					}
				}
				/*****計算上義字出現在文章的數量*****/
				for(int k=0;k<hypernyms.size();k++){//上義字的清單
					if(word.get(j).equals(hypernyms.get(k))){					
						countHypernyms[i]++;
					}
				}
				/*****計算包含關係字詞出現在文章的數量*****/
				for(int k=0;k<meronyms.size();k++){//包含關係字詞的清單
					if(word.get(j).equals(meronyms.get(k))){					
						countMeronyms[i]++;
					}
				}
				/*****計算詞性關係字詞出現在文章的數量*****/
				for(int k=0;k<relatedSynsets.size();k++){//詞性關係字詞的清單
					if(word.get(j).equals(relatedSynsets.get(k))){					
						countRelatedSynsets[i]++;
					}
				}
			}
			if(countSynonyms[i]!=0)//因為重複字的關係要把本身出現第一次扣掉
				countSynonyms[i]--;			
		}		
	}
	
	//將Wordnet裡所取的語義字去除重複字
	public ArrayList<String> wordnetHash(ArrayList<ArrayList<String>> allSynsets){
		set.clear();
		ArrayList<String> word = new ArrayList<String>();
		for (List<String> synsets : allSynsets) {
		   	for(String word2 : synsets){
		   		set.add(word2);
		   	}
		}		
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			word.add((String) iterator.next());
		}
		return word;
	}

	public double[] getCountSynonyms() {
		return countSynonyms;
	}
	public void setCountSynonyms(double[] countSynonyms) {
		this.countSynonyms = countSynonyms;
	}

	public double[] getCountHyponyms() {
		return countHyponyms;
	}
	public void setCountHyponyms(double[] countHyponyms) {
		this.countHyponyms = countHyponyms;
	}

	public double[] getCountHypernyms() {
		return countHypernyms;
	}
	public void setCountHypernyms(double[] countHypernyms) {
		this.countHypernyms = countHypernyms;
	}

	public double[] getCountMeronyms() {
		return countMeronyms;
	}
	public void setCountMeronyms(double[] countMeronyms) {
		this.countMeronyms = countMeronyms;
	}

	public double[] getCountRelatedSynsets() {
		return countRelatedSynsets;
	}
	public void setCountRelatedSynsets(double[] countRelatedSynsets) {
		this.countRelatedSynsets = countRelatedSynsets;
	}

	public double[] getAllCountWordnet() {
		return allCountWordnet;
	}
	public void setAllCountWordnet(double[] allCountWordnet) {
		this.allCountWordnet = allCountWordnet;
	}	
}
