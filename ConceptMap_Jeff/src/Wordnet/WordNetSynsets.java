package Wordnet;

import java.util.ArrayList;
import java.util.Arrays;

import Preprocess.PartOfSpeech;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;
import edu.smu.tspell.wordnet.NounSynset;

public class WordNetSynsets {

	private ArrayList<ArrayList<String>> synonyms = new ArrayList<ArrayList<String>>();//同義字
	private ArrayList<ArrayList<String>> hyponyms = new ArrayList<ArrayList<String>>();//下義詞
	private ArrayList<ArrayList<String>> hypernyms = new ArrayList<ArrayList<String>>();//上義詞
	private ArrayList<ArrayList<String>> meronyms = new ArrayList<ArrayList<String>>();//包含關係字詞
	private ArrayList<ArrayList<String>> relatedSynsets = new ArrayList<ArrayList<String>>();//詞性關係字詞
	private String hyp_mer;//上、下、包含語意
	private int hyperHierarchical;//上義詞階層數
	
	public void Semantics(String word){
		String bin = PartOfSpeech.class.getClassLoader().getResource("").getPath().replace("bin/", "").replace("classes/", "").replace("%20", " ");
		System.setProperty("wordnet.database.dir", bin+"RelatedDocuments\\WordNet\\3.0\\dict");
		WordNetDatabase database = WordNetDatabase.getFileInstance(); //  Get the synsets containing the wrod form
		
		Synset[] synsets = database.getSynsets(word, SynsetType.NOUN);//名詞
		
		Synonym(synsets);
		NounSynset(synsets);
		DerivationallyRelatedForms(synsets,word);
	}
	
	//存取不同語義的同義字
	public void Synonym(Synset[] synsets){
		synonyms.clear();
		if (synsets.length > 0) {
			for (int i = 0; i < synsets.length; i++) {
				ArrayList<String> synset = new ArrayList<String>();
				String[] wordForms = synsets[i].getWordForms();//取得每個語意的同義詞(多個)  
                for (int j = 0; j< wordForms.length; j++) {                      
                	synset.add(wordForms[j]);
                }
                synonyms.add(synset);
			}				
        }
	}
	
	public void NounSynset(Synset[] synsets){
		hyponyms.clear();hypernyms.clear();meronyms.clear();
		if (synsets.length > 0) {
			for (int i = 0; i < synsets.length; i++) { 
				hyp_mer = "";
				String hyp_mer2 = Hyponyms(synsets[i]);
				if(!hyp_mer2.isEmpty()){
					ArrayList<String> hyp_mer = new ArrayList<String>(Arrays.asList(hyp_mer2.split(",")));					
					hyponyms.add(hyp_mer);
				}
				
				hyp_mer = ""; hyperHierarchical = 0;
				hyp_mer2 = Hypernyms(synsets[i]);
				if(!hyp_mer2.isEmpty()){
					ArrayList<String> hyp_mer = new ArrayList<String>(Arrays.asList(hyp_mer2.split(",")));					
					hypernyms.add(hyp_mer);
				}
				
				hyp_mer = "";
				hyp_mer2 = Meronyms(synsets[i]);
				if(!hyp_mer2.isEmpty()){
					ArrayList<String> hyp_mer = new ArrayList<String>(Arrays.asList(hyp_mer2.split(",")));					
					meronyms.add(hyp_mer);
				}
			}
		}		
	}
	
	public String Hyponyms(Synset synsets){
		NounSynset[] nounsynset = ((NounSynset) synsets).getHyponyms(); //取得下義詞
		if(nounsynset.length>0){		    
		    for (int i = 0; i < nounsynset.length; i++) {		    	
				for(int j = 0; j < nounsynset[i].getWordForms().length; j++){//每個語意有多個字詞，用getWordForms()取得陣列						
					hyp_mer = hyp_mer + nounsynset[i].getWordForms()[j] + ",";
				}				
				Hyponyms(nounsynset[i]);				
		    }		    
		}
		return hyp_mer;
	}
	
	public String Hypernyms(Synset synsets){
		NounSynset[] nounsynset = ((NounSynset) synsets).getHypernyms(); //取得上義詞
		if(nounsynset.length>0){		    
		    for (int i = 0; i < nounsynset.length; i++) {		    	
				for(int j = 0; j < nounsynset[i].getWordForms().length; j++){//每個語意有多個字詞，用getWordForms()取得陣列						
					hyp_mer = hyp_mer + nounsynset[i].getWordForms()[j] + ",";
				}
				hyperHierarchical++;
				if(hyperHierarchical<3)//上義詞只取至第3層
					Hypernyms(nounsynset[i]);				
		    }		    
		}
		return hyp_mer;
	}
	
	public String Meronyms(Synset synsets){
		NounSynset[] nounsynset = ((NounSynset) synsets).getPartMeronyms(); //取得包含關係字詞	
		if(nounsynset.length>0){		    
		    for (int i = 0; i < nounsynset.length; i++) {
				for(int j = 0; j < nounsynset[i].getWordForms().length; j++){//每個語意有多個字詞，用getWordForms()取得陣列						
					hyp_mer = hyp_mer + nounsynset[i].getWordForms()[j] + ",";
				}				
				Meronyms(nounsynset[i]);				
		    }
		}
		nounsynset = ((NounSynset) synsets).getMemberMeronyms(); //取得包含關係字詞	
		if(nounsynset.length>0){		    
		    for (int i = 0; i < nounsynset.length; i++) {
				for(int j = 0; j < nounsynset[i].getWordForms().length; j++){//每個語意有多個字詞，用getWordForms()取得陣列						
					hyp_mer = hyp_mer + nounsynset[i].getWordForms()[j] + ",";
				}				
				Meronyms(nounsynset[i]);				
		    }
		}
		nounsynset = ((NounSynset) synsets).getSubstanceMeronyms(); //取得包含關係字詞	
		if(nounsynset.length>0){		    
		    for (int i = 0; i < nounsynset.length; i++) {
				for(int j = 0; j < nounsynset[i].getWordForms().length; j++){//每個語意有多個字詞，用getWordForms()取得陣列						
					hyp_mer = hyp_mer + nounsynset[i].getWordForms()[j] + ",";
				}				
				Meronyms(nounsynset[i]);				
		    }
		}
		return hyp_mer;
	}
	
	//存取不同語義的詞性變化之字詞
	public void DerivationallyRelatedForms(Synset[] synsets, String word){
		relatedSynsets.clear();
		if (synsets.length > 0) {
			for (int i = 0; i < synsets.length; i++) {
				ArrayList<String> synset = new ArrayList<String>();
				WordSense[] wordSense = synsets[i].getDerivationallyRelatedForms(word);//取得每個語意的詞性關聯(多個)
				for(int j=0;j<wordSense.length;j++){
					String[] wordForms = wordSense[j].getSynset().getWordForms();
					for(int k=0; k<wordForms.length;k++){
						synset.add(wordForms[k]);
					}
				}
				relatedSynsets.add(synset);
			}				
		}
		else{  
			//System.err.println("No synsets exist that contain the word form '" + word + "'");  
		}
	}

	//存取不同語義的同義字
	public ArrayList<ArrayList<String>> getSynonyms() {
		return synonyms;
	}
	public void setSynonyms(ArrayList<ArrayList<String>> synonyms) {
		this.synonyms = synonyms;
	}

	public ArrayList<ArrayList<String>> getHyponyms() {
		return hyponyms;
	}
	public void setHyponyms(ArrayList<ArrayList<String>> hyponyms) {
		this.hyponyms = hyponyms;
	}

	public ArrayList<ArrayList<String>> getHypernyms() {
		return hypernyms;
	}
	public void setHypernyms(ArrayList<ArrayList<String>> hypernyms) {
		this.hypernyms = hypernyms;
	}

	public ArrayList<ArrayList<String>> getMeronyms() {
		return meronyms;
	}
	public void setMeronyms(ArrayList<ArrayList<String>> meronyms) {
		this.meronyms = meronyms;
	}

	//存取不同語義的詞性變化之字詞
	public ArrayList<ArrayList<String>> getRelatedSynsets() {
		return relatedSynsets;
	}
	public void setRelatedSynsets(ArrayList<ArrayList<String>> relatedSynsets) {
		this.relatedSynsets = relatedSynsets;
	}
}
