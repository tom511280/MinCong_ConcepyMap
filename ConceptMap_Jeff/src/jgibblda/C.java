package jgibblda;

import java.io.IOException;
import java.text.DecimalFormat;

abstract class SuperC{
int a = 5;
int b = 7;
public SuperC(int i,int j){
	a = i;
	b = j;
}
public void area(){
	System.out.println(a*b);
	area(0);
}
public abstract void area(int i);
}
public class C extends SuperC{
	public C(int i,int j){
		super(i*j,j);
	}
	public static void main(String[] args){
//		C aa1 = new C(2,4);
//		aa1.area();
//		C aa2 = new C(3,5);
//		aa2.area(false);
//		String s3 = new String("hello");
//		String s4 = new String("hello");
//		System.out.println(s3==s4);
//		System.out.println(s3.equals(s4));
		
		Integer s3 = new Integer(1);
		Integer s4 = new Integer(1);
		
		String s5 = new String("hello");
		String s6 = new String("hello");
		
		String s7 = "hello";
		String s8 = "hello";
		
		
		System.out.println(s7==s8);
		System.out.println(s7.equals(s8));
	}
	@Override
	public void area(int i) {
		System.out.println(this.a*this.b%2);
		
	}
	public void area(boolean show) {
	    System.out.println(this.a*this.b/2);
	}

}
