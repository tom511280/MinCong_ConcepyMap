/*
 * Copyright (C) 2007 by
 * 
 * 	Xuan-Hieu Phan
 *	hieuxuan@ecei.tohoku.ac.jp or pxhieu@gmail.com
 * 	Graduate School of Information Sciences
 * 	Tohoku University
 * 
 *  Cam-Tu Nguyen
 *  ncamtu@gmail.com
 *  College of Technology
 *  Vietnam National University, Hanoi
 *
 * JGibbsLDA is a free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * JGibbsLDA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JGibbsLDA; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

package jgibblda;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.kohsuke.args4j.*;

import Export_File.Export_SingleLDA;
import Preprocess.dataPreprocess;

public class LDA {
	public String FileName;
	public dataPreprocess preprocess = new dataPreprocess();

	// 以下為建構子供整合後的程式使用
	public LDA(String FileName,String Path, dataPreprocess preprocess) throws IOException {
		this.preprocess = preprocess;
		this.FileName = FileName;
		Path = Path+"\\";
		/************************************************/
		// 檢查是否訓練過單篇文章的主題分布 沒有則進行訓練
		File file_exist = new File(Path + FileName
				+ "_LDA.txt.model-final.THETA");
		if (!file_exist.exists()) {
			System.out.println(FileName+" 的LDA還未訓練過，訓練時間漫長請耐心等候*******************************");
			Export_SingleLDA es = new Export_SingleLDA(FileName,Path, false, preprocess.getArticleWord());
			System.out.println("訓練LDA模型中*******************************");
			// 以下為原來成API內容
			/************************************************/
			LDACmdOption option = new LDACmdOption();
			option.dir = Path;
			System.out.println(FileName + "_LDA.txt");
			option.dfile = FileName + "_LDA.txt";

			CmdLineParser parser = new CmdLineParser(option);
			Inferencer inferencer = new Inferencer();
			inferencer.init(option);

			Model newModel = inferencer.inference();

			for (int i = 0; i < newModel.phi.length; ++i) {
				// phi: K * V
				// System.out.println("-----------------------\ntopic" + i +
				// " : ");
				for (int j = 0; j < 10; ++j) {
					// System.out.println(inferencer.globalDict.id2word.get(j) +
					// "\t" + newModel.phi[i][j]);
				}
			}
			System.out.println(newModel.twordsSuffix);

			/*************************************/
			ArrayList<String> delete_list = new ArrayList<String>();
			String delete_string = Path + FileName
					+ "_LDA.txt.model-final.";
			delete_list.add(delete_string + "TASSIGN");
			delete_list.add(delete_string + "PHI");
			delete_list.add(delete_string + "OTHERS");
			delete_list.add(delete_string + "TWORDS");
			delete_list.add(Path + FileName + "_LDA.txt");

			for (String delete : delete_list) {
				File file_delete = new File(delete);
				file_delete.delete();
				System.err.println("刪除了: " + delete);
			}
		} else {
			System.out.println(FileName+" 的LDA已經訓練過，可直接進行TPR的關鍵字萃取*******************************");
		}
		/************************************************/
	}
	// 以下為LDA API提供的Main Function
	/*
	 * public static void main(String args[]){ double startTime, endTime,
	 * totTime; startTime = System.currentTimeMillis();// 取得目前時間
	 * 
	 * LDACmdOption option = new LDACmdOption(); CmdLineParser parser = new
	 * CmdLineParser(option);
	 * 
	 * try { if (args.length == 0){ showHelp(parser); return; }
	 * 
	 * parser.parseArgument(args);
	 * 
	 * if (option.est || option.estc){ Estimator estimator = new Estimator();
	 * estimator.init(option); estimator.estimate(); } else if (option.inf){
	 * Inferencer inferencer = new Inferencer(); inferencer.init(option);
	 * 
	 * Model newModel = inferencer.inference();
	 * 
	 * for (int i = 0; i < newModel.phi.length; ++i){ //phi: K * V
	 * System.out.println("-----------------------\ntopic" + i + " : "); for
	 * (int j = 0; j < 10; ++j){
	 * System.out.println(inferencer.globalDict.id2word.get(j) + "\t" +
	 * newModel.phi[i][j]); } } System.out.println(newModel.twordsSuffix); } }
	 * catch (CmdLineException cle){ System.out.println("Command line error: " +
	 * cle.getMessage()); showHelp(parser); return; } catch (Exception e){
	 * System.out.println("Error in main: " + e.getMessage());
	 * e.printStackTrace(); return; } endTime = System.currentTimeMillis();
	 * totTime = endTime - startTime; System.out.println();
	 * System.out.println("Using Time: " + totTime / 1000 + " sec"); }
	 * 
	 * public static void showHelp(CmdLineParser parser){
	 * System.out.println("LDA [options ...] [arguments...]");
	 * parser.printUsage(System.out); }
	 */

}
