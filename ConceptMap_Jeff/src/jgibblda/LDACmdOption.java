package jgibblda;

import org.kohsuke.args4j.*;

public class LDACmdOption {
	
	@Option(name="-est", usage="Specify whether we want to estimate model from scratch")
	public boolean est = true;
	
	@Option(name="-estc", usage="Specify whether we want to continue the last estimation")
	public boolean estc = false;
	
	@Option(name="-inf", usage="Specify whether we want to do inference")
	//public boolean inf = true;
	public boolean inf = false;
	@Option(name="-dir", usage="Specify directory")
	public String dir = "D:/20150505LDAtest";
//	public String dir = "D:/wiki_5w_1014";
//	public String dir = "D:/wiki_test";
	//D:\wiki10\1.dat
	//C:\Users\USE0000000000000000000000000000000000000000000000000000000000000R\workspace\JGibbLDA-v.1.0\models\12345678\1.dat

	
	@Option(name="-dfile", usage="Specify data file")
	public String dfile = "duc2001_LDA.txt";
	
	
	@Option(name="-model", usage="Specify the model name")
	public String modelName = "model-final";
	
	@Option(name="-alpha", usage="Specify alpha")
	public double alpha = 0.2;
	
	@Option(name="-beta", usage="Specify beta")
	public double beta = 0.1;
	
	@Option(name="-ntopics", usage="Specify the number of topics")
	public int K = 50;
	
	@Option(name="-niters", usage="Specify the number of iterations")
	public int niters = 100;
	
	@Option(name="-savestep", usage="Specify the number of steps to save the model since the last save")
	public int savestep = 100;
	
	@Option(name="-twords", usage="Specify the number of most likely words to be printed for each topic")
	public int twords = 500;
	
	@Option(name="-withrawdata", usage="Specify whether we include raw data in the input")
	public boolean withrawdata = false;
	
	@Option(name="-wordmap", usage="Specify the wordmap file")
	public String wordMapFileName = "wordmap.txt";
}
