package Topic_MethodMenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import object.Hash_compare;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

class Google_object
{
	double co_occurrence = 0;
	ArrayList<String>nodeList = new ArrayList<String>();
	public Google_object(String n1,String n2)
	{
		nodeList.add(n1);
		nodeList.add(n2);
		this.co_occurrence = co_occurrence;
	}
	//在這連結的方向不重要  因為兩個字皆在google上搜尋不用考慮誰前誰後
	public boolean Gcontain(Google_object Gobject)
	{
		//for(String nodes : nodeList)
		if(nodeList.contains(Gobject.nodeList.get(0)) && nodeList.contains(Gobject.nodeList.get(1)))
			return true;
		else
			return false;
	}
}


public class GR {
	private HashMap<String, Double>rdefine = new HashMap<String, Double>();
	private ArrayList<HashMap<String, HashMap<String, Double>>> GPedge_weight = new ArrayList<HashMap<String, HashMap<String, Double>>>();
	private ArrayList<Google_object>Gcolist = new ArrayList<Google_object>();
	private int Searchcount = 0;
	private int IpNo;
	/*******************Google Graph*********************/
	public GR(ArrayList<HashMap<String, HashMap<String, Double>>> Pedge_weight,int Searchcount,int IpNo) throws IOException, InterruptedException
	{
		//this.Pedge_weight = Pedge_weight;
		this.Searchcount = Searchcount;	
	    this.IpNo = IpNo;	
		int paracount = 0;
		
		for(HashMap<String, HashMap<String, Double>> para : Pedge_weight)//paragraph
		{
			HashMap<String, HashMap<String, Double>>link = new HashMap<String, HashMap<String, Double>>();
			for(String key : para.keySet())//mainnode
			{
				HashMap<String, Double>node = new HashMap<String, Double>();
				for(String value : para.get(key).keySet())
				{
					Google_object google_object = new Google_object(key,value);
					node.put(key, contain_element(google_object,Pedge_weight.get(paracount).get(key).get(value)));
				}
				link.put(key, node);
			}
			GPedge_weight.add(link);
			paracount++;
		}
	}
	public double contain_element(Google_object Gobject,double linkweight) throws ClientProtocolException, IOException, InterruptedException{
		double co_occurrence=0;
		for(Google_object Gkey : Gcolist)
		{
			if(Gkey.Gcontain(Gobject))
				co_occurrence = Gkey.co_occurrence*linkweight;
		}
		if(co_occurrence==0)
		{
			co_occurrence = Math.log(Gco_occurrence(Gobject.nodeList.get(0),Gobject.nodeList.get(1)))*linkweight;//取log縮小範圍
			Gobject.co_occurrence = co_occurrence;
			Gcolist.add(Gobject);
			if(this.Searchcount%250==0)//每250次搜尋換一次IP
	    		{
				System.out.println("**"+Searchcount);
	    		System.out.println("**"+Searchcount);
	    		System.out.println("**"+IpNo);
	    		Change_IP();
	    		this.IpNo++;
	    		
	    		if(this.IpNo == 6)//只有7個IP  所以到第五個IP時要歸零回到第一個IP
	    			this.IpNo = 0;
	    		}
			Searchcount++;
	  
		}
		return co_occurrence;
	}
	
	/*******************Google relation*********************/
	public GR(HashMap<String, Double>keyphrase,int Searchcount,int IpNo) throws IOException, InterruptedException
	{
	    this.Searchcount = Searchcount;	
	    this.IpNo = IpNo;	
		ArrayList<String>mainkeyphrase = new ArrayList<String>();
		Hash_compare hash_compare = new Hash_compare();
		hash_compare.SDhash_compare(keyphrase);
		
		//取得前3名
		mainkeyphrase.add(hash_compare.getKeyArray().get(0));
		mainkeyphrase.add(hash_compare.getKeyArray().get(1));
		mainkeyphrase.add(hash_compare.getKeyArray().get(2));
		
		System.out.println("第一名: "+hash_compare.getKeyArray().get(0));
		System.out.println("第二名: "+hash_compare.getKeyArray().get(1));
		System.out.println("第三名: "+hash_compare.getKeyArray().get(2));
		

		
		
		for(String main : mainkeyphrase)
		{
			HashMap<String, Double>Erdefine = new HashMap<String, Double>();
		    for(int i = 3;i < hash_compare.getKeyArray().size();i++)//算共現值 只算第3名之外
		    {
		    	double tatal_weight = 0;
		    	String[]tmp = hash_compare.getKeyArray().get(i).split(" ");//把這些詞分割成字
		    	for(int j = 0;j < tmp.length;j++)
		    	{
		    				 try {
		    				        	tatal_weight+=Gco_occurrence(main,tmp[j]); 
		    				        }
		    				        catch (Exception ex) {
		    				        	tatal_weight+=0; 
		    							System.out.println("連線發生錯誤");
		    				        }
		    		
		    		
		    		//rdefine.put(hash_compare.getKeyArray().get(i),tatal_weight);
//		    		Erdefine.put(hash_compare.getKeyArray().get(i),tatal_weight);
		    		if(this.Searchcount%250==0)//每100次搜尋換一次IP
		    		{
//		    		System.out.println(Searchcount);
//		    		System.out.println(IpNo);
		    		Change_IP();
		    		this.IpNo++;
		    		
		    		if(this.IpNo == 7)//只有8個IP  所以到第五個IP時要歸零回到第一個IP
		    			this.IpNo = 0;
		    		}
		    		this.Searchcount++;
		    	}
		    	/*******************權重*********************/
//		    	Erdefine.put(hash_compare.getKeyArray().get(i),tatal_weight/tmp.length);
		    	rdefine.put(hash_compare.getKeyArray().get(i),tatal_weight);
		    }
		    Hash_compare hash_compare2 = new Hash_compare();//排名從新定義後的結果
			hash_compare2.SDhash_compare(Erdefine);
			//rdefine.clear();
			for(int j = 0;j < hash_compare2.getKeyArray().size();j++)//算名次分數
			{
//				System.out.println(rdefine.get(hash_compare2.getKeyArray().get(j)));
//				System.out.print("第"+(j+1)+"名: "+hash_compare2.getKeyArray().get(j)+" 分數: ");
//				System.out.println(hash_compare2.getValueArray().get(j));
				
				//rdefine.put(hash_compare2.getKeyArray().get(j), 
						//hash_compare2.getValueArray().get(j)+rdefine.get(hash_compare2.getKeyArray().get(j)));
				if(rdefine.containsKey(hash_compare2.getKeyArray().get(j)))
				    rdefine.put(hash_compare2.getKeyArray().get(j), (j+1)+rdefine.get(hash_compare2.getKeyArray().get(j)));
				//原來的加上當前排名分數
				else
				    rdefine.put(hash_compare2.getKeyArray().get(j), (double) (j+1));
			}
		}
		
//		double w = 0.7;
//		double w1 = 0.3;
		//反轉
		for(String phrase : rdefine.keySet())
		{
//			System.out.println(phrase+".排名總和: "+rdefine.get(phrase));
			/*******************權重*********************/
			String[]tmp = phrase.split(" ");
			double len = Double.valueOf(tmp.length);
//			if(len==1)
//			    rdefine.put(phrase, (((double)(1/rdefine.get(phrase)))+keyphrase.get(phrase))/len);//除以1來反轉數值  因為排名越低代表越重要
//			    rdefine.put(phrase, keyphrase.get(phrase)/len);//除以1來反轉數值  因為排名越低代表越重要
			    rdefine.put(phrase, ((double)(1/rdefine.get(phrase))));//除以1來反轉數值  因為排名越低代表越重要
//			else
//				rdefine.put(phrase, (((double)(1/rdefine.get(phrase)))+keyphrase.get(phrase)));//除以1來反轉數值  因為排名越低代表越重要
			
			
//			rdefine.put(phrase, ((w1*(double)(1/rdefine.get(phrase)))+w*keyphrase.get(phrase)));//除以1來反轉數值  因為排名越低代表越重要
		}
		rdefine.put(hash_compare.getKeyArray().get(0),(double) 4);
		rdefine.put(hash_compare.getKeyArray().get(1),(double) 3);
		rdefine.put(hash_compare.getKeyArray().get(2),(double) 2);
		
		Hash_compare hash_compare3 = new Hash_compare();//排名從新定義後的結果
		hash_compare3.SDhash_compare(rdefine);
		for(int j = 0;j < hash_compare3.getKeyArray().size();j++)//算名次分數
		{
			System.out.print("第"+(j+1)+"名: "+hash_compare3.getKeyArray().get(j)+" 分數: ");
			System.out.println(hash_compare3.getValueArray().get(j));
		}
	}
	private double Gco_occurrence(String node1,String node2) throws ClientProtocolException, IOException, InterruptedException
	{
		String mainnode = "";
		String[]tmp = node1.split(" ");
		for(int i = 0;i < tmp.length;i++)
		{
			if(i!=(tmp.length-1))
			    mainnode+=tmp[i].replaceAll("\\pP", "")+"+";
			else
				mainnode+=tmp[i].replaceAll("\\pP", "");	
		}
		node1 = mainnode;
		
		double co_occurrence = 0;
		System.out.println(node1+" : "+node2);
		String url="https://www.google.com/search?q="+node1+"+"+node2;
		Document doc1 = Jsoup.connect(url).timeout(30000).header("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2").get();  
		String result = doc1.select("div[id=resultStats]").text();
		co_occurrence= Double.parseDouble(result.replace("About","").replace("約有","").replace("項結果","").replace(",","").trim()); //取關鍵字搜尋的值
		System.out.println("有"+co_occurrence+"次");
		return co_occurrence;
	}
	private void Change_IP() throws IOException, InterruptedException
	{
		String[] ipNum = {"88", "91", "93", "96", "97", "106", "73"};//8個ip 2250個搜索量	
		System.out.println("準備成"+IpNo);
		System.out.println("準備IP轉換成"+ipNum[IpNo]);
			Runtime.getRuntime().exec(
					"cmd /c start C:\\Users\\USER\\Desktop\\"
					+ "ConceptMap_Jeff\\RelatedDocuments\\googleIP\\ori\\"+ipNum[IpNo]+".bat");
		System.out.println("等待IP轉換時間...");
		Thread.sleep(8000);//轉換時間
		System.out.println("成功換成IP"+ipNum[IpNo]);
		
	}
	public HashMap<String, Double> getrdefine()
	{
		return rdefine;
	}
	public ArrayList<HashMap<String, HashMap<String, Double>>> getGPedge_weight()
	{
		return GPedge_weight;
	}
	public int getSearchcount()
	{
		return Searchcount;
	}
	public int getIpNo()
	{
		return IpNo;	
	}
}
