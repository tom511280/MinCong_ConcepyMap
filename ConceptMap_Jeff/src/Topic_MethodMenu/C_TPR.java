package Topic_MethodMenu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Hash_compare;
import Export_File.Export_Keyphrase;
import LDA_Model.LDA_Model;
import NER.demo.Entity;
import Preprocess.dataPreprocess;
import Topic_Iteration.CTopic_Iteration;
import Topic_Parameter.CParameter;
import Topic_keyphraseExtract.CTopic_keyphraseExtract;
import Word_Graph.CWord_Graph;


public class C_TPR{
	private CParameter cparameter;
	private String FileName;
	public C_TPR(String FileName, dataPreprocess Preprocess, LDA_Model lda_model) throws IOException{

		//使用建構子給定: 預處理 LDA模型 方法類型
		this.FileName = FileName;
		cparameter = new CParameter(FileName, Preprocess, lda_model);
		
		CWord_Graph word_graph = new CWord_Graph(cparameter,3,1);
		word_graph.execute();
		cparameter.setCWord_Graph(word_graph);
		
		CTopic_Iteration topic_iteration = new CTopic_Iteration(50, cparameter);
		topic_iteration.execute();
		cparameter.setCTopic_Iteration(topic_iteration);
		
		CTopic_keyphraseExtract topic_keyphraseExtract = new CTopic_keyphraseExtract(cparameter);
		topic_keyphraseExtract.execute();
		cparameter.setCTopic_keyphraseExtract(topic_keyphraseExtract);
		
		
	}
	public CParameter getresult(){
		return cparameter;
	}
	public void export() throws IOException{
		Export_Keyphrase export_keyphrase = new Export_Keyphrase(FileName,"D:\\Keyphrase_Result",false,cparameter.getTopic_keyphraseExtract().getCkeyphrase());
	}
	public ArrayList<Entity> getConceptMap_Cpreprocess(){//把要放進第一層的概念包裝成Conceptmap可以接受的格式
		ArrayList<Entity> concept = new ArrayList<Entity>();
		//降序排序
		Hash_compare hash_compare = new Hash_compare();  
		hash_compare.SDhash_compare((HashMap<String, Double>)cparameter.getTopic_keyphraseExtract().getCkeyphrase());
		for (int i = 0;i < hash_compare.getKeyArray().size();i++) 
		{
			Entity entity = new Entity(hash_compare.getKeyArray().get(i), String.valueOf(hash_compare.getValueArray().get(i)));
			concept.add(entity);
		}	
		return concept;
	}
}
