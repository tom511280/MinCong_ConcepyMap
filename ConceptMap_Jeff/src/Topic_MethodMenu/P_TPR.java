package Topic_MethodMenu;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Hash_compare;
import Export_File.Export_Keyphrase;
import LDA_Model.LDA_Model;
import NER.demo.Entity;
import Preprocess.dataPreprocess;
import Topic_Iteration.PTopic_Iteration;
import Topic_Parameter.CParameter;
import Topic_Parameter.PParameter;
import Topic_keyphraseExtract.PTopic_keyphraseExtract;
import Word_Graph.PWord_Graph;


public class P_TPR {
	private PParameter pparameter;
	private String FileName;
	public P_TPR(String FileName, dataPreprocess Preprocess, LDA_Model lda_model) throws IOException{

		System.out.println("萃取段落關鍵詞**************************");
		
		this.FileName = FileName;
		//使用建構子給定: 預處理 LDA模型 方法類型
		pparameter = new PParameter(FileName, Preprocess, lda_model);
		
		//word_graph
		PWord_Graph word_graph = new PWord_Graph(pparameter,3,1);
		word_graph.execute();
		pparameter.setPWord_Graph(word_graph);
		
		//Topic_Iteration
		PTopic_Iteration topic_iteration = new PTopic_Iteration(50, pparameter);
		topic_iteration.execute();
		pparameter.setPTopic_Iteration(topic_iteration);
		
		//Topic_keyphraseExtract
		PTopic_keyphraseExtract topic_keyphraseExtract = new PTopic_keyphraseExtract(pparameter);
		topic_keyphraseExtract.execute();
		pparameter.setPTopic_keyphraseExtract(topic_keyphraseExtract);
		
		//檢查有沒有存放段落關鍵詞擷取結果的資料夾
		File dir=new File("D:\\Keyphrase_Result\\"+FileName);
	       if(dir.exists()){
	           System.out.println("A folder with name 'new folder' is already exist in the path "+FileName);
	       }else{
	           dir.mkdir();
	       }	
	}
	
	public PParameter getresult(){
		return pparameter;
	}
	//輸出
	public void export() throws IOException{
		int para_count = 1;
		for(HashMap<String, Double> para : pparameter.getPTopic_keyphraseExtract().getPkeyphrase()){
			Export_Keyphrase export_keyphrase = new Export_Keyphrase(FileName+"_"+para_count,"D:\\Keyphrase_Result\\"+FileName,false,para);
			para_count++;
		}
	}
	public ArrayList<ArrayList<Entity>> getConceptMap_Ppreprocess(){//把要放進第二三層的概念包裝成Conceptmap可以接受的格式
		ArrayList<ArrayList<Entity>> concept = new ArrayList<ArrayList<Entity>>();
		for(HashMap<String, Double> para : pparameter.getPTopic_keyphraseExtract().getPkeyphrase()){
			ArrayList<Entity> Paraconcept = new ArrayList<Entity>();
			Hash_compare hash_compare = new Hash_compare();  
			hash_compare.SDhash_compare((HashMap<String, Double>)para);
			for (int i = 0;i < hash_compare.getKeyArray().size();i++) 
			{
				Entity entity = new Entity(hash_compare.getKeyArray().get(i), String.valueOf(hash_compare.getValueArray().get(i)));
				Paraconcept.add(entity);
			}	
			concept.add(Paraconcept);
		}
		return concept;
	}
}
