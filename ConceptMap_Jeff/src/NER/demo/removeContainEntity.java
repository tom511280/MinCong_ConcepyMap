package NER.demo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class removeContainEntity extends Entity{
	public removeContainEntity(String entity, String entity_type) {
		super(entity, entity_type);
	}
    
	@Override
	public boolean equals(Object obj) {		
		if(!(obj instanceof Entity)){
	   		 return false;
	   	}else{
	   		removeContainEntity e = (removeContainEntity)obj;	   		
	   		if(e.entity.toLowerCase().indexOf(entity.toLowerCase())>=0)
	    		return true;
	        else
	            return false;
	   	}
	}
	
	public static Set<Entity> singleElement(Set<Entity> phrase2){
		ArrayList<removeContainEntity> element2 = new ArrayList<removeContainEntity>();
        for(Entity e1: phrase2){
        	element2.add(new removeContainEntity(e1.entity, e1.entity_type));
        }
        ArrayList<removeContainEntity> newEntity = new ArrayList<removeContainEntity>();
        Iterator it = element2.iterator();
        while(it.hasNext()){
            Object obj = it.next();
            if(!(newEntity.contains(obj))){//contain會調用Person類中的equals方法
            	newEntity.add((removeContainEntity) obj);
            }
        }
        Set<Entity> newEntity2 = new LinkedHashSet<Entity>();
        for(removeContainEntity e1: newEntity){
        	newEntity2.add(new Entity(e1.entity, e1.entity_type));
        }
        return newEntity2;
    }
}
