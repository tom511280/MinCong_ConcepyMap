package NER.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import Preprocess.outputFile;
import Preprocess.outputText;

public class accessNER {
	private String bin = outputText.class.getClassLoader().getResource("").getPath().replace("bin/", "").replace("classes/", "").replace("%20", " ");
	private String root = bin+"RelatedDocuments\\nerdir\\";
	outputFile output = new outputFile();
	
	public void storeNER(ArrayList<Entity> entity) throws IOException{		
		Set<Entity> set = new LinkedHashSet<Entity>();
		ArrayList<Entity> sortEntity = new ArrayList<Entity>();
		ArrayList<Entity> myEntity = new ArrayList<Entity>();
		
		myEntity = extractNER();//先取出既有的Entity
		
		for(Entity e1 : myEntity)
			set.add(e1);
		//由於Entity類別以定義好去除重複的方法，因此放入set即可去除
		for(Entity e1 : entity)
			set.add(e1);

		Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
        	sortEntity.add((Entity) iterator.next());
        }
        
        // 依string排序
        Collections.sort(sortEntity, new Comparator<Entity>() {
        	public int compare(Entity o1, Entity o2) {
        		return o1.getEntity().compareTo(o2.getEntity());
            }
        });
        // 依Integer排序
//        Collections.sort(sortEntity, new Comparator<Entity>() {
//        	public int compare(Entity o1, Entity o2) {
//        		return o2.getInteger()-o1.getInteger();
//            }
//        });
        
        //輸出到txt檔 
        outputFile output = new outputFile();
        output.clearText(root, "accessNER");//有檔案則會清空檔案
		
		String content = "";		
		for(Entity e : sortEntity){
			content = content + e.entity+" @@ "+ e.entity_type + "\r\n";
		}

		output.outputFile2(root, "accessNER", content);	
	}
	
	public ArrayList<Entity> extractNER() throws IOException{
		ArrayList<Entity> extractEntity = new ArrayList<Entity>();
		
		File document = new File(root+"accessNER.txt");
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"utf-8"));
			String text = null;
			String[] text_split = null;
			while ((text = reader.readLine()) != null) {//readLine()方法會在讀取到使用者的換行字元時，再一次將整行字串傳入。
				text_split = text.split(" @@ ");
				extractEntity.add(new Entity(text_split[0],text_split[1]));
			}
			
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return extractEntity;
	}
	
	public Entity findNER(String phrase) throws IOException{
		ArrayList<String> findEntity = new ArrayList<String>();
		ArrayList<String> findEntityType = new ArrayList<String>();
		
		File document = new File(root+"accessNER.txt");
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"utf-8"));
			String text = null;
			String[] text_split = null;
			while ((text = reader.readLine()) != null) {//readLine()方法會在讀取到使用者的換行字元時，再一次將整行字串傳入。
				text_split = text.split(" @@ ");
				findEntity.add(text_split[0]);
				findEntityType.add(text_split[1]);
			}
			
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Entity entity = null;
		for(int i=0;i<findEntity.size();i++){
			if(findEntity.get(i).equals(phrase))
				entity = new Entity(findEntity.get(i),findEntityType.get(i));
		}
		return entity;
	}
}
