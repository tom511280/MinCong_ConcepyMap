package NER.demo;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.*;
import edu.stanford.nlp.ling.CoreLabel;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** This is a demo of calling CRFClassifier programmatically.
 *  <p>
 *  Usage: <code> java -mx400m -cp "stanford-ner.jar:." NERDemo [serializedClassifier [fileName]]</code>
 *  <p>
 *  If arguments aren't specified, they default to
 *  ner-eng-ie.crf-3-all2006.ser.gz and some hardcoded sample text.
 *  <p>
 *  To use CRFClassifier from the command line:
 *  java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier
 *      [classifier] -textFile [file]
 *  Or if the file is already tokenized and one word per line, perhaps in
 *  a tab-separated value format with extra columns for part-of-speech tag,
 *  etc., use the version below (note the 's' instead of the 'x'):
 *  java -mx400m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier
 *      [classifier] -testFile [file]
 *
 *  @author Jenny Finkel
 *  @author Christopher Manning
 */
public class Entity_nlp {
	private ArrayList<Entity> entity;
	
	public ArrayList<Entity> Entity(String par){
		ArrayList<Entity> entity = new ArrayList<Entity>();
		String bin = Entity_nlp.class.getClassLoader().getResource("").getPath().replace("bin/", "").replace("classes/", "").replace("%20", " ");
		String serializedClassifier = bin+"classifiers/english.all.3class.distsim.crf.ser.gz";
		AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
		/* For either a file to annotate or for the hardcoded text example,
        this demo file shows two ways to process the output, for teaching
        purposes.  For the file, it shows both how to run NER on a String
        and how to run it on a whole file.  For the hard-coded String,
        it shows how to run it on a single sentence, and how to do this
        and produce an inline XML output format.
		*/
		
        //System.out.println(classifier.classifyWithInlineXML(par));
        //System.out.println(classifier.classifyToString(s2, "xml", true));
        //System.err.println(classifier.classifyToString(s2));       
        ArrayList<String> tags = new ArrayList<String>();
        ArrayList<String> words = new ArrayList<String>();
        
        Pattern pattern = Pattern.compile("<[A-Z]*>[^<>]*</[A-Z]*>");
        Pattern pattern2 = Pattern.compile("<[A-Z]*>");
        Pattern pattern3 = Pattern.compile(">.*<");
  	  	Matcher matcher = pattern.matcher(classifier.classifyWithInlineXML(par));
  	  	while(matcher.find()) {
  	  		String tag = "", word = "";
  	  		String tagWord = matcher.group();
  	  		Matcher matcher2 = pattern2.matcher(tagWord);
  	  		while(matcher2.find()) {
  	  			String[] tag_split = matcher2.group().split("");
  	  			for(int i=2;i<tag_split.length-1;i++)
  	  				tag = tag + tag_split[i];
  	  			tags.add(tag);
  	  		}
	  	  	Matcher matcher3 = pattern3.matcher(tagWord);
  	  		while(matcher3.find()) {
  	  			String[] word_split = matcher3.group().split("");
	  	  		for(int i=2;i<word_split.length-1;i++)
	  	  			word = word + word_split[i];
	  	  		words.add(word);
  	  		}
  	  	}
  	  	for(int i=0;i<tags.size();i++){
  	  		entity.add(new Entity(words.get(i),tags.get(i))); 
  	  	}
  	  	
  	  	entity = Entity.singleElement(entity);//去除重複性
  	  	setEntity(entity);
		return entity;
	}
	public ArrayList<Entity> getEntity() {
		return entity;
	}
	public void setEntity(ArrayList<Entity> entity) {
		this.entity = entity;
	}
	
	public static void main(String[] args){
		Entity_nlp entity = new Entity_nlp();
		entity.Entity("With his full head of white hair, erect posture and graceful, smiling visage, the first impression that Wang Zhongtian, 85, gives is of a well-studied man of letters.");
	  	for(Entity object: entity.getEntity()){
	  		object.show();//顯示排序結果
	  	}
	  	System.out.println("end");
	}
}