package NER.demo;

import java.util.ArrayList;
import java.util.Iterator;

public class Entity {
	public String entity;      
	public String entity_type;  
      
    public Entity(String entity, String entity_type){
        this.entity = entity;  
        this.entity_type = entity_type;  
    }
    public String getEntity(){
    	return entity;
    }
    public String getEntity_type(){
    	return entity_type;
    }

    
    public void show(){
    	System.out.println(entity+" @@ "+entity_type);
    }
    
  //object類的equals為位址比較 需自定義equals方法 才能夠判斷出相等物件
    @Override  
	public boolean equals(Object obj) {
    	if(!(obj instanceof Entity)){
    		 return false;
    	}else{
    		Entity p = (Entity)obj;
    		return this.entity.equals(p.entity);
    	}
	}    

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return entity.hashCode() ;
	}
	
	public static ArrayList<Entity> singleElement(ArrayList<Entity> element){
        ArrayList<Entity> newEntity = new ArrayList<Entity>();
        Iterator it = element.iterator();
        while(it.hasNext()){
            Object obj = it.next();
            if(!(newEntity.contains(obj))){//contain會調用Person類中的equals方法
            	newEntity.add((Entity) obj);
            }
        }
        return newEntity;
    }	
}
