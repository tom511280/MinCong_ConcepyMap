package NER.demo;

import java.util.ArrayList;
import NER.demo.Entity;
import Preprocess.StemWords;

public class Entity2 extends Entity{
	
	public Entity2(String entity, String entity_type) {
		super(entity, entity_type);
	}
    
  //object類的equals為位址比較 需自定義equals方法 才能夠判斷出相等物件
    @Override  
	public boolean equals(Object obj) {
    	Entity2 e = (Entity2) obj;
    	if(entity.indexOf(e.entity)>=0 || e.entity.indexOf(entity)>=0)
    		return true;    
        else
            return false;
	}
    
    public static ArrayList<Entity> singleElement2(ArrayList<Entity> element, ArrayList<Entity> elementStem){
        ArrayList<Entity2> newEntity = new ArrayList<Entity2>();
        ArrayList<Entity> newEntity2 = new ArrayList<Entity>();
        
        for(int i=0;i<element.size();i++){        		
        	Object obj = new Entity2(element.get(i).entity,element.get(i).entity_type);
        	Object obj2 = new Entity2(elementStem.get(i).entity,elementStem.get(i).entity_type);
        	if(!(newEntity.contains(obj2))){//contain會調用Person類中的equals方法
            	newEntity.add((Entity2) obj);
            }
        }
        for(Entity2 e2 : newEntity){
        	if(e2.entity_type.equals("null"))
        		e2.entity = e2.entity.toLowerCase();
        	newEntity2.add(new Entity(e2.entity,e2.entity_type));
        }
        newEntity2 = Entity.singleElement(newEntity2);
        return newEntity2;
    }
}
