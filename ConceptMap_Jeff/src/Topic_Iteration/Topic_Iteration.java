package Topic_Iteration;


import java.util.ArrayList;
import java.util.HashMap;

import Preprocess.dataPreprocess;
import Topic_Parameter.Parameter;
import Word_Graph.CWord_Graph;



abstract public class Topic_Iteration {
	public ArrayList<HashMap<String, Double>> topic_wordList;//存放主題之下字的機率分布
	public dataPreprocess preprocess;//存放預處理
	//public ArrayList<HashMap<String, Double>> iteration_hash;//存放每次迭代的值
	
	@SuppressWarnings("unchecked")
	public Topic_Iteration(Parameter parameter){
		this.topic_wordList = (ArrayList<HashMap<String, Double>>)parameter.getLDA_Model().getTopic_Word().getProbability();
		this.preprocess = parameter.getdataPreprocess();
	}
	
	public ArrayList<HashMap<String, Double>> process(HashMap<String, HashMap<String, Double>> word_graphList) {
		ArrayList<HashMap<String, Double>> final_pagerank = new ArrayList<HashMap<String, Double>>();// 最終迭代後的pagerank值
		for (int i = 0; i < topic_wordList.size(); i++)// topic Iteration
		{
			ArrayList<HashMap<String, Double>> each_iteration = new ArrayList<HashMap<String, Double>>();
			// 由pagerank所得到的每次的迭代值<節點x,x的pagerank值>
			HashMap<String, Double> Initial_pagerank_iteration = new HashMap<String, Double>();
			// 第一次迭代給予給個word初始值=1
			for (String word : preprocess.getArticleWord_hash()) {
				Initial_pagerank_iteration.put(word, (double) 1);
			}
			each_iteration.add(Initial_pagerank_iteration);
			for (int j = 1; j < 100; j++)// 進行後98次迭代 第一次給予初值
			{
				if (j == 99 || ckeck_iteration_constraint(each_iteration) == true)// 如果本次迭代和前兩次的迭代數值差距皆小於0.001
																						   // 或
																						   // 進行到第100次迭代
																						   // 則停止計算
				{
					final_pagerank.add(iteration(word_graphList, each_iteration.get(j - 1),topic_wordList.get(i)));// 給定此主題內每個字的的最終topic_pagerank
					break;
				} else {
					each_iteration.add(iteration(word_graphList, each_iteration.get(j - 1),topic_wordList.get(i)));// 計算每次迭代 是為了檢查終止條件
				}
			}
		}
		return final_pagerank;
	}
	
	//判斷該不該停止迭代
	public boolean ckeck_iteration_constraint(ArrayList<HashMap<String, Double>> pagerank_iteration_hash2){
			boolean iteration_state = false;// 判定是否達到迭代停止條件
			if (pagerank_iteration_hash2.size() > 2)// 前兩次不做檢查
			{
				for (String key : pagerank_iteration_hash2.get(
						pagerank_iteration_hash2.size() - 1).keySet())// 取得當前迭代的HashMap
				{
					double corrent_value = pagerank_iteration_hash2.get(
							pagerank_iteration_hash2.size() - 1).get(key);
					double before_first_value = pagerank_iteration_hash2.get(
							pagerank_iteration_hash2.size() - 2).get(key);
					double before_second_value = pagerank_iteration_hash2.get(
							pagerank_iteration_hash2.size() - 3).get(key);

					double first = Math.abs(corrent_value - before_first_value);
					double second = Math.abs(corrent_value - before_second_value);

					if (first <= 0.001 && second <= 0.001)// 如果當前值和前2次迭代的差距小於0.001就達成停止條件,
															// 使用絕對值 因為不知道哪邊小
						iteration_state = true;
				}
			}

			return iteration_state;
	}
	//out_degree的運算
	public double out_degree(String wi, String wj,HashMap<String, Double> Previous_iteration,HashMap<String, HashMap<String, Double>> edge_weight) {
		double allout_degree = 0;
		double edge_value = 0;
		edge_value = edge_weight.get(wj).get(wi);// wj投給wi的票數
		for (String key : edge_weight.get(wj).keySet()) {// 計算wj的所有投票總數
			allout_degree += edge_weight.get(wj).get(key);// 加總
		}
		// System.out.println((edge_value/allout_degree*Previous_iteration.get(wj)));
		return edge_value / allout_degree * Previous_iteration.get(wj);// 計算出到底wi對wj的重要性*前一次迭代的值
																		// 並將結果回傳
	}
	public HashMap<String, Double> iteration(HashMap<String, HashMap<String, Double>> word_graphList, HashMap<String, Double> Previous_iteration,HashMap<String, Double> Each_topicword) {
		// TODO Auto-generated method stub
		double avg_destribution = (double) 1 / 12000;
		HashMap<String, Double> topic_pagerank_hash = new HashMap<String, Double>();// 存放當次迭代各個vertex的topic_pagerank
		// 計算每個字wi的權重
		for (String wi : word_graphList.keySet()) // 假如邊
			{
				double wi_vote = 0;// 別的vertex的當前topic_pagerank 用作計算本vertex分數
				double damping = 0.5;// 阻尼係數
				double p_z = avg_destribution;// 該vertex在該topic的出線機率
				if (Each_topicword.containsKey(wi)) {
					double total_topicValue = 0;
					// double pz = (double)(1/50);

					// 使用pr(z|w)
					for (HashMap<String, Double> all_topic : topic_wordList) {
						total_topicValue += all_topic.get(wi);
					}
					p_z = (double) (Each_topicword.get(wi) / total_topicValue * 1);
				}
				// 做公式(3)的∑部分 計算每個指向wi的wj 其中wj對於wi的重要性
				for (String wj : word_graphList.keySet()) {// 找所有有邊連向自己的結點
					if (word_graphList.get(wj).containsKey(wi) == true)// main_key有指向current_key
					{
						wi_vote += out_degree(wi, wj, Previous_iteration,word_graphList);// 所有wj投給wi的重要性總和
																			// 也就是wi的總票數
					}
				}
				topic_pagerank_hash.put(wi, damping * wi_vote + (1 - damping)
						* p_z);//字在主題的權重
			}
		return topic_pagerank_hash;
		
	}
//	abstract void process();//處理程序
	abstract public void execute();//執行程式
}
