package Topic_Iteration;

import java.util.ArrayList;
import java.util.HashMap;

import Topic_Parameter.PParameter;

public class PTopic_Iteration extends Topic_Iteration{
	private ArrayList<ArrayList<HashMap<String, Double>>> final_pagerank = new ArrayList<ArrayList<HashMap<String, Double>>>();// 每個段落最終迭代後的pagerank值
	private ArrayList<HashMap<String, HashMap<String, Double>>> word_graphList;//存放字圖
	public PTopic_Iteration(int k, PParameter pparameter){
		super(pparameter);
		this.word_graphList = pparameter.getPword_graph().getPWordGraph();
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		for (HashMap<String, HashMap<String, Double>> para :  word_graphList) {//因為bag of word 所以把每一段的文字都丟入同一序列
			ArrayList<HashMap<String, Double>> Parafinal_pagerank = new ArrayList<HashMap<String, Double>>();// 各個段落最終迭代後的pagerank值
			Parafinal_pagerank = process(para);
			final_pagerank.add(Parafinal_pagerank);
		}
	}
	public ArrayList<ArrayList<HashMap<String, Double>>> getPfinal_pagerank(){
		return final_pagerank;
	}
}
