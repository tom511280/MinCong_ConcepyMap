package Topic_Iteration;

import java.util.ArrayList;
import java.util.HashMap;
import Topic_Parameter.CParameter;

public class CTopic_Iteration extends Topic_Iteration{
	private ArrayList<HashMap<String, Double>> final_pagerank = new ArrayList<HashMap<String, Double>>();// 最終迭代後的pagerank值
	private HashMap<String, HashMap<String, Double>> word_graphList;//存放字圖
	public CTopic_Iteration(int k, CParameter cparameter){
		super(cparameter);
		this.word_graphList = cparameter.getCword_graph().getCWordGraph();
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		this.final_pagerank = process(word_graphList);
	}
	
	public ArrayList<HashMap<String, Double>> getCfinal_pagerank(){
		return final_pagerank;
	}
}
