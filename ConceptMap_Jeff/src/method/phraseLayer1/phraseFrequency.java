package method.phraseLayer1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import NER.demo.Entity;
import NER.demo.removeContainEntity;
import Preprocess.SortObject;
import Preprocess.StemWords;
import Preprocess.dataPreprocess;

public class phraseFrequency implements phraseScore{
	
	dataPreprocess preprocess = null;
	StemWords stem = new StemWords();
	private String allText = "";
	private ArrayList<Double> score = new ArrayList<Double>();//phrase的權重
	private Set<Entity> phrases = new LinkedHashSet<Entity>();
	private ArrayList<Entity> phrases2 = new ArrayList<Entity>();
	
	public phraseFrequency(dataPreprocess preprocess) {
		this.preprocess = preprocess;
		for(String paragraph : preprocess.getParagraph())
			this.allText = this.allText + paragraph + " ";
		this.allText = this.allText.trim();
		extractPhrase();
		weight();
	}	
	
	public void extractPhrase() {		
		for(ArrayList<Entity> parPN : preprocess.getPhrase_ner()){
			for(Entity pn : parPN){
				phrases.add(pn);
			}
		}
		//先依詞彙長度做排序
		ArrayList<SortObject> List = new ArrayList<SortObject>();
		for(Entity e: phrases){
			//System.out.println(e.entity+"!"+e.entity.length());
			List.add(new SortObject(e,e.entity.length()));			
		}
		Collections.sort(List, SortObject.Comparators.VALUE);//依照value(權重)進行排序
		for(SortObject object: List){
			phrases2.add(object.entity);
		}
		phrases2 = removeContainEntity.singleElement(phrases2);//刪除有出現在複合字的字詞
	}
	
	public void weight() {
		Integer[] numPhrase = new Integer[phrases2.size()];
		Arrays.fill(numPhrase, 0);
		allText = " " + allText;//為了處理首字元的問題要多加一空白
		for(int i=0; i<phrases2.size(); i++){
			int index = 0; String text = allText;
			while(index != text.length()){
				String newText = text.substring(index, text.length());
				if(newText.toLowerCase().indexOf(phrases2.get(i).entity.toLowerCase())>=0){					
					index = index + newText.toLowerCase().indexOf(phrases2.get(i).entity.toLowerCase()) + phrases2.get(i).entity.length();
					String startBlank = text.substring(index-phrases2.get(i).entity.length()-1, index-phrases2.get(i).entity.length());
					String endBlank = text.substring(index, index+1);
					if(startBlank.matches("[ (]") && endBlank.matches("[ .,!?)]"))
						numPhrase[i]++;
				}
				else
					index = text.length();
			}
			score.add((double)numPhrase[i]);
		}
	}
	
	@Override
	public ArrayList<Double> score() {
		return score;
	}

	@Override
	public Entity[] phrases() {
		Entity[] Phrase = phrases2.toArray(new Entity[0]);
		return Phrase;
	}

}
