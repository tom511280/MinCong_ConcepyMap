package method.phraseLayer1;

import java.util.ArrayList;

import NER.demo.Entity;

public interface phraseScore {	
	public abstract Entity[] phrases();
	public abstract ArrayList<Double> score();
}
