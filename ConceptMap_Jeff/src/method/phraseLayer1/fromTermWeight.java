package method.phraseLayer1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import NER.demo.Entity;
import NER.demo.removeContainEntity;
import Preprocess.SortObject;
import Preprocess.StemWords;
import Preprocess.dataPreprocess;

public class fromTermWeight implements phraseScore{
	
	dataPreprocess preprocess = null;
	StemWords stem = new StemWords();
	private ArrayList<Double> score = new ArrayList<Double>();//phrase的權重
	private ArrayList<Double> termScore = null;
	private Set<Entity> phrases = new LinkedHashSet<Entity>();
	private ArrayList<Entity> phrases2 = new ArrayList<Entity>();
	private ArrayList<String> words_hash = new ArrayList<String>();
	
	public fromTermWeight(dataPreprocess preprocess, ArrayList<Double> termScore) {
		this.preprocess = preprocess;
		this.words_hash = preprocess.getArticleWord_hash();
		this.termScore = termScore;
		extractPhrase();
		weight();
	}	
	
	public void extractPhrase() {		
		for(ArrayList<Entity> parPN : preprocess.getPhrase_ner()){
			for(Entity pn : parPN){
				phrases.add(pn);
			}
		}
		//先依詞彙長度做排序
		ArrayList<SortObject> List = new ArrayList<SortObject>();
		for(Entity e: phrases){
			//System.out.println(e.entity+"!"+e.entity.length());
			List.add(new SortObject(e,e.entity.length()));			
		}
		Collections.sort(List, SortObject.Comparators.VALUE);//依照value(權重)進行排序
		for(SortObject object: List){
			phrases2.add(object.entity);
		}
		phrases2 = removeContainEntity.singleElement(phrases2);//刪除有出現在複合字的字詞
	}
	
	public void weight() {
		double[] phraseScore = new double[phrases2.size()];	
		int i = 0;
		for(Entity entity: phrases2){			
			String[] phrase = entity.entity.split(" ");
			for(int j=0; j<phrase.length; j++){
				for(int k=0; k<words_hash.size(); k++){
					if(stem.Stem(phrase[j].toLowerCase()).equals(words_hash.get(k).toLowerCase())){
						phraseScore[i] = phraseScore[i] + termScore.get(k);			
					}
				}					
			}
			score.add(phraseScore[i]);
			i++;
		}
	}
	
	@Override
	public ArrayList<Double> score() {
		return score;
	}

	@Override
	public Entity[] phrases() {
		Entity[] Phrase = phrases2.toArray(new Entity[0]);
		return Phrase;
	}

}
