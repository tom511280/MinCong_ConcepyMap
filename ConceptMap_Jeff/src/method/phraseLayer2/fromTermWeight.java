package method.phraseLayer2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import NER.demo.Entity;
import NER.demo.Entity2;
import NER.demo.removeContainEntity;
import Preprocess.SortObject;
import Preprocess.StemWords;
import Preprocess.dataPreprocess;

public class fromTermWeight implements phraseScore{
	
	dataPreprocess preprocess;
	StemWords stem = new StemWords();
	private ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();//phrase的權重
	private ArrayList<ArrayList<Double>> termScore = null;
	private int paraNum;
	private ArrayList<ArrayList<String>> words_hash = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<Entity>> textToRegular = new ArrayList<ArrayList<Entity>>();
	private ArrayList<ArrayList<Entity>> textToNer = new ArrayList<ArrayList<Entity>>();
	
	public fromTermWeight(dataPreprocess preprocess, ArrayList<ArrayList<Double>> termScore) {
		this.preprocess = preprocess;
		this.termScore = termScore;
		this.paraNum = preprocess.getParaNum();//段落數
		this.words_hash = preprocess.getSentenceToWord_hash();
		this.textToRegular = preprocess.getTextToRegular();
		this.textToNer = preprocess.getTextToNer();
		phraseWeight();
	}	
	
	private void phraseWeight() {
		for(int i=0; i<paraNum; i++){
			ArrayList<Double> phraseScore = new ArrayList<Double>();
			ArrayList<Entity> Phrase_ner = preprocess.getPhrase_ner().get(i);//取得段落詞彙
			double[] PhraseScore = new double[Phrase_ner.size()];				
			for(int j=0; j<Phrase_ner.size(); j++){
				String[] phrase = Phrase_ner.get(j).entity.split(" ");	
				for(int k=0; k<phrase.length; k++){
					for(int l=0; l<words_hash.get(i).size(); l++){							
						if(stem.Stem(phrase[k].toLowerCase()).equals(stem.Stem(words_hash.get(i).get(l).toLowerCase()))){
							for(int m=0; m<textToRegular.get(i).size(); m++){
								if(Phrase_ner.get(j).entity.equals(textToRegular.get(i).get(m).entity))
									PhraseScore[j] = PhraseScore[j] + termScore.get(i).get(l);
							}
//							for(int m=0; m<textToNer.get(i).size(); m++){
//								if(Phrase_ner.get(j).entity.equals(textToNer.get(i).get(m).entity))
//									PhraseScore[j] = PhraseScore[j] + termScore.get(i).get(l);
//							}
						}							
					}						
				}
				phraseScore.add(PhraseScore[j]);
			}
			score.add(phraseScore);
		}
	}
	
	@Override
	public ArrayList<ArrayList<Double>> score() {
		return score;
	}
}
