package method.termLayer1;

import java.util.ArrayList;

import Preprocess.dataPreprocess;
import Preprocess.httpPost;
import Wordnet.Wordnet;

public class ASL_LC implements termScore{
	private ArrayList<Double> score = new ArrayList<Double>();//字的權重
	private ArrayList<Double> scoreLC = new ArrayList<Double>();//各段落lexical chain的權重
	private ArrayList<Double> scoreASL = new ArrayList<Double>();//各段落verbArgStructure的權重
	dataPreprocess preprocess = null;
	
	public ASL_LC(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		combine();
	}	
	
	private void combine(){	
		httpPost http = new httpPost();
		scoreASL = http.verbArgScore2(preprocess);
		
		Wordnet wordnet = new Wordnet();	
		scoreLC = wordnet.wordnetScore2(preprocess);

		scoreCombine();
	}
	
	private void scoreCombine() {	
		//將各段落的字在動詞參數結構與wordnet做分數加總  scoreASL.get(i) + scoreLC.get(i)
		for(int i=0; i<scoreASL.size(); i++){
			score.add(scoreASL.get(i) + scoreLC.get(i));
		}
	}

	@Override
	public ArrayList<Double> score() {
		return score;
	}

}
