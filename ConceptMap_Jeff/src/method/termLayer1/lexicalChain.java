package method.termLayer1;

import java.util.ArrayList;

import Preprocess.dataPreprocess;
import Wordnet.Wordnet;

public class lexicalChain implements termScore{
	private ArrayList<Double> score = new ArrayList<Double>();//字的權重
	dataPreprocess preprocess = null;
	
	public lexicalChain(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		wordnet();
	}	

	public void wordnet(){		
		Wordnet wordnet = new Wordnet();	
		score = wordnet.wordnetScore2(preprocess);
	}

	@Override
	public ArrayList<Double> score() {		
		return score;
	}
}
