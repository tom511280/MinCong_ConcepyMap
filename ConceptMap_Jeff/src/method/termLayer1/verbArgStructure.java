package method.termLayer1;

import java.util.ArrayList;
import Preprocess.dataPreprocess;
import Preprocess.httpPost;

public class verbArgStructure implements termScore{
	private ArrayList<Double> score = new ArrayList<Double>();//整天文章字的權重
	dataPreprocess preprocess = null;
	
	public verbArgStructure(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		ASL();
	}	

	public void ASL(){		
		httpPost http = new httpPost();
		score = http.verbArgScore2(preprocess);
	}

	@Override
	public ArrayList<Double> score() {		
		return score;
	}
}
