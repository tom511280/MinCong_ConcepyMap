package method.termLayer1;

import java.util.ArrayList;
import java.util.Collections;

import Preprocess.dataPreprocess;

public class termFrequency implements termScore{
	private ArrayList<String> words = new ArrayList<String>();
	private ArrayList<String> words_hash = new ArrayList<String>();
	private Integer parNum;
	private double[] num = null;//出現次數
	private ArrayList<Double> score = new ArrayList<Double>();//字的權重
	dataPreprocess preprocess = null;
	
	public termFrequency(dataPreprocess preprocess) {
		this.parNum = preprocess.getParaNum();
		this.words = preprocess.getArticleWord();
		this.words_hash = preprocess.getArticleWord_hash();
		this.preprocess = preprocess;
		
		num = new double[this.words_hash.size()];
		frequency();
	}
	
	public termFrequency(ArrayList<String> words_hash, ArrayList<String> words, Integer parNum){
		this.parNum = parNum;
		this.words = words;
		this.words_hash = words_hash;
		num = new double[words_hash.size()];
		frequency();
	}	

	public void frequency(){		
		for (int i = 0; i < words_hash.size(); i++) {
			num[i] = Collections.frequency(words, words_hash.get(i));//傳回第二個引數在集合中出現的次數
			//score.add(num[i] / (parNum-1));//此不考慮段落title,因此-1
			score.add(num[i]);
		}
	}
	
	public void TF(){
		for (int i = 0; i < words_hash.size(); i++) {
			double count = 0;
			for (int j = 0; j < words.size(); j++) {
				if (words_hash.get(i).equals(words.get(j))) {
					count++;
				}
			}
			num[i] = count ;
			score.add(num[i] / (parNum-1));
		}
	}

	@Override
	public ArrayList<Double> score() {		
		return score;
	}
}
