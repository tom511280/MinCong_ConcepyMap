package method.termLayer1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class tfProcess {
	private ArrayList<ArrayList<String>> sentenceToWord = null;
	private List<List<String>> newTextToSentence = new ArrayList<List<String>>();
	private List<String> newParagraph = new ArrayList<String>();//用來儲存每一個段落的內容
	private Set<String> processWords  = new LinkedHashSet<String>();//存放需過濾"-"的字
	private Set<String> processWords2  = new LinkedHashSet<String>();//存放需過濾轉大小寫的字
	private double[] num = null;//出現次數
	private double[] num2 = null;//出現次數
	private double[] num3 = null;//出現次數

	//此類別方法是為了處理有"-"連接字。以頻率來找,若有"-"的頻率小於已刪除掉的,則將此字的"-"刪除
	public tfProcess(ArrayList<ArrayList<String>> sentenceToWord) {
		this.sentenceToWord = sentenceToWord;
		ArrayList<String> articleWords = new ArrayList<String>();
		ArrayList<String> articleWords2 = new ArrayList<String>();
		ArrayList<String> articleWords3 = new ArrayList<String>();
		
		for(ArrayList<String> words : sentenceToWord){
			articleWords.addAll(words);
			for(String word : words){
				articleWords2.add(word.replace("-", ""));
				articleWords3.add(word.toLowerCase());
			}
		}
		num = new double[articleWords.size()];
		num2 = new double[articleWords.size()];
		num3 = new double[articleWords.size()];
		
		for (int i = 0; i < articleWords.size(); i++) {
			num[i] = Collections.frequency(articleWords, articleWords.get(i));//傳回第二個引數在集合中出現的次數
			num2[i] = Collections.frequency(articleWords2, articleWords.get(i));//為了處理Tai-pei或Williams-port的問題
			num3[i] = Collections.frequency(articleWords3, articleWords.get(i));
			if(num2[i]>num[i]){
				processWords.add(articleWords.get(i));
			}
			if(num3[i]>num[i]){
				processWords2.add(articleWords.get(i));
			}
		}
		replaceSameWords();
	}
	
	public void replaceSameWords(){
		for(ArrayList<String> parWords : sentenceToWord){
			for(int i=0; i<parWords.size();i++){
				for(String pwords : processWords){
					if(parWords.get(i).replace("-", "").equals(pwords)){
						parWords.set(i, parWords.get(i).replace("-", ""));
					}
				}
				for(String pwords : processWords2){
					if(parWords.get(i).toLowerCase().equals(pwords)){
						parWords.set(i, parWords.get(i).toLowerCase());
					}
				}
			}			
		}		
	}
	
	public void replaceParagraph(List<List<String>> textToSentence){
		for(List<String> parSentence : textToSentence){
			List<String> newParSentence = new ArrayList<String>();
			String par = "";
			for(String sentence : parSentence){
				String[] words = sentence.split(" ");
				String newSentence = "";
				for(int i=0; i<words.length;i++){
					for(String pwords : processWords){
						if(words[i].replace("-", "").indexOf(pwords)>=0){
							words[i] = words[i].replace("-", "");							
						}						
					}
					newSentence = newSentence + words[i] +" ";
				}
				newParSentence.add(newSentence.trim());
				par = par + newSentence.trim() + " ";
			}
			newParagraph.add(par.trim());
			newTextToSentence.add(newParSentence);
		}
	}

	public ArrayList<ArrayList<String>> getSentenceToWord() {
		return sentenceToWord;
	}
	public void setSentenceToWord(ArrayList<ArrayList<String>> sentenceToWord) {
		this.sentenceToWord = sentenceToWord;
	}

	public List<List<String>> getNewTextToSentence() {
		return newTextToSentence;
	}
	public void setNewTextToSentence(List<List<String>> newTextToSentence) {
		this.newTextToSentence = newTextToSentence;
	}

	public List<String> getNewParagraph() {
		return newParagraph;
	}
	public void setNewParagraph(List<String> newParagraph) {
		this.newParagraph = newParagraph;
	}
}
