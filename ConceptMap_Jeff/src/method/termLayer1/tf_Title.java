package method.termLayer1;

import java.util.ArrayList;
import java.util.Collections;

import Preprocess.dataPreprocess;
//tf+title
public class tf_Title implements termScore{
	private ArrayList<String> title_word = new ArrayList<String>();
	private ArrayList<String> words_hash = new ArrayList<String>();
	private ArrayList<Double> title_score = new ArrayList<Double>();//字在title的權重
	private ArrayList<Double> tf_score  = new ArrayList<Double>();//字在整篇文章頻率的權重
	private ArrayList<Double> score = new ArrayList<Double>();//字在整篇文章頻率的權重
	dataPreprocess preprocess;
	
	public tf_Title(dataPreprocess preprocess) {
		this.title_word = preprocess.getSentenceToWord().get(0);
		this.words_hash = preprocess.getArticleWord_hash();
		this.preprocess = preprocess;
		termFrequency tf = new termFrequency(preprocess);
		tf_score = tf.score();
		appearInTitle();
	}
	
	public tf_Title(ArrayList<String> title_word, ArrayList<String> words_hash) {
		this.title_word = title_word;
		this.words_hash = words_hash;	
		appearInTitle();
	}

	//從文章中所有字已去除stop words與做stemming的字詞,找尋是否有出現在文章title中,若有出現權重為1,若無則為0
	public void appearInTitle(){
		for(int i=0;i<words_hash.size();i++){
			int frequency = Collections.frequency(title_word, words_hash.get(i));
			if(frequency>0)
				title_score.add((double) 1);
			else
				title_score.add((double) 0);
		}
	}

	@Override
	public ArrayList<Double> score() {
		//字的頻率與是否出現於title的分數加總
		for(int i=0; i<words_hash.size(); i++){
			score.add(tf_score.get(i) / (preprocess.paraNum-1) + title_score.get(i));
		}
		return score;
	}	
}
