package method.termLayer2;

import java.util.ArrayList;

import Preprocess.dataPreprocess;
import Preprocess.httpPost;
import Wordnet.Wordnet;

public class tf_ASL implements termScore{
	private ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();//各段落字的權重
	private ArrayList<ArrayList<Double>> scoreLC = new ArrayList<ArrayList<Double>>();//各段落lexical chain的權重
	private ArrayList<ArrayList<Double>> scoreASL = new ArrayList<ArrayList<Double>>();//各段落verbArgStructure的權重
	private ArrayList<ArrayList<Double>> scoreTF = new ArrayList<ArrayList<Double>>();
	dataPreprocess preprocess = null;
	
	public tf_ASL(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		combine();
	}	
	
	private void combine(){
		termFrequency tf = new termFrequency(preprocess);
		scoreTF = tf.score();
				
		httpPost http = new httpPost();
		scoreASL = http.verbArgScore(preprocess);
		
		
//		Wordnet wordnet = new Wordnet();	
//		scoreLC = wordnet.wordnetScore(preprocess);

		scoreCombine();
	}
	
	private void scoreCombine() {	
		//將各段落的字在動詞參數結構與wordnet做分數加總  scoreASL.get(i).get(j) + scoreLC.get(i).get(j)
		for(int i=0; i<scoreASL.size(); i++){
			ArrayList<Double> score2 = new ArrayList<Double>();
			for(int j=0; j<scoreASL.get(i).size();j++){
				score2.add(scoreASL.get(i).get(j) + scoreTF.get(i).get(j));
				System.out.println(scoreASL.get(i).get(j)+"!"+scoreTF.get(i).get(j));
			}
			score.add(score2);
		}
	}

	@Override
	public ArrayList<ArrayList<Double>> score() {
		return score;
	}

}
