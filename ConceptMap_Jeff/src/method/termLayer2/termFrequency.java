package method.termLayer2;

import java.util.ArrayList;
import java.util.Collections;

import Preprocess.dataPreprocess;

public class termFrequency implements termScore{
	private Integer parNum;
	private ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();//各段落字的權重
	dataPreprocess preprocess = null;
	
	public termFrequency(dataPreprocess preprocess) {
		this.parNum = preprocess.getParaNum();
		this.preprocess = preprocess;
		
		frequency();
	}	

	public void frequency(){
		for(int i=0; i<parNum; i++){
			ArrayList<String> words = preprocess.getSentenceToWord().get(i);
			ArrayList<String> words_hash = preprocess.getSentenceToWord_hash().get(i);
			ArrayList<Double> score2 = new ArrayList<Double>();
			double[] num = new double[words_hash.size()];
			int sentenceNum = preprocess.getTextToSentence().get(i).size();
			for (int j = 0; j < words_hash.size(); j++) {
				num[j] = Collections.frequency(words, words_hash.get(j));//傳回第二個引數在集合中出現的次數
				score2.add(num[j] / sentenceNum);//此不考慮段落title,因此-1
			}
			score.add(score2);
		}
	}

	@Override
	public ArrayList<ArrayList<Double>> score() {
		return score;
	}
}
