package method.termLayer2;

import java.util.ArrayList;
import Preprocess.dataPreprocess;
import Preprocess.httpPost;

public class verbArgStructure implements termScore{
	private ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();//各段落字的權重
	dataPreprocess preprocess = null;
	
	public verbArgStructure(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		ASL();
	}	

	public void ASL(){		
		httpPost http = new httpPost();
		score = http.verbArgScore(preprocess);
	}

	@Override
	public ArrayList<ArrayList<Double>> score() {		
		return score;
	}
}
