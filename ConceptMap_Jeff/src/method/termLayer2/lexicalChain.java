package method.termLayer2;

import java.util.ArrayList;
import Preprocess.dataPreprocess;
import Wordnet.Wordnet;

public class lexicalChain implements termScore{
	private ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();//各段落字的權重
	dataPreprocess preprocess = null;
	
	public lexicalChain(dataPreprocess preprocess) {
		this.preprocess = preprocess;	
		wordnet();
	}	

	public void wordnet(){		
		Wordnet wordnet = new Wordnet();	
		score = wordnet.wordnetScore(preprocess);
	}

	@Override
	public ArrayList<ArrayList<Double>> score() {		
		return score;
	}
}
