package Topic_keyphraseExtract;

import java.util.ArrayList;
import java.util.HashMap;

import org.tartarus.snowball.ext.PorterStemmer;

import NER.demo.Entity;
import Preprocess.dataPreprocess;
import Topic_Parameter.Parameter;

abstract public class Topic_keyphraseExtract {
	public dataPreprocess datapreprocess;
	public ArrayList<Double>document_topic = new ArrayList<Double>();//文檔內的主題分布
	@SuppressWarnings("unchecked")
	public Topic_keyphraseExtract(Parameter parameter){
		HashMap<Integer,ArrayList<Double>>Alldocument_topic = (HashMap<Integer,ArrayList<Double>>) parameter.getLDA_Model().getDocument_Topic().getProbability();
		document_topic = Alldocument_topic.get(1);//因為是單篇所以只取第一篇文章的主題分布
		datapreprocess = parameter.getdataPreprocess();
	}
	public ArrayList<HashMap<String, Double>> topic_phraseweight(ArrayList<HashMap<String, Double>> final_pagerank_hash) {
	//存放單字在每個主題的權重分數
	ArrayList<HashMap<String, Double>> final_pagerank_keyphrase = new ArrayList<HashMap<String, Double>>();
    //存放短語在每個主題的權重分數
//	ArrayList<HashMap<String, Double>> final_pagerank_hash = new ArrayList<HashMap<String, Double>>();
		for(int i = 0;i < final_pagerank_hash.size();i++)//每個主題
		{
		  HashMap<String, Double> keyphrase_hash = new HashMap<String, Double>();//keyphrase在主題內的重要性分數
		  for(ArrayList<Entity>key_array : datapreprocess.getTextToRegular())//計算phrase在每個主題內的重要性
			 {
				 for(Entity key :  key_array)
				 {
					 if(!keyphrase_hash.containsKey(key.entity))
					 keyphrase_hash.put(key.entity, (double) 0);//給予phrase權重初始值
					  
					 String tmp[] = null;
					 tmp = key.entity.split(" ");//切開phrase裡的單字
					 double weight = 0;
					 for(int j = 0; j < tmp.length; j++)
					 {
						 //使用波特詞幹還原phrase內的單字
						 PorterStemmer porterstemmer = new PorterStemmer();
						 porterstemmer.setCurrent(tmp[j].trim().replaceAll("\\pP", "").toLowerCase());
						 porterstemmer.stem();
						 String word = porterstemmer.getCurrent();
					
						//把phrase內每個字的權重加總
						 if(final_pagerank_hash.get(i).containsKey(word))
						 {
						     weight =  keyphrase_hash.get(key.entity)+final_pagerank_hash.get(i).get(word);
						     //System.out.println(weight);
						 }
						 keyphrase_hash.put(key.entity, weight);
					 }
				 }
			 }
		  final_pagerank_keyphrase.add(keyphrase_hash);
		}
		return final_pagerank_keyphrase;
	}
	public HashMap<String, Double> summary(ArrayList<HashMap<String, Double>> final_pagerank_keyphrase, HashMap<String, Double> keyphrase) {
//		 ArrayList<HashMap<String, Double>> final_pagerank_keyphrase = new ArrayList<HashMap<String, Double>>();
		//*********************************  
		  for(String key : keyphrase.keySet())
		  {
			  double weight = 0;
			  for(int i = 0;i < final_pagerank_keyphrase.size();i++)
			  {
				  weight+=final_pagerank_keyphrase.get(i).get(key)*document_topic.get(i);
			  }
			  keyphrase.put(key, weight);
		  }
		return keyphrase; 
	  }
	
	public HashMap<String, Double> initialize() {//給予初值
		// TODO Auto-generated method stub
		//找出不重複phrase 並給予初值
		 HashMap<String, Double> keyphrase = new HashMap<String, Double>();//keyphrase 總分
		 for(ArrayList<Entity>key_array : datapreprocess.getTextToRegular())//每個段落
		 {
			 for(Entity key :  key_array)
			 {
				 if(!keyphrase.containsKey(key.entity))//如果不重複hash裡不包含這個phrase 就給予phrase權重初始值
					 keyphrase.put(key.entity, (double) 0);
			 }
		 }
		return keyphrase;
	}
    abstract public void execute();
}
