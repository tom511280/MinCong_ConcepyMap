package Topic_keyphraseExtract;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import Topic_MethodMenu.GR;
import Topic_Parameter.PParameter;

public class PTopic_keyphraseExtract extends Topic_keyphraseExtract{
	private ArrayList<HashMap<String, Double>> keyphrase = new ArrayList<HashMap<String, Double>>();//各個段落內keyphrase的總分
	private ArrayList<ArrayList<HashMap<String, Double>>> final_pagerank_hash = new ArrayList<ArrayList<HashMap<String, Double>>>();//各個段落內存放單字在每個主題的權重分數 from Topic_Iteration
	private ArrayList<HashMap<String, Double>> final_pagerank_keyphrase = new ArrayList<HashMap<String, Double>>();//各個段落內存放短語在每個主題的權重分數

	
	public PTopic_keyphraseExtract(PParameter pparameter){
		super(pparameter);
		this.final_pagerank_hash = pparameter.getTopic_Iteration().getPfinal_pagerank();
	}
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		for(ArrayList<HashMap<String, Double>> para : final_pagerank_hash){
			HashMap<String, Double> Parakeyphrase = new HashMap<String, Double>();//段落keyphrase總分
			Parakeyphrase = initialize();
			this.final_pagerank_keyphrase = topic_phraseweight(para);
			Parakeyphrase = summary(final_pagerank_keyphrase, Parakeyphrase);
			keyphrase.add(Parakeyphrase);
			
//			 Google_relation google_relation = new Google_relation(keyphrase,Searchcount,IpNo);
//			  Searchcount = google_relation.getSearchcount();
//			  IpNo = google_relation.getIpNo();
//			  Pkeyphrase.add(google_relation.getrdefine());
		}
	}
	public void TGPRexecute() throws IOException, InterruptedException {
		int Searchcount = 1,IpNo = 0;
		for(ArrayList<HashMap<String, Double>> para : final_pagerank_hash){
			HashMap<String, Double> Parakeyphrase = new HashMap<String, Double>();//段落keyphrase總分
			Parakeyphrase = initialize();
			this.final_pagerank_keyphrase = topic_phraseweight(para);
			Parakeyphrase = summary(final_pagerank_keyphrase, Parakeyphrase);
			
			//TGPR運算
			GR google_relation = new GR(Parakeyphrase,Searchcount,IpNo);
			Searchcount = google_relation.getSearchcount();
			IpNo = google_relation.getIpNo();
			keyphrase.add(google_relation.getrdefine());
		}
	}
	public ArrayList<HashMap<String, Double>> getPkeyphrase(){
		return keyphrase;
	}
}
