package Topic_keyphraseExtract;

import java.util.ArrayList;
import java.util.HashMap;

import org.tartarus.snowball.ext.PorterStemmer;

import NER.demo.Entity;
import Topic_Parameter.CParameter;

public class CTopic_keyphraseExtract extends Topic_keyphraseExtract{
	private HashMap<String, Double> keyphrase = new HashMap<String, Double>();//keyphrase 總分
	private ArrayList<HashMap<String, Double>> final_pagerank_hash = new ArrayList<HashMap<String, Double>>();//存放單字在每個主題的權重分數
	private ArrayList<HashMap<String, Double>> final_pagerank_keyphrase = new ArrayList<HashMap<String, Double>>();//存放短語在每個主題的權重分數
	public CTopic_keyphraseExtract(CParameter cparameter){
		super(cparameter);
		this.final_pagerank_hash = cparameter.getCTopic_Iteration().getCfinal_pagerank();
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		this.keyphrase = initialize();
		this.final_pagerank_keyphrase = topic_phraseweight(final_pagerank_hash);
		this.keyphrase = summary(final_pagerank_keyphrase, keyphrase);
	}
	
	public HashMap<String, Double> getCkeyphrase(){
		return keyphrase;
	}
}
