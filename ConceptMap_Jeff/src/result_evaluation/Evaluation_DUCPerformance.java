package result_evaluation;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;

import object.Keyphrase;
import object.Metrics;
import object.Read_object;
import file_read.Facade_DUCGolden;
import file_read.Facade_System;

public class Evaluation_DUCPerformance extends Evaluation_Base implements Evaluation_Interface{
	private HashMap<String,ArrayList<Keyphrase>> system_keyphrase = new HashMap<String,ArrayList<Keyphrase>>();
	private HashMap<String,ArrayList<Keyphrase>> golden_keyphrase = new HashMap<String,ArrayList<Keyphrase>>();
	private HashMap<String,Metrics> metrics_list = new HashMap<String,Metrics>();
	public Evaluation_DUCPerformance(Read_object read_Sobject,Read_object read_Gobject,int M) throws IOException
	{
		//設定路徑
//		this.Goldenpath = read_Gobject.getpath();
//		this.SystemFolder = read_Sobject.getFolderPath();
		this.M = M;
		
		//系統
		Facade_System facade_system = new Facade_System(read_Sobject);
		system_keyphrase = facade_system.getResult();
		
		//stem
		for(String article : system_keyphrase.keySet()){
			system_keyphrase.put(article, Evaluation_Stem(system_keyphrase.get(article)));
		}
		
		//人工
		Facade_DUCGolden facade_ducgolden = new Facade_DUCGolden(read_Gobject);
		golden_keyphrase = facade_ducgolden.getResult();
		
		//stem
		for(String article : golden_keyphrase.keySet()){
			golden_keyphrase.put(article, Evaluation_Stem(golden_keyphrase.get(article)));
		}
		
		//計算指標
		for(String article : system_keyphrase.keySet()){
			System.out.println(article+"***************************");
			metrics_list.put(article,Calculate(system_keyphrase.get(article),
					golden_keyphrase.get(article),M));
		}	
	}
	@Override
	public void Show_result() {
		for(String article : metrics_list.keySet()){
			System.out.println(article+": "+metrics_list.get(article).getPercision());
			System.out.println(article+": "+metrics_list.get(article).getRecall());
			System.out.println(article+": "+metrics_list.get(article).getF1measure());
			
			total_Precision+= metrics_list.get(article).getPercision();
			total_Recall+= metrics_list.get(article).getRecall();
			total_F1_measure+= metrics_list.get(article).getF1measure();
		}
		total_Precision= (double)(total_Precision/metrics_list.size());
		total_Recall= (double)(total_Recall/metrics_list.size());
		total_F1_measure= (double)(total_F1_measure/metrics_list.size());
		
		System.out.println("total_Precision: "+total_Precision);
		System.out.println("total_Recall: "+total_Recall);
		System.out.println("total_F1_measure: "+total_F1_measure);
	}
}
