package result_evaluation;

import java.util.ArrayList;
import java.util.HashMap;

import file_read.Facade_DUCGolden;
import file_read.Facade_Para_PAOGolden;
import file_read.Facade_Para_System;
import file_read.Facade_System;
import object.Keyphrase;
import object.Metrics;
import object.Read_object;

public class Evaluation_PAOPerformance extends Evaluation_Base implements Evaluation_Interface{
	
	private HashMap<String,HashMap<Integer,Metrics>> metrics_list = new HashMap<String,HashMap<Integer,Metrics>>();
	private HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>> system_keyphrase = new HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>>();
	private HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>> golden_keyphrase = new HashMap<String,HashMap<Integer,ArrayList<Keyphrase>>>();
	public Evaluation_PAOPerformance(Read_object read_Sobject,Read_object read_Gobject,int M)
	{
		this.M = M;
		
		//系統
		Facade_Para_System facade_para_system = new Facade_Para_System(read_Sobject);
		system_keyphrase = facade_para_system.getResult();

		
		//stem
		for(String article : system_keyphrase.keySet()){
			for(int para : system_keyphrase.get(article).keySet())
			{
				system_keyphrase.get(article).put(para, Evaluation_Stem(system_keyphrase.get(article).get(para)));
			}	
		}
				
		
		//人工
		Facade_Para_PAOGolden facade_paogolden = new Facade_Para_PAOGolden(read_Gobject);
		golden_keyphrase = facade_paogolden.getResult();
			
		
		//stem
		for(String article : golden_keyphrase.keySet()){
			for(int para : golden_keyphrase.get(article).keySet())
			{
				golden_keyphrase.get(article).put(para, Evaluation_Stem(golden_keyphrase.get(article).get(para)));
			}	
		}
				
		//計算指標
		for(String article : system_keyphrase.keySet())
		{
			System.out.println(article+"**************");
			HashMap<Integer,Metrics>single_article = new HashMap<Integer,Metrics>();
			for(int para : system_keyphrase.get(article).keySet())
			{
				System.out.println(para+"**************");
				single_article.put(para,Calculate(system_keyphrase.get(article).get(para),
						golden_keyphrase.get(article).get(para),M));
			}	
			metrics_list.put(article,single_article);
		}	
	}

	@Override
	public void Show_result() {
		int totalpara = 0;
	
		
		for(String article : metrics_list.keySet()){
			double article_P = 0;
			double article_r = 0;
			double article_f = 0;
			int paracount = 0;
			System.out.println("**********文章: "+article+"*********");
			for(int para : metrics_list.get(article).keySet())
			{
			totalpara++;
			paracount++;
			System.out.println("*****第"+para+"段*****"+"Percision: "+metrics_list.get(article).get(para).getPercision());
//			System.out.println("*****第"+para+"段*****"+"Recall: "+metrics_list.get(article).get(para).getRecall());
//			System.out.println("*****第"+para+"段*****"+"F1measure: "+metrics_list.get(article).get(para).getF1measure());
//			
			total_Precision+= metrics_list.get(article).get(para).getPercision();
			total_Recall+= metrics_list.get(article).get(para).getRecall();
			total_F1_measure+= metrics_list.get(article).get(para).getF1measure();
			article_P+= metrics_list.get(article).get(para).getPercision();
			article_r+= metrics_list.get(article).get(para).getRecall();
			article_f+= metrics_list.get(article).get(para).getF1measure();
			}
			System.out.println("article_Precision: "+(double)(article_P/paracount));
			System.out.println("article_Precision: "+(double)(article_r/paracount));
			System.out.println("article_Precision: "+(double)(article_f/paracount));
		}
		total_Precision= (double)(total_Precision/totalpara);
		total_Recall= (double)(total_Recall/totalpara);
		total_F1_measure= (double)(total_F1_measure/totalpara);
//		
		System.out.println("total_Precision: "+total_Precision);
		System.out.println("total_Recall: "+total_Recall);
		System.out.println("total_F1_measure: "+total_F1_measure);
//		
	}
}
