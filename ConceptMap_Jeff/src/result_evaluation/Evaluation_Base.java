package result_evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import org.tartarus.snowball.ext.PorterStemmer;
import object.Keyphrase;
import object.Metrics;

public class Evaluation_Base {
//	public String SystemFolder="";
//	public String GoldenFolder="";//適用於標準答案同在不同txt檔
//	public String Goldenpath="";//適用於標準答案都存在同一個txt檔
	
	
	public double total_Precision = 0;
	public double total_Recall = 0;
	public double total_F1_measure = 0;
	
	public int M = 0;
	public ArrayList<Keyphrase> Evaluation_Stem(ArrayList<Keyphrase>keyphrase_list)
	{
		for(Keyphrase keyphrase : keyphrase_list)
		{
			String[]word = keyphrase.getOr().split(" ");//切割
			String phrase = "";
			for(int i = 0;i < word.length;i++)
			{
				PorterStemmer porterstemmer = new PorterStemmer();//波特詞幹物件
				porterstemmer.setCurrent(word[i].trim()
						.replaceAll("\\pP", "").toLowerCase());
				porterstemmer.stem();
				phrase += porterstemmer.getCurrent() + " ";
				
			}
			keyphrase.setCl(phrase);
			
		}
		return keyphrase_list;
	}
	public Metrics Calculate(ArrayList<Keyphrase>system_list,ArrayList<Keyphrase>golden_list,int M)
	{
		
		Metrics metrics = new Metrics();
		int match = 0;
		int number_count = 0;
		for(Keyphrase Skeyphrase : system_list)
		{
			for(Keyphrase Kkeyphrase : golden_list)
			{
				if(Skeyphrase.getCl().equals(Kkeyphrase.getCl()) && number_count<M)
				{
					System.out.println(Skeyphrase.getCl()+" "+Kkeyphrase.getCl());
					match++;
					break;
				}
			}
			number_count++;
	    }
		metrics.setC_M((double)match);//對的關鍵詞數量
		metrics.setG_M((double)golden_list.size());//人工給定的關鍵詞數量
		metrics.setH_M(M);//人工設定取幾個比較
		
		return metrics;
	}
}
