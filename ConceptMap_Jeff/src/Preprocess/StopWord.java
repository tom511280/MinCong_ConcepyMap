package Preprocess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class StopWord {
	private ArrayList<String> stopwordlist = new ArrayList<String>();//用來儲存每一個stopword的內容
	final File file = new File("RelatedDocuments\\Stopwordlist.csv");//stopword檔案路徑
	
	public StopWord(){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String str = null;			
			while ((str = br.readLine()) != null) {
				stopwordlist.add(str);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.setStopwordlist(stopwordlist);
	}
	
	//removing stop word 和標點符號
	public ArrayList<String> removeStopWord(ArrayList<String> text) throws IOException {

		ArrayList<String> stopword = new ArrayList<String>();

		String punmark = "\\p{Punct}";
		String punnumber = "\\p{Digit}";
		for (int i = 0; i < text.size(); i++) {
			if(text.get(i).indexOf(punmark)>=0 || text.get(i).indexOf(punnumber)>=0){
				text.set(i, text.get(i).replaceAll(punmark, ""));
				text.set(i, text.get(i).replaceAll(punnumber, ""));
			}
			if (!text.get(i).equals("") && !this.stopwordlist.contains(text.get(i).toLowerCase())) { // stopword去除
				stopword.add(text.get(i));
			}
		}
		return stopword;
	}
	
	public ArrayList<ArrayList<String>> removeStopWord2(ArrayList<ArrayList<String>> text) {
		ArrayList<ArrayList<String>> stopword2 = new ArrayList<ArrayList<String>>();
		String punmark = "\\p{Punct}";
		String punnumber = "\\p{Digit}";
		for (ArrayList<String> words : text) {
			ArrayList<String> stopword = new ArrayList<String>();
			for (int i = 0; i < words.size(); i++) {
				if(words.get(i).indexOf(punmark)>=0 || words.get(i).indexOf(punnumber)>=0){
					words.set(i, words.get(i).replaceAll(punmark, ""));
					words.set(i, words.get(i).replaceAll(punnumber, ""));
				}
				if (!words.get(i).equals("") && !this.stopwordlist.contains(words.get(i).toLowerCase())) { // stopword去除
					stopword.add(words.get(i));
				}
			}
			stopword2.add(stopword);
		}
		return stopword2;
	}
	
	public ArrayList<String> getStopwordlist() {
		return stopwordlist;
	}

	public void setStopwordlist(ArrayList<String> stopwordlist) {
		this.stopwordlist = stopwordlist;
	}

	

}
