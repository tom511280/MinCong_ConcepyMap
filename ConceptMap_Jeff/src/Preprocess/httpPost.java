package Preprocess;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.Consts;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.*;

public class httpPost {

	/**
	 * @param args
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	private StemWords stem = new StemWords();
	private File stopFile = new File("RelatedDocument\\Stopwordlist.csv");
	private List<String> stopList = new ArrayList<String>();
	private List<String> tagWord = new ArrayList<String>(); // 原始的word
	private List<String> tagWord2 = new ArrayList<String>(); // 己經前處理的word																
	private List<Integer> countWord = new ArrayList<Integer>(); // 原始的word
	private List<String> count_word = new ArrayList<String>();

	private List<String> TagNoun = new ArrayList<String>();
	StopWord stopword = new StopWord();

	public ArrayList<String> httpData(String sentence) {
		TagNoun.clear();
		List<String> NounWord = new ArrayList<String>();
		List<String> NounWord2 = new ArrayList<String>();
		List<Integer> Nounindex = new ArrayList<Integer>();
		List<String> Arg = new ArrayList<String>();
		List<List<String>> Explanation = new ArrayList<List<String>>();
		ArrayList<String> Argument = new ArrayList<String>();
		
		NounWord2.clear();
		try {
			// TODO Auto-generated method stub
			List<String> tag = new ArrayList<String>();
			List<String> word = new ArrayList<String>();
			List<String> tagWord = new ArrayList<String>();
			stopList = stopword.getStopwordlist();			

			/*保留名詞 */
			// Shallow Parser (Chunker) Demo Results
			String Noun_url = "http://cogcomp.cs.illinois.edu/demo/shallowparse/results.php";
			HttpPost httpPost = new HttpPost(Noun_url);
			List<NameValuePair> param = new ArrayList<NameValuePair>();
			param.add(new BasicNameValuePair("text", sentence));
			httpPost.setEntity(new UrlEncodedFormEntity(param, Consts.UTF_8));
			HttpResponse resp2 = new DefaultHttpClient().execute(httpPost);

			String noun_Result = EntityUtils.toString(resp2.getEntity(), "utf-8");
			Document doc2 = Jsoup.parse(noun_Result);
			Elements noun_table = doc2.select("div[id=results]");
			Elements trs2 = noun_table.select("span[class=token NP]");
			for (Element link : trs2) {
				NounWord.add(link.text().trim());
			}	

			String url = "http://cogcomp.cs.illinois.edu/demo/srl/results.php";
			HttpPost httpPost2 = new HttpPost(url);	
			List<NameValuePair> param2 = new ArrayList<NameValuePair>();
			param2.add(new BasicNameValuePair("text", sentence));
			httpPost2.setEntity(new UrlEncodedFormEntity(param2, Consts.UTF_8));
			HttpResponse resp = new DefaultHttpClient().execute(httpPost2);
			
			String updateResult = EntityUtils.toString(resp.getEntity(), "utf-8");		
			Document doc = Jsoup.parse(updateResult);
			Elements table = doc.select("table[id=disp]");
			Elements trs = table.select("tr");
			Elements name = table.select("td[col^=srl]");			
			Elements table2 = doc.select("table[id=tokens]");
			Elements td2 = table2.select("td");
			
			int line = 0;
			for (Element link : td2) {
				word.add(link.text());
				line++;
			}
			
			Iterator trIter = trs.iterator();
			boolean firstRow = true;
			int trNo = 1;
			int tdMax = 0;
			while (trIter.hasNext()) {
				Element tr = (Element) trIter.next();
				if (firstRow) {
					firstRow = false;
					continue;
				}
				Elements tds = tr.select("td[col^=srl]");
				
				Iterator tdIter = tds.iterator();
				int tdCount = 1;				
				String country = null;
				Pattern pattern = Pattern.compile(".*\\[");				
				String labels = "";
				// process new line
				while (tdIter.hasNext()) {
					Element td = (Element) tdIter.next();
					String num = td.attr("col").replace("srl", "");
					
					tdCount = Integer.parseInt(num);
					if(tdCount>tdMax)
						tdMax = tdCount;
					if (!td.select("a").text().equals("")) {
						Matcher matcher = pattern.matcher(td.select("a").text());
						if(matcher.find())
							labels = matcher.replaceAll("").replaceAll("]", "");
						else
							labels = "V";

						int k = Integer.parseInt(td.attr("rowspan"));
						if (k == 1) {						
							country = word.get(trNo);							
						} else {
							int st = k + trNo;							
							String fianl = "";
							for (int i = trNo; i < st; i++) {
								fianl = fianl + word.get(i) + " ";
							}							
							country = fianl;
						}
						tagWord.add(country);
						String args = tdCount+ ":" + labels +":"+country.trim();
						Arg.add(args);

					} else {
						country = td.text();
					}
				}
				trNo++;
			}
			
			if(Arg.size()!=0){
			String[] srl = new String[tdMax+1];
			
			for(int i=0; i<Arg.size(); i++){
				String[] arg_split = Arg.get(i).split(":");
				for(int j=0; j<=tdMax; j++){
					if(Integer.parseInt(arg_split[0])==j)
						srl[j] = srl[j] + Arg.get(i)+"@@";
				}
			}
			
			for(int i=0; i<srl.length; i++){
				List<String> srl1 = new ArrayList<String>();
				String[] phrase = srl[i].replace("null", "").split("@@");
				for(int j=0; j<phrase.length; j++){
					srl1.add(phrase[j]);
				}
				Explanation.add(srl1);
			}
			
			for(List<String> par : Explanation){
				for(String role : par){
					String[] arg_split = role.split(":");
					if(!arg_split[1].equals("V"))
						Argument.add(arg_split[2]);					
				}
			}						
			}
		} catch (ClientProtocolException c) {
			System.out.print("c"+c.getMessage());

		} catch (IOException io) {
			System.out.print("io"+io.getMessage());
		}
		return Argument;

	}
	
	//將每個段落的句子根據動詞參數結構去計算字的權重
	public ArrayList<ArrayList<Double>> verbArgScore(dataPreprocess preprocess) {
		ArrayList<ArrayList<Double>> score = new ArrayList<ArrayList<Double>>();
		for(int i=0; i<preprocess.getParaNum();i++){//從1開始是為了不考慮title
			double[] wordNum = new double[preprocess.getSentenceToWord_hash().get(i).size()];
			double[] wordNum2 = null;
			int[] appear = new int[preprocess.getSentenceToWord_hash().get(i).size()];
			for(String sentence : preprocess.getTextToSentence().get(i)){
				wordNum2 = new double[preprocess.getSentenceToWord_hash().get(i).size()];
				ArrayList<String> Argument = httpData(sentence.replace("’", "'").replace("“", "").replace("”", ""));
				for(int j=0;j<Argument.size();j++){//非動詞參數結構字串
					String[] singleWord = Argument.get(j).split(" ");
					for(int k=0;k< preprocess.getSentenceToWord_hash().get(i).size(); k++){
						for(int l=0; l<singleWord.length; l++){
							if(stem.Stem(singleWord[l].toLowerCase()).equals(stem.Stem(preprocess.getSentenceToWord_hash().get(i).get(k).toLowerCase()))){
								wordNum[k]++;
								wordNum2[k]++;
							}
						}
					}
				}
				for(int j=0;j<wordNum2.length;j++){
					if(wordNum2[j]>0)
						appear[j]++;
				}				
			}
			ArrayList<Double> score2 = new ArrayList<Double>();
			for(int j=0;j<wordNum.length;j++){
				if(wordNum[j]!=0 && appear[j]!=0){
					score2.add((wordNum[j]/preprocess.getTextToSentence().get(i).size()));
					//score2.add((wordNum[j]/appear[j]));//字的權重(ctfp)
				}
				else
					score2.add(0.0);
			}
			score.add(score2);
		}
		return score;
	}

	//將每個段落的句子根據動詞參數結構去計算字的權重
	public ArrayList<Double> verbArgScore2(dataPreprocess preprocess) {
		ArrayList<Double> score = new ArrayList<Double>();
		List<String> paraSentence = new ArrayList<String>();
		
		for(List<String> sentence : preprocess.getTextToSentence()){
			paraSentence.addAll(sentence);
		}
		
		double[] wordNum = new double[preprocess.getArticleWord_hash().size()];
		double[] wordNum2 = null;
		int[] appear = new int[preprocess.getArticleWord_hash().size()];
		for(String sentence : paraSentence){
			wordNum2 = new double[preprocess.getArticleWord_hash().size()];
			ArrayList<String> Argument = httpData(sentence.replace("’", "'").replace("“", "").replace("”", ""));
			for(int j=0;j<Argument.size();j++){//非動詞參數結構字串
				String[] singleWord = Argument.get(j).split(" ");
				for(int k=0; k<preprocess.getArticleWord_hash().size(); k++){
					for(int l=0; l<singleWord.length; l++){
						if(stem.Stem(singleWord[l].toLowerCase()).equals(stem.Stem(preprocess.getArticleWord_hash().get(k).toLowerCase()))){
							System.out.println("***");
							wordNum[k]++;
							wordNum2[k]++;
						}
					}
				}
			}
			for(int j=0;j<wordNum2.length;j++){
				if(wordNum2[j]>0)
					appear[j]++;
			}				
		}

		for(int j=0;j<wordNum.length;j++){
			if(wordNum[j]!=0 && appear[j]!=0)
				score.add((wordNum[j]/appear[j]));//字的權重(ctfp)
			else
				score.add(0.0);
			
		}
		return score;
	}	
}
