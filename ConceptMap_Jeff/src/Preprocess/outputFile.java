package Preprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class outputFile {
	BufferedWriter bw = null;
	FileWriter writer = null;
	
	public void outputFile1(String root,ArrayList<String> keyword, int fileName) {//在此匯出多重檔案
		
		for(int i=0;i<keyword.size();i++){
			// 底下開始把跑出來的資料印成txt檔
			try {				
					// 建立物件，並且裝上Buffer
					writer = new FileWriter(root+ (i+1) + ".txt", true);
					bw = new BufferedWriter(writer);
					bw.write(keyword.get(i).replace(" ", "\r\n"));
	
			} catch (FileNotFoundException e) {
				System.out.print("檔案找不到");
			} catch (IOException e) {
	
			} finally {
				try {
					bw.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	public void outputFile2(String root,String fileName,String content) {//在此只匯出單一檔案(wiki相似度)
		try {
			// 建立物件，並且裝上Buffer
			writer = new FileWriter(root + fileName + ".txt", true);
			bw = new BufferedWriter(writer);
			bw.write(content);

		} catch (FileNotFoundException e) {
			System.out.print("檔案找不到");
		} catch (IOException e) {

		} finally {
			try {
				bw.close();
			} catch (IOException e) {
			}
		}
	}
	
	public void clearText(String root,String fileName){
		try {
			FileWriter fw =  new FileWriter(root + fileName + ".txt");
			fw.write("");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
