package Preprocess;

import java.sql.SQLException;
import java.util.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import db_connect.deleteTable;
import db_connect.insertTable;
import db_connect.selectArticle;
import db_connect.sqlObject;

public class readFile {
	private List<String> paragraph = new ArrayList<String>();//用來儲存每一個段落的內容
	private List<String> chin_paragraph = new ArrayList<String>();//用來儲存每一個段落的內容
	public int paraNum;//用來儲存共有多少個段落
	private List<List<String>> textToSentence = new ArrayList<List<String>>();
	
	public readFile(){		
	}
	
	public ArrayList<String> documentWord(String fileRoot)//在此讀檔後後續做wiki的動作
	{
		try{
			String tmp="";
			ArrayList<String> keyword = new ArrayList<String>();
			FileReader fr = new FileReader(fileRoot);
			BufferedReader bfr = new BufferedReader(fr);

			while ((tmp = bfr.readLine()) != null) {
					keyword.add(tmp);
			}
			fr.close();
		
			return keyword;
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
		return null;
	}
	

	
	
	public void readFile(String file, int paraNumber){//用於一般檔檔
		File document = new File(file);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"MS950"));
			String text = null;

			while ((text = reader.readLine()) != null) {
				paragraph.add(text.trim().replace("’", "'"));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(paraNumber!=0){
			paraProcess para = new paraProcess();
			paragraph = para.paraNumber(paraNumber,paragraph);
		}
		
		this.setParagraph(paragraph);
		this.setParaNum(paragraph.size());
		this.retrieveSentence();//開始進行切割句子
	}
	
	public void readOnline(ArrayList<String>paratext , int paraNumber){//用於讀取線上的文章
		for(String text : paratext){
			paragraph.add(text.trim().replace("’", "'"));
		}
				
		if(paraNumber!=0){
			paraProcess para = new paraProcess();
			paragraph = para.paraNumber(paraNumber,paragraph);
		}
		
		this.setParagraph(paragraph);
		this.setParaNum(paragraph.size());
		this.retrieveSentence();//開始進行切割句子
	}
	
	public void readDatabase(String file, int paraNumber, String Source, sqlObject sqlobj)
		    throws SQLException//用於讀取資料庫內的文章
		  {
		    selectArticle select = new selectArticle();
		    paraProcess para = new paraProcess();
		    deleteTable delete = new deleteTable();
		    insertTable insert = new insertTable();

		    select.setIpInfo(sqlobj.Ip, sqlobj.User, sqlobj.PassWord);
		    delete.setIpInfo(sqlobj.Ip, sqlobj.User, sqlobj.PassWord);
		    insert.setIpInfo(sqlobj.Ip, sqlobj.User, sqlobj.PassWord);
		    
		    select.article(file, Source);
		    this.paragraph = select.getParagraph();
		    if (Source.equals("Panorama"))
		    {
		      this.chin_paragraph = select.getChin_paragraph();
		      para.paraNumber2(paraNumber + 1, this.paragraph, this.chin_paragraph);
		      this.chin_paragraph = para.getChin_para();
		      delete.paragraph(file, Source);
			  insert.paragraph(file, Source, this.paragraph, this.chin_paragraph);
		    }
		    else if (Source.equals("Article_Upload"))
		    {
		      para.paraNumber2(paraNumber + 1, this.paragraph, this.chin_paragraph);
		      delete.paragraph(file, Source);
			  insert.paragraph(file, Source, this.paragraph, this.chin_paragraph);
		    }
		    else if(Source.equals("wecan") || Source.equals("wecan_summary"))
		    {
		      para.paraNumber2(paraNumber + 1, this.paragraph, this.chin_paragraph);
		    }
		    
		    this.paragraph = para.getPara();
		    setParagraph(this.paragraph);
		    setParaNum(this.paragraph.size());
		    retrieveSentence();
		  }
		  
	
//	public void readFile2(String file){
//		File document = new File(file);
//		if(document.exists()){
//			BufferedReader reader = null;
//			try {
//				reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"MS950"));
//				String text = null;
//	
//				while ((text = reader.readLine()) != null) {//readLine()方法會在讀取到使用者的換行字元時，再一次將整行字串傳入。
//					paragraph.add(text.replace("’", "'").trim());
//				}
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//	
//			} catch (IOException e) {
//				System.out.println("檔案路徑錯誤！");
//			} finally {
//				try {
//					if (reader != null) {
//						reader.close();
//					}
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//			
//			this.setParagraph(paragraph);
//			this.setParaNum(paragraph.size());
//			this.retrieveSentence();//開始進行切割句子
//		}
//	}

	public void readFile3(String file, int paraNumber){//用於讀取DUC2001資料及內未經處理含有XML標籤的文章
		File document = new File(file);
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(document),"MS950"));
			String text = null;		
			boolean text_flag = false;
			while ((text = reader.readLine()) != null) {
				if(text.equals("<TEXT>"))
			        {
			        	text_flag = true;
			        	continue;
		            }
				else if(text.equals("</TEXT>"))
			        {
			        	text_flag = false;
			        }
				if(text_flag == true)
				paragraph.add(text.trim().replace("’", "'"));
			}
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			System.out.println("檔案路徑錯誤！");
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(paraNumber!=0){
			paraProcess para = new paraProcess();
			paragraph = para.paraNumber(paraNumber,paragraph);
		}
		
		this.setParagraph(paragraph);
		this.setParaNum(paragraph.size());
		this.retrieveSentence();//開始進行切割句子
	}
	public List<List<String>>  retrieveSentence() {
		for (String para : this.getParagraph()) {
			textToSentence.add(SentenceDetection.getSentence(para));
		}
		return textToSentence;
	}
	
	//存取文章的英文段落
	public List<String> getParagraph() {
		return paragraph;
	}
	public void setParagraph(List<String> textls) {
		this.paragraph = textls;
	}
	
	//存取文章的中文段落
	public List<String> getChin_paragraph() {
		return chin_paragraph;
	}
	public void setChin_paragraph(List<String> chin_paragraph) {
		this.chin_paragraph = chin_paragraph;
	}

	//存取文章段落的數量
	public int getParaNum() {
		return paraNum;
	}
	public void setParaNum(int paraNum) {
		this.paraNum = paraNum;
	}

	public List<List<String>> getTextToSentence() {
		return textToSentence;
	}
	public void setTextToSentence(List<List<String>> textToSentence) {
		this.textToSentence = textToSentence;
	}

}
