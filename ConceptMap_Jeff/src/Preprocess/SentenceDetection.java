package Preprocess;

import java.io.IOException;
import java.util.ArrayList;

import com.aliasi.sentences.MedlineSentenceModel;
import com.aliasi.sentences.SentenceModel;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;

public class SentenceDetection {

	static final TokenizerFactory TOKENIZER_FACTORY = IndoEuropeanTokenizerFactory.INSTANCE;
	static final SentenceModel SENTENCE_MODEL = new MedlineSentenceModel();
	
	public static ArrayList<String> getSentence(String text) {
		ArrayList<String> sentence = new ArrayList<String>();
		ArrayList<String> tokenList = new ArrayList<String>();
		ArrayList<String> whiteList = new ArrayList<String>();

		Tokenizer tokenizer = TOKENIZER_FACTORY.tokenizer(text.toCharArray(),0, text.length());
		tokenizer.tokenize(tokenList, whiteList);
		String[] tokens = new String[tokenList.size()];
		String[] whites = new String[whiteList.size()];
		
		tokenList.toArray(tokens);
		whiteList.toArray(whites);
		
		int[] sentenceBoundaries = SENTENCE_MODEL.boundaryIndices(tokens,whites);
		
		if (sentenceBoundaries.length < 1) {
			System.out.println("No sentence boundaries found.");
		}
		int sentStartTok = 0;
		int sentEndTok = 0;
		String phrase = "";
		for (int i = 0; i < sentenceBoundaries.length; ++i) {
			sentEndTok = sentenceBoundaries[i];
			for (int j = sentStartTok; j <= sentEndTok; j++) {
				phrase = phrase + tokens[j] + whites[j + 1];
			}

			phrase = phrase.trim().replace(".\" ", ".\" QQQ").replace("?\" ", "?\" QQQ").replace("!’ ", "!’ QQQ");
			String[] part = phrase.split("QQQ");
			for(int j=0;j<part.length;j++)
				sentence.add(part[j]);
			
			phrase = "";
			sentStartTok = sentEndTok + 1;
		}
		return sentence;
	}
}
