package Preprocess;

import java.util.ArrayList;

import NER.demo.Entity;

public class outputText extends outputFile{
	private String bin = outputText.class.getClassLoader().getResource("").getPath().replace("bin/", "").replace("classes/", "").replace("%20", " ");
	private String root = bin+"RelatedDocuments\\result\\";
	
	public void outputPhrase(String fileName, ArrayList<ArrayList<Entity>> context){
		clearText(root, fileName);//有檔案則會清空檔案
		
		String content = "";
		for(int i=0;i<context.size();i++){			
			for(Entity text : context.get(i))
				content = content + text.entity+" ("+ text.entity_type +")"+"\r\n";
			content = content +"\r\n";
			content = content + "********************\r\n";
		}
		outputFile2(root, fileName, content);
		System.err.println(fileName + " 檔已輸出。");
	}
	public void outputPhrase_inarrstring(String fileName, ArrayList<String> context){
		clearText(root, fileName);//有檔案則會清空檔案
		
		String content = "";
				int i = 0;
			for(String text : context)
			{
				i++;
				content+= i+". "+text+"\r\n";
			}
			//content = content +"\r\n";
			//content = content + "********************\r\n";
		
		outputFile2(root, fileName, content);
		System.err.println(fileName + " 檔已輸出。");
	}

}
