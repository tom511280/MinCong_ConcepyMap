package Preprocess;

import java.util.ArrayList;
import java.util.List;

public class paraProcess {
	private List<String> para = new ArrayList<String>();
	private List<String> chin_para = new ArrayList<String>();
	
	//針對附上的檔案(沒有附中文的內容)進行段落處理
	public List<String> paraNumber(int paraNumber, List<String> paragraph) {
		
		para = paragraph;
		int index = 0, index2 = 0;
		String[] word;
		while(para.size()>paraNumber){
			int num = 10000;
			for(int i=1;i<para.size();i++){
				word = para.get(i).split(" ");
				if(num>word.length){
					num = word.length;
					index = i;//找出段落最小的編號索引
				}
			}

			if(index==1){
				index2 = 2;
				para.set(index2, para.get(index)+" "+para.get(index2));
			}
			else if(index==para.size()-1){
				index2 = para.size()-2;
				para.set(index2, para.get(index2)+" "+para.get(index));
			}
			else{
				word = para.get(index-1).split(" ");
				int previous = word.length;
				word = para.get(index+1).split(" ");
				int after = word.length;
				if(previous<after){
					index2 = index-1;
					para.set(index2, para.get(index2)+" "+para.get(index));
				}
				else{
					index2 = index+1;
					para.set(index2, para.get(index)+" "+para.get(index2));
				}
			}
			para.remove(index);
			setPara(para);
		}
		
		return para;	
		
	}
	
	//針對光華雜誌(有附中文的內容)進行段落處理
	public void paraNumber2(int paraNumber, List<String> paragraph, List<String> chin_paragraph) {

		para = paragraph;
		if(paraNumber==1)
			paraNumber = para.size();
		chin_para = chin_paragraph;
		int index = 0, index2 = 0;
		String[] word;
		while(para.size()>paraNumber){
			int num = 10000;
			for(int i=1;i<para.size();i++){
				word = para.get(i).split(" ");
				if(num>word.length){
					num = word.length;
					index = i;//找出段落最小的編號索引
				}
			}

			if(index==1){
				index2 = 2;
				para.set(index2, para.get(index)+" "+para.get(index2));
				chin_para.set(index2, chin_para.get(index)+" "+chin_para.get(index2));
			}
			else if(index==para.size()-1){
				index2 = para.size()-2;
				para.set(index2, para.get(index2)+" "+para.get(index));
				chin_para.set(index2, chin_para.get(index2)+" "+chin_para.get(index));
			}
			else{
				word = para.get(index-1).split(" ");
				int previous = word.length;
				word = para.get(index+1).split(" ");
				int after = word.length;
				if(previous<after){
					index2 = index-1;
					para.set(index2, para.get(index2)+" "+para.get(index));
					chin_para.set(index2, chin_para.get(index2)+" "+chin_para.get(index));
				}
				else{
					index2 = index+1;
					para.set(index2, para.get(index)+" "+para.get(index2));
					chin_para.set(index2, chin_para.get(index)+" "+chin_para.get(index2));
				}
			}
			para.remove(index);
			chin_para.remove(index);
			
			setPara(para);
			setChin_para(chin_para);
		}
	}

	public List<String> getPara() {
		return para;
	}
	public void setPara(List<String> para) {
		this.para = para;
	}

	public List<String> getChin_para() {
		return chin_para;
	}
	public void setChin_para(List<String> chin_para) {
		this.chin_para = chin_para;
	}	
}
