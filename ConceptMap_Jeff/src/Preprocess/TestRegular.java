package Preprocess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//從句子或段落中用正規表達式取得phrase 
public class TestRegular {	
  
  public ArrayList<String> Regular(String sentence) throws ClassNotFoundException, IOException{
	  PartOfSpeech pos = new PartOfSpeech();
	  ArrayList<String> word = new ArrayList<String>();
	  ArrayList<String> tag = new ArrayList<String>();
	  ArrayList<Integer> getTags = new ArrayList<Integer>();
	  ArrayList<String> getWord = new ArrayList<String>();
	  ArrayList<String> getWordToEntity = new ArrayList<String>();
	  Set<String> set = new LinkedHashSet<String>();	  	  	  
	  
	  pos.wordSegmentation(pos.findPartOfSpeech(sentence));

	  for (int i = 0; i < pos.getWords().size(); i++) {		  
			word.add(pos.getWords().get(i));
			tag.add(pos.getTags().get(i));
	  }
	  
	  //將詞性從頭到尾都加起來後最後再從後面開始尋找
	  String tags="";
	  for(int i=0;i<tag.size();i++)
		  tags = i + tag.get(i) + " "+tags;
	  //4gram
	//  Pattern pattern = 
			//  Pattern.compile("([\\d]+n[a-z]+[^a-z]? ){1,3}([\\d]+jj)|"
			  	        //	+ "([\\d]+n[a-z]+[^a-z]? ){1,3}([\\d]+od)|"
			  		     //   + "([\\d]+n[a-z]+[^a-z]? ){1,4}");//形容詞+名詞先,複合名詞後
	  Pattern pattern = Pattern.compile("([\\d]+n[a-z]+[^a-z]? ){1,3}([\\d]+jj )|([\\d]+n[a-z]+[^a-z]? ){1,4}");//形容詞+名詞先,複合名詞後

	  //	  Pattern pattern = Pattern.compile("[\\d]+jj )|([\\d]+n[a-z]+[^a-z]?");
//	  Pattern pattern = Pattern.compile("([\\d]+n[a-z]+[^a-z]?)");
	  Matcher matcher = pattern.matcher(tags);
	  
	  
	  Pattern pattern2 = Pattern.compile("[\\d]+");//此正規表達式是為了取得單字所在的位置(數字)
	  while(matcher.find()) {
		  String phrase = "";
		//  String tag1 = "";
		  getTags.clear();          
          String findReg = matcher.group().trim();
          
          Matcher matcher2 = pattern2.matcher(findReg);
          while(matcher2.find()) {      	  
        	  getTags.add(Integer.parseInt(matcher2.group()));//取出詞性所對應的位置
          }
          
          Collections.sort(getTags);
          //利用取得的位置，將詞彙組合起來
          for(int i=0;i<getTags.size();i++)
         // {
        	  phrase = phrase + word.get(getTags.get(i))+" ";
        //      tag1 = tag1+tag.get(getTags.get(i))+" ";
         // }
          phrase = phrase.trim();
        //  System.out.println(phrase);
        //  System.out.println(tag1);
          set.add(phrase);//用set來儲存非重複的詞彙
	  }
	  
	  Iterator iterator = set.iterator();
	  while (iterator.hasNext()) {
		  getWord.add((String) iterator.next());
	  }
	  Collections.reverse(getWord);
	  
	  for(String phrase : getWord){
		  phrase = phrase.replace("-", "ZAZA").replace("'s", "ABAB").replaceAll("[\\p{Punct}]*", "");
		  phrase = phrase.replace("ZAZA", "-").replace("ABAB", "'s");
		  if(!phrase.equals(""))
			  getWordToEntity.add(phrase);
	  }
	  return getWordToEntity;
  }
}