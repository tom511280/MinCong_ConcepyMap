package Preprocess;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.aliasi.classify.ConditionalClassification;
import com.aliasi.hmm.HiddenMarkovModel;
import com.aliasi.hmm.HmmDecoder;
import com.aliasi.tag.TagLattice;
import com.aliasi.tokenizer.RegExTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.Streams;

public class PartOfSpeech {
	static TokenizerFactory TOKENIZER_FACTORY = new RegExTokenizerFactory("(-|'|\\d|\\p{L})+|\\S");
	private List<String> wordTags = new ArrayList<String>();
	private List<String> words =new ArrayList<String>();
	private List<String> tags = new ArrayList<String>();
	
	//將傳進來的句子都切割成單字並找出每個單字的詞性
	public List<String> findPartOfSpeech(String line) throws ClassNotFoundException, IOException {        
		String bin = PartOfSpeech.class.getClassLoader().getResource("").getPath().replace("bin/", "").replace("classes/", "").replace("%20", " ");
		String str = bin+"\\RelatedDocuments\\pos-en-general-brown.HiddenMarkovModel";
		
		FileInputStream fileIn = new FileInputStream(str);
		ObjectInputStream objIn = new ObjectInputStream(fileIn);
		HiddenMarkovModel hmm = (HiddenMarkovModel) objIn.readObject();
		Streams.closeQuietly(objIn);
		HmmDecoder decoder = new HmmDecoder(hmm);
		
		
		//以句子為單位的方式進行判定
		List<String> wordTags = new ArrayList<String>();
		ArrayList<String>sentence = new ArrayList<String>();
		SentenceDetection sentencedetection = new SentenceDetection();
		sentence = sentencedetection.getSentence(line);
		int paracount = 0;
		for(String st : sentence)
		{
//			paracount++;
//			System.out.println(paracount+" : "+st);
		
		//char[] cs = line.toCharArray();
		char[] cs = st.toCharArray();
		Tokenizer tokenizer = TOKENIZER_FACTORY.tokenizer(cs, 0, cs.length);
		
		String[] tokens = tokenizer.tokenize();
		//System.out.println(tokens.toString());
		List<String> tokenList = Arrays.asList(tokens);
		TagLattice<String> lattice = decoder.tagMarginal(tokenList);
		//System.out.println(lattice);
		for (int tokenIndex = 0; tokenIndex < tokenList.size(); ++tokenIndex) {
			ConditionalClassification tagScores = lattice.tokenClassification(tokenIndex);
			//System.out.println(tagScores);
			String tag = tagScores.category(0);
			wordTags.add(tokenList.get(tokenIndex) + " " + tag);
//			System.out.println("word: "+tokenList.get(tokenIndex)+"  tag: "+tag);
		}
//		for(int i=0;i<wordTags.size();i++)
//			System.out.println(wordTags.get(i));
//		
//		for(int i = 0;i<tokenList.size();i++)
//		{
//			System.out.println(tokenList.get(i));
//		}
		}
		return wordTags;
	}
	
	public List<String> wordPartOfSpeech(String[] tokens) throws ClassNotFoundException, IOException {
		String str = ".\\RelatedDocuments\\pos-en-general-brown.HiddenMarkovModel";
		FileInputStream fileIn = new FileInputStream(str);
		ObjectInputStream objIn = new ObjectInputStream(fileIn);
		HiddenMarkovModel hmm = (HiddenMarkovModel) objIn.readObject();
		Streams.closeQuietly(objIn);
		HmmDecoder decoder = new HmmDecoder(hmm);
		
		
		List<String> tokenList = Arrays.asList(tokens);
		TagLattice<String> lattice = decoder.tagMarginal(tokenList);
		List<String> wordTags = new ArrayList<String>();
		for (int tokenIndex = 0; tokenIndex < tokenList.size(); ++tokenIndex) {
			ConditionalClassification tagScores = lattice.tokenClassification(tokenIndex);
			String tag = tagScores.category(0);
			wordTags.add(tokenList.get(tokenIndex) + " " + tag);
			//System.out.println(tokenList.get(tokenIndex) + " " + tag);
		}
//		for(int i=0;i<wordTags.size();i++)
//			System.out.println(wordTags.get(i));
		return wordTags;
	}
	
	//將findPartOfSpeech function傳進來的句子把單字和詞性分開做儲存
	public void wordSegmentation(List<String> wordTags) {
		this.setWordTags(wordTags);
		List<String> words = new ArrayList<String>();
		List<String> tags = new ArrayList<String>();
		for (int i = 0; i < this.getWordTags().size(); ++i) {
			String[] str = wordTags.get(i).split(" ");
			words.add(str[0]);
			tags.add(str[1]);
//			System.out.println(str[0]+" "+str[1]);
		}
		this.setTags(tags);
		this.setWords(words);
	}
	
	public List<String> getWordTags() {
		return wordTags;
	}
	public void setWordTags(List<String> wordTags) {
		this.wordTags = wordTags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public void setWords(List<String> words) {
		this.words = words;
	}
	public List<String> getWords() {
		return words;
	}
	public List<String> getTags() {
		return tags;
	}

}
