package Preprocess;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import method.phraseLayer1.fromTermWeight;

import org.xml.sax.SAXException;

import com.lowagie.text.DocumentException;

import db_connect.deleteTable;
import db_connect.insertTable;
import db_connect.sqlObject;
import drawConceptMap.conceptMap;
import NER.demo.Entity;
import factory.phraseWeight;
import factory.phraseWeightParagraph;
import factory.termWeight;
import factory.termWeightParagraph;

public class mainProcess {
//	NumberFormat nf = NumberFormat.getInstance();
//
//	
//	
//	
//	public ArrayList<Entity> concept1 = new ArrayList<Entity>();
//	public ArrayList<String> excelText = new ArrayList<String>();// 存放要匯出excel的內容
//	public HashMap<Integer, Integer> sentence_number_hash = new HashMap<Integer, Integer>();// 存放每段的句數
//	public dataPreprocess preprocess = new dataPreprocess();
//	public SentenceDetection sentencedetection = new SentenceDetection();
//	public ArrayList<ArrayList<Entity>> concept2 = new ArrayList<ArrayList<Entity>>();
//	public ArrayList<ArrayList<Double>> similarity2 = new ArrayList<ArrayList<Double>>();
//	public ArrayList<Integer> import_para = new ArrayList<Integer>();// 存放句子大於三段的段落
//	
////	ArrayList<ArrayList<Entity>> concept3 = new ArrayList<ArrayList<Entity>>();//去除重複
//	
//	public String fileName;
//	public int paraNum;
//	public int secondNum;
//	public int thirdNum;
//	public String kind;
//	
//	public Boolean process(String fileName, int paraNum, int secondNum,
//			int thirdNum, String kind ,int methods ,boolean title_boolean, sqlObject sqlobj) throws ClassNotFoundException,
//			XPathExpressionException, IOException, SAXException,
//			ParserConfigurationException, RowsExceededException,
//			WriteException, SQLException {
//		
//		
//		this.fileName = fileName;
//		this.paraNum = paraNum;
//		this.secondNum = secondNum;
//		this.thirdNum = thirdNum;
//		this.kind = kind;
//		
//		
//		nf.setMaximumFractionDigits(4);// 設定最大的小數位數，並將最後一位數4捨5入
//		preprocess.preprocess(fileName, paraNum, kind,sqlobj);
//		
//	    switch(methods){
//			case 1://使用主題方法來產生ConceptMap(旻璁)
//				TopicProcess();
//				Cut_Paragraph();
//				drawconcept();
//				break;
//			case 2://使用語意方法來產生ConceptMap(建任)
//				SemanticProcess();
//				//Cut_Paragraph();
//				//drawconcept();
//				break;
//				
//	    }
//	    
//		return true;
//	}
//	private void drawconcept() throws FileNotFoundException{
//
//	    //產生概念圖
//	    try {
//			new conceptMap(fileName, kind, import_para, concept1,
//					concept2, secondNum, thirdNum);
//		} catch (DocumentException e) {
//			
//		}
//	}
//	private void SemanticProcess()
//	{
//		/******************************CenterIdea*************************************/
//		// 選擇計算term weight的方法
//		termWeight tw = new termWeight(1, preprocess);
//		ArrayList<Double> termScore = tw.getScore();
//
//		// 將單一關鍵字依分數進行排序
//		ArrayList<SortObject> List2 = new ArrayList<SortObject>();
//		for (int i = 0; i < preprocess.getArticleWord_hash().size(); i++) {
//			List2.add(new SortObject(preprocess.getArticleWord_hash().get(i),
//					termScore.get(i)));
//		}
//		Collections.sort(List2, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
//		ArrayList<String> keyword = new ArrayList<String>();
//		ArrayList<Double> similarity = new ArrayList<Double>();
//		for (SortObject object : List2) {
////			object.show();//顯示排序結果
//			excelText.add(object.obj + "@@" + object.value);
//			keyword.add(object.obj);
//			similarity.add(object.value);
//		}
//		
//		ArrayList<ArrayList<Entity>> outpurarr = new ArrayList<ArrayList<Entity>>();
//		outpurarr.add(concept1);
//		outputText output = new outputText();
//		output.outputPhrase_inarrstring(fileName,excelText);
//		
//////		 選擇計算phrase weight的方法
////		phraseWeight pw = new phraseWeight(1, preprocess, termScore);
////		Entity[] phrases = pw.getPhrase();
////		ArrayList<Double> phraseScore1 = pw.getScore();
////
////		// 將第一層的phrase依分數進行排序
////		List2.clear();
////		excelText.clear();
////		for (int i = 0; i < phrases.length; i++) {
////			List2.add(new SortObject(phrases[i], phraseScore1.get(i)));
////		}
////		ArrayList<String> keyphrase = new ArrayList<String>();
////		Collections.sort(List2, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
////		for (SortObject object : List2) {
////			// object.show();//顯示排序結果
////			excelText.add(object.entity.entity + " : " + object.value);
////			keyphrase.add(object.entity.entity);
////		}
////		concept1.add(List2.get(0).entity);// 第1層main idea
////		
////		/******************************ParagraphIdea*************************************/
////		termWeightParagraph twp = new termWeightParagraph(1, preprocess);
////		ArrayList<ArrayList<Double>> termScore2 = twp.getScore();
//////
////		
////		// 選擇計算各段落phrase weight的方法
////		phraseWeightParagraph pwp = new phraseWeightParagraph(1, preprocess,
////				termScore2);
////		ArrayList<ArrayList<Double>> phraseScore2 = pwp.getScore();
//////
////		excelText.clear();
////		// 將單一關鍵字依分數進行排序
////		for (int i = 0; i < termScore2.size(); i++) {
////			ArrayList<String> secondKeyword = new ArrayList<String>();
////			ArrayList<SortObject> List = new ArrayList<SortObject>();
////			for (int j = 0; j < termScore2.get(i).size(); j++) {
////				List.add(new SortObject(preprocess.getSentenceToWord_hash()
////						.get(i).get(j), termScore2.get(i).get(j)));
////			}
////			Collections.sort(List, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
////			for (SortObject object : List) {
////				// object.show();//顯示排序結果
////				excelText.add(object.obj + "@@" + object.value);
////				secondKeyword.add(object.obj);
////			}
////			if (i < termScore2.size() - 1) {
////				excelText.add("" + "@@" + "");
////				excelText.add("第" + (i + 1) + "段");
////			}
////		}
////
////		excelText.clear();
////		// 將phrase依分數進行排序
////		int noTitle = 0;// 針對摘要資料集若沒有title,i則從0開始
////		if (phraseScore2.size() == 1)
////			noTitle = -1;
////		for (int i = 1 + noTitle; i < phraseScore2.size(); i++) {
////			ArrayList<String> secondKeyphrase = new ArrayList<String>();
////			ArrayList<SortObject> List = new ArrayList<SortObject>();
////			ArrayList<Entity> concept = new ArrayList<Entity>();
////			ArrayList<Double> psimilarity = new ArrayList<Double>();
////			for (int j = 0; j < phraseScore2.get(i).size(); j++) {
////				if (!preprocess.getPhrase_ner().get(i).get(j).equals(" ")) {
////					List.add(new SortObject(preprocess.getPhrase_ner().get(i)
////							.get(j), phraseScore2.get(i).get(j)));
////				}
////			}
////			Collections.sort(List, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
////			for (SortObject object : List) {
////				// object.show();//顯示排序結果
////				excelText.add(object.entity.entity + "@@" + object.value);
////				Entity entity = new Entity(object.entity.entity,
////						String.valueOf(object.value));
////				// concept.add(object.entity);
////				concept.add(entity);
////				psimilarity.add(object.value);
////				secondKeyphrase.add(object.entity.entity);
//////				System.out.println(object.entity.entity + "@@" + object.value);
////			}
////			if (i < phraseScore2.size() - 1) {
////				excelText.add("" + "@@" + "");
////				excelText.add("第" + (i + 1) + "段");
////			}
////			concept2.add(concept);
////			similarity2.add(psimilarity);
////		}
//////		ArrayList<ArrayList<Entity>> outpurarr = new ArrayList<ArrayList<Entity>>();
//////		outpurarr.add(concept1);
//////		outputText output = new outputText();
//////		output.outputPhrase_inarrstring(fileName,excelText);
//	}
//	private void TopicProcess()
//	{
//		
//	}
//	/******************************將概念新增至DB*************************************/
//	private void ConceptInsert(String fileName,String kind) throws SQLException
//	{
//		deleteTable delete = new deleteTable();
//	    delete.phrase(fileName, kind);
//	    insertTable insertPhrase = new insertTable();
//	    insertPhrase.phrase(fileName, kind, concept1, concept2, similarity2,import_para);
//	}
//	/******************************去除少於三句段落*************************************/
//	private void Cut_Paragraph()
//	{
//	    //取得每段的句子數 
// 		for (int i = 0; i < preprocess.getParagraph().size(); i++) {
// 			sentence_number_hash.put(i,sentencedetection.getSentence(preprocess.getParagraph().get(i)).size());
// 		}
// 		//取得句數大於3句的句子編號
// 		int para_number = 0;
//		for (java.util.Iterator<ArrayList<Entity>> allElements = concept2
//				.listIterator(); allElements.hasNext();) {
////			ArrayList<Entity> currentElement = allElements.next();
//			if (sentence_number_hash.get(para_number) < 3)
//				allElements.remove();
//			else
//				import_para.add(para_number);
//			para_number++;
//		}
//	}
}
