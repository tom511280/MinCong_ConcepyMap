package Preprocess;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import mainprocess.ProcessCommand;
import method.termLayer1.tfProcess;



import org.tartarus.snowball.ext.PorterStemmer;
import org.xml.sax.SAXException;

import db_connect.sqlObject;
import NER.demo.Entity;
import NER.demo.Entity2;
import NER.demo.Entity_nlp;
import NER.demo.accessNER;

public class dataPreprocess extends readFile{

	private Set<String> set = new LinkedHashSet<String>();//用來存放不重複的東西	
	private ArrayList<String> articleWord = new ArrayList<String>();//已做完stopword和stemming而存的單字，是按照整篇文章的個別字來存
	private ArrayList<String> articleWord_hash = new ArrayList<String>();//已做完stopword和stemming而存的單字，是按照整篇文章的個別字來存
	private ArrayList<ArrayList<Entity>> textToRegular = new ArrayList<ArrayList<Entity>>();
	private ArrayList<ArrayList<String>> textToRegular2 = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<Entity>> textToNer = new ArrayList<ArrayList<Entity>>();
	private ArrayList<ArrayList<Entity>> phrase_ner = new ArrayList<ArrayList<Entity>>();
	private ArrayList<ArrayList<String>> sentenceToWord = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> sentenceToWord_hash = new ArrayList<ArrayList<String>>();
	private ArrayList<ArrayList<String>> sentenceToWord2 = new ArrayList<ArrayList<String>>();
	
	PartOfSpeech pos = new PartOfSpeech();
//	StemWords stem = new StemWords();
	StopWord stopword = new StopWord();
	accessNER access = new accessNER();
	PorterStemmer porterstemmer = new PorterStemmer();
	
	//針對整篇文章進行前處理
	public void preprocess(ProcessCommand pc,int paraNumber) throws IOException, ClassNotFoundException, XPathExpressionException, SAXException, ParserConfigurationException, SQLException  {		
		if(pc.Source.equals("readfile"))
		readFile(pc.FilePath+"\\"+pc.FileName+ ".txt",paraNumber);//讀取檔案並將文章進行段落和句子切割
		else if(pc.Source.equals("readOnline"))
		readOnline(pc.online_text,paraNumber);//讀取線上檔案並將文章進行段落和句子切割
		else if(pc.Source.equals("multiple_file"))
		readFile(pc.FilePath+"\\"+pc.FileName,paraNumber);//讀取檔案並將文章進行段落和句子切割	
		else if(pc.Source.equals("other"))
		readFile3(pc.FilePath+"\\"+pc.FileName,paraNumber);
		else//包含Panorama Article_Upload
		readDatabase(pc.FileName, paraNumber, pc.Source, pc.DB);

		int paraNum = 1;//段落數
		for (String para : getParagraph()) {
			System.out.println("第"+paraNum + "段&" + para.trim());			
			sentenceToWord.add(paraToWord(para.trim()));
			//System.out.println(paraToWord(para));		
			paraNum++;
		}
		
		sentenceToWord2 = stopword.removeStopWord2(sentenceToWord);//移除stop word 和標點符號(各段落做處理)
		//System.out.println(sentenceToWord2);
		
		//處理"-"問題
		tfProcess tf = new tfProcess(sentenceToWord2);
		sentenceToWord2 = tf.getSentenceToWord();
		tf.replaceParagraph(getTextToSentence());
		setParagraph(tf.getNewParagraph());
		setTextToSentence(tf.getNewTextToSentence());

		//stemming(針對個段落裡的字做字詞還原)		
		sentenceToWord.clear();//(只是為了重新使用並使用該變數)
		set.clear();//(只是為了重新使用)
		//針對各段落用ArrayList儲存重複和非重複的單字陣列
		for(ArrayList<String> para : sentenceToWord2){
			set.clear();
			ArrayList<String> singleWord = new ArrayList<String>();
			ArrayList<String> singleWord_hash = new ArrayList<String>();
			for (String word : para) {
				if(word.indexOf("-")>=0){
					set.add(word);
					singleWord.add(word);
				}
				else{
					for(String par : getParagraph()){
//						if(!word.equals(word.toLowerCase()) && par.indexOf(" "+word.toLowerCase()+" ")<=0){
//							set.add(word);
//							singleWord.add(word);
//							break;
//						}
//						else{
							
							//wordnet還原
//							String stemWord = stem.Stem(word);
//							System.out.println(stemWord);
							//------------------------
							
//							//波特詞幹演算法還原
							porterstemmer.setCurrent(word.toLowerCase());
							porterstemmer.stem();
            	        	String stemWord = porterstemmer.getCurrent();  
            	        	//System.out.println(stemWord);
            	        	//------------------------
							set.add(stemWord);//處理Stemming;
							singleWord.add(stemWord);
							break;
//						}
					}
					
				}
			}
			sentenceToWord.add(singleWord);//全部段落會重複的單字清單
			Iterator iterator = set.iterator();
			while (iterator.hasNext()) {
				singleWord_hash.add((String) iterator.next());
			}
			sentenceToWord_hash.add(singleWord_hash);//全部段落不會重複的單字清單
		}

		//用ArrayList儲存整篇文章重複和非重複的單字陣列
		set.clear();
		for(ArrayList<String> para : sentenceToWord){
			for (String word : para) {				
				articleWord.add(word);
				set.add(word);
			}
		}		
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			articleWord_hash.add((String) iterator.next());
		}
		
		//找出phrase和NER
		TestRegular test = new TestRegular();
		Entity_nlp entity2 = new Entity_nlp();
		for(String par : getParagraph()){
			par = par.replace("\"", "").replace("“", "").replace("”", "").replace("’", "'").replace("‘", "'");
			//System.out.println("test:   "+par);
			textToRegular2.add(test.Regular(par));
			//textToNer.add(entity2.Entity(par));
		}

		//針對用正規表達式所找出的phrase再丟入NER去作判別
		for(ArrayList<String> p : textToRegular2){
			ArrayList<Entity> pToNer = new ArrayList<Entity>();
			for(String w : p){
				pToNer.add(new Entity(w,null));
			}
//			for(String w : p){
//				Entity e = access.findNER(w);
//				if(e!=null){
//					pToNer.add(e);
//				}
//				else{
//					ArrayList<Entity> pToNer2 = entity2.Entity(w);//針對每個字找出NER
//					if(!pToNer2.isEmpty()){
//						String oPhrase = "";
//						for(Entity e1 : pToNer2){
//							oPhrase = e1.entity;
//							pToNer.add(e1);
//						}
//						if(!w.equals(oPhrase))
//							pToNer.add(new Entity(w,"null"));
//					}
//					else
//						pToNer.add(new Entity(w,"null"));
//				}
//			}
			textToRegular.add(pToNer);
		}phrase_ner = textToRegular;
		//combine();
	}
	
	//將傳入的段落或句子找出單字與詞性
	public ArrayList<String> paraToWord(String para) throws ClassNotFoundException, IOException{
		ArrayList<String> singleWord = new ArrayList<String>();
		para = para.replace("“", "").replace("”", "");
		pos.wordSegmentation(pos.findPartOfSpeech(para));
		for (int i = 0; i < pos.getWords().size(); i++) {
			
//			System.out.print(pos.getTags().get(i)+"***");
//			System.out.print(pos.getWords().get(i)+"***");
			
			String[] tags = pos.getTags().get(i).split("");
			if(tags.length>1)
			{
				if(tags[0].equals("n") || (tags[0].equals("") && tags[1].equals("n"))){
				if(!pos.getWords().get(i).matches("[\\p{Punct}]*") && !pos.getWords().get(i).matches("’‘") && !pos.getWords().get(i).equals("s")){
//					System.out.println(pos.getWords().get(i)+"**"+pos.getTags().get(i));
					singleWord.add(pos.getWords().get(i).replace("'s", "").replace("'", ""));
				}
			}
			}
		}
		return singleWord;
	}
	
	//將正規表達式和NER的phrase結合起來
	public void combine() throws IOException{
		int par = 0;
		if(getTextToNer().size()==0){
			phrase_ner = getTextToRegular();
		}
		if(getTextToRegular().size()==0){
			phrase_ner = getTextToNer();
		}
		for(List<Entity> regular : getTextToRegular()){
			ArrayList<Entity> phrase = new ArrayList<Entity>();
			ArrayList<Entity> phrase2 = new ArrayList<Entity>();
			for(int i=0;i<getTextToNer().get(par).size(); i++)
				phrase.add(getTextToNer().get(par).get(i));
			for(int i=0;i<regular.size();i++)
				phrase.add(regular.get(i));			
			phrase = Entity.singleElement(phrase);
			
			for(Entity w : phrase){
				if(w.entity_type.equals("null")){
					String[] w_split = w.entity.split(" |-");
					String wStem = "";
					for(String s : w_split){
						wStem = wStem + s + " ";
					}
					phrase2.add(new Entity(wStem.trim(),"null"));
				}
				else{
					phrase2.add(w);	
				}
			}

			access.storeNER(phrase2);
			phrase = Entity2.singleElement2(phrase,phrase2);
			
			phrase_ner.add(phrase);			
			par++;
		}		
	}

	//用ArrayList存取整篇文章重複的單字陣列
	public ArrayList<String> getArticleWord() {
		return articleWord;
	}
	public void setArticleWord(ArrayList<String> articleWord) {
		this.articleWord = articleWord;
	}
	
	//用ArrayList存取整篇文章非重複的單字陣列
	public ArrayList<String> getArticleWord_hash() {
		return articleWord_hash;
	}
	public void setArticleWord_hash(ArrayList<String> articleWord_hash) {
		this.articleWord_hash = articleWord_hash;
	}
	
	//針對各段落用二維ArrayList存取重複的單字陣列
	public ArrayList<ArrayList<String>> getSentenceToWord() {
		return sentenceToWord;
	}
	public void setSentenceToWord(ArrayList<ArrayList<String>> sentenceToWord) {
		this.sentenceToWord = sentenceToWord;
	}

	//針對各段落用二維ArrayList存取非重複的單字陣列
	public ArrayList<ArrayList<String>> getSentenceToWord_hash() {
		return sentenceToWord_hash;
	}
	public void setSentenceToWord_hash(
			ArrayList<ArrayList<String>> sentenceToWord_hash) {
		this.sentenceToWord_hash = sentenceToWord_hash;
	}
	
	//針對各段落用正規表達式來存取phrase
	public ArrayList<ArrayList<Entity>> getTextToRegular() {
		return textToRegular;
	}
	public void setTextToRegular(ArrayList<ArrayList<Entity>> textToRegular) {
		this.textToRegular = textToRegular;
	}

	//針對各段落用NER來存取phrase
	public ArrayList<ArrayList<Entity>> getTextToNer() {
		return textToNer;
	}
	public void setTextToNer(ArrayList<ArrayList<Entity>> textToNer) {
		this.textToNer = textToNer;
	}
	
	//存取各段落用正規表達式與NER的phrase
	public ArrayList<ArrayList<Entity>> getPhrase_ner() {
		return phrase_ner;
	}
	public void setPhrase_ner(ArrayList<ArrayList<Entity>> phrase_ner) {
		this.phrase_ner = phrase_ner;
	}	
}
