package Preprocess;
import java.text.NumberFormat;
import java.util.*;

import NER.demo.Entity;

public class SortObject implements Comparable<SortObject>{
	public String obj;
	public String obj2;
	public Double num;
	public Double value;
	public Entity entity;

	public SortObject(String obj, Double value) {
		this.obj = obj;
		this.value = value;
	}
	
	public SortObject(String obj,String obj2) {
		this.obj = obj;
		this.obj2 = obj2;
	}
	
	public SortObject(String obj, Double num, Double value) {
		this.obj = obj;
		this.num = num;
		this.value = value;
	}
	
	public SortObject(Entity entity, double value) {
		this.entity = entity;
		this.value = value;
	}

	public void show(){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);//設定最大的小數位數，並將最後一位數4捨5入
		System.out.println(obj+" :: " + nf.format(value));
	}
	
	@Override
	public int compareTo(SortObject o) {
		return Comparators.VALUE.compare(this, o);
	}
	
	public static class Comparators {

        public static Comparator<SortObject> OBJ = new Comparator<SortObject>() {
            @Override
            public int compare(SortObject o1, SortObject o2) {
            	return o1.obj.compareTo(o2.obj);
            }
        };
        public static Comparator<SortObject> NUM = new Comparator<SortObject>() {
            @Override
            public int compare(SortObject o1, SortObject o2) {
            	int result;
            	result = o1.num < o2.num ? 1
            			: (o1.num == o2.num ? 0 : -1);
            	if (result == 0) {
            		result = o1.obj.compareTo(o2.obj);
            	}
            	return result;
            }
        };
        public static Comparator<SortObject> VALUE = new Comparator<SortObject>() {
            @Override
            public int compare(SortObject o1, SortObject o2) {
            	int result;
            	if(o1.value < o2.value)
            		result = 1;
            	else if(o1.value > o2.value)
            		result = -1;
            	else
            		result = 0;
//            	result = o1.value < o2.value ? 1
//            			: (o1.value == o2.value ? 0 : -1);
//            	if (result == 0) {
//            		result = o1.obj.compareTo(o2.obj);
//            	}
            	return result;
            }
        };
    }	
}