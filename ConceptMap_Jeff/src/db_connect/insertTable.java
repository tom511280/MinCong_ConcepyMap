package db_connect;

import NER.demo.Entity;

import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class insertTable
{
  private sqlQuery test = new sqlQuery();
  public void setIpInfo(String Ip,String User,String PassWord){
	  test.setIpInfo(Ip, User, PassWord);
  }
  public void paragraph(String article_id, String kind, List<String> paragraph, List<String> chin_paragraph)
    throws SQLException
  {
    int count = 0;
    if (kind.equals("Panorama")) {
      for (int i = 1; i < paragraph.size(); i++)
      {
        this.test.anySQL("INSERT INTO concept_paragraph (article_id,eng_paragraph,chin_paragraph,`order`) VALUES(?,?,?,?)");
        this.test.pst.setString(1, article_id);
        this.test.pst.setString(2, (String)paragraph.get(i));
        this.test.pst.setString(3, (String)chin_paragraph.get(i));
        this.test.pst.setInt(4, i);
        int count2 = this.test.pst.executeUpdate();
        if (count2 != 0) {
          count++;
        }
      }
    } else if (kind.equals("Article_Upload")) {
      for (int i = 1; i < paragraph.size(); i++)
      {
        this.test.anySQL("INSERT INTO concept_paragraph_upload (article_id,paragraph,`order_num`) VALUES(?,?,?)");
        this.test.pst.setString(1, article_id);
        this.test.pst.setString(2, (String)paragraph.get(i));
        this.test.pst.setInt(3, i);
        int count2 = this.test.pst.executeUpdate();
        if (count2 != 0) {
          count++;
        }
      }
    }
    if (count != 0) {
      System.err.println("已新增 " + count + " 筆記錄。");
    }
  }
  
  public void phrase(String fileName, String kind, ArrayList<Entity> concept1, ArrayList<ArrayList<Entity>> concept2, ArrayList<ArrayList<Double>> similarity2, ArrayList<Integer> import_para)
    throws SQLException
  {
    NumberFormat nf2 = NumberFormat.getInstance();
    nf2.setMaximumFractionDigits(2);
    int count = 0;
    String table_kind = "";
    if(kind.equals("Panorama") || kind.equals("Article_Upload"))
    {
    if (kind.equals("Panorama")) 
    table_kind = "keyphrase_paragraph";
    else if (kind.equals("Article_Upload")) 
    table_kind = "keyphrase_paragraph_upload";
    
    this.test.anySQL("INSERT INTO " + table_kind + " (article_id,keyphrases,entity,similarity,`order`) VALUES(?,?,?,?,?)");
    this.test.pst.setString(1, fileName);
    this.test.pst.setString(2, ((Entity)concept1.get(0)).entity);
    this.test.pst.setString(3, ((Entity)concept1.get(0)).entity_type);
    this.test.pst.setString(4, null);
    this.test.pst.setInt(5, 0);
    }
    else if(kind.equals("wecan"))
    {
    	table_kind = "keyphrase_paragraph";
    	this.test.anySQL("INSERT INTO " + table_kind + " (task_id,keyphrases,entity,`order`) VALUES(?,?,?,?)");
        this.test.pst.setString(1, fileName);
        this.test.pst.setString(2, ((Entity)concept1.get(0)).entity);
        this.test.pst.setString(3, ((Entity)concept1.get(0)).entity_type);
        this.test.pst.setInt(4, 0);	
    }
    int count2 = this.test.pst.executeUpdate();
    if (count2 != 0) {
      count++;
    }
    for (int i = 1; i < concept2.size(); i++)
    {
      String phrase = "";String entity = "";String similarity = "";
      for (int j = 0; j < ((ArrayList)concept2.get(i)).size(); j++)
      {
        phrase = phrase + ((Entity)((ArrayList)concept2.get(i)).get(j)).entity;
        entity = entity + ((Entity)((ArrayList)concept2.get(i)).get(j)).entity_type;
        similarity = similarity + nf2.format(((ArrayList)similarity2.get(i)).get(j));
        if (j < ((ArrayList)concept2.get(i)).size() - 1)
        {
          phrase = phrase + "@@";
          entity = entity + "@@";
          similarity = similarity + "@@";
        }
      }
      if (kind.equals("Panorama") || kind.equals("Article_Upload"))
      {
      this.test.anySQL("INSERT INTO " + table_kind + " (article_id,keyphrases,entity,similarity,`order`) VALUES(?,?,?,?,?)");
      this.test.pst.setString(1, fileName);
      this.test.pst.setString(2, phrase);
      this.test.pst.setString(3, entity);
      this.test.pst.setString(4, similarity);
      this.test.pst.setInt(5, i);
      }
      else if(kind.equals("wecan"))
      {
    	 // System.out.println(import_para);
    	  for(int z = 0;z <  import_para.size();z++)
    	  {
    	  if(i==(import_para.get(z)+1))//只加入長度大於三的段落
    	  {
    	  System.out.println(i+" "+(import_para.get(z)+1));
    	  this.test.anySQL("INSERT INTO " + table_kind + " (task_id,keyphrases,entity,`order`) VALUES(?,?,?,?)");
          this.test.pst.setString(1, fileName);
          this.test.pst.setString(2, phrase);
          this.test.pst.setString(3, entity);
          this.test.pst.setInt(4, (import_para.get(z)+1));  
          test.pst.executeUpdate();
     	  }
    	  }
      }
     // int count3 = this.test.pst.executeUpdate();
      //if (count3 != 0) {
      //  count++;
      //}
    }
    if (count != 0) {
      System.err.println("已新增 " + count + " 筆記錄。");
    }
  }
  public void summary_dbinsert(String fileName, String kind, ArrayList<String> mark, HashMap<Integer,String> summary,String title)
		    throws SQLException
{
		    NumberFormat nf2 = NumberFormat.getInstance();
		    nf2.setMaximumFractionDigits(2);
		    int count = 0;
		    String table_kind = "summary";
		     
		    	  for(int i = 0;i < mark.size();i++)
		    	  {
		    	  if(i==0)
		    	  {
		    		  this.test.anySQL("INSERT INTO " + table_kind + " (article_id,content,paragraph,mark) VALUES(?,?,?,?)");
			          this.test.pst.setString(1, fileName);
			          this.test.pst.setString(2, title);
			          this.test.pst.setInt(3, 0);  
			          this.test.pst.setString(4, "title");
			          test.pst.executeUpdate();
		    	  }
		    	  //{
		    	  //System.out.println(i+" "+(import_para.get(z)+1));
		    	  else
		    	  {
		    	  this.test.anySQL("INSERT INTO " + table_kind + " (article_id,content,paragraph,mark) VALUES(?,?,?,?)");
		          this.test.pst.setString(1, fileName);
		          if(summary.containsKey(i))
		              this.test.pst.setString(2, summary.get(i));
		          else
		        	  this.test.pst.setString(2, null);  
		          this.test.pst.setInt(3, i);
		          this.test.pst.setString(4, mark.get(i));  
		          test.pst.executeUpdate();
		    	  }
		     	 // }
		    	  }
		      
		    if (count != 0) {
		      System.err.println("已新增 " + count + " 筆記錄。");
		    }
		  }
}
