package db_connect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class selectArticle
{
  private ArrayList<String> select_column = new ArrayList();
  private String sql = "";
  private List<String> paragraph = new ArrayList();
  private List<String> chin_paragraph = new ArrayList();
  private sqlQuery test = new sqlQuery();
  
  public void setIpInfo(String Ip,String User,String PassWord){
	  test.setIpInfo(Ip, User, PassWord);
  }
  public void article(String article_id, String kind)
    throws SQLException
  {
    this.select_column.add(article_id);
    if (kind.equals("Panorama"))
    {
      this.sql = "SELECT replace(eng_topic,'’','\\'') as eng_topic, chin_topic FROM article where id=?";
      test.SelectTable(this.sql, this.select_column);
      if (test.rs.next())
      {
        this.paragraph.add(test.rs.getString("eng_topic"));
        this.chin_paragraph.add(test.rs.getString("chin_topic"));
      }
      this.sql = "SELECT a.id,a.eng_topic,a.chin_topic, replace(replace(eng_paragraph,'XAX','\\''),'QQQ','-') as eng_paragraph,p.chin_paragraph as chin_paragraph, p.`order` FROM article a JOIN paragraph p ON a.id = p.article_id where a.id =?";
      test.SelectTable(this.sql, this.select_column);
      while (test.rs.next())
      {
        this.paragraph.add(test.rs.getString("eng_paragraph"));
        this.chin_paragraph.add(test.rs.getString("chin_paragraph"));
      }
    }
    else if (kind.equals("Article_Upload"))
    {
      this.sql = "SELECT replace(title,'’','\\'') as title FROM article_upload where article_id=?";
      test.SelectTable(this.sql, this.select_column);
      if (test.rs.next()) {
        this.paragraph.add(test.rs.getString("title"));
      }
      this.sql = "SELECT a.article_id,a.title, replace(replace(paragraph,'XAX','\\''),'QQQ','-') as paragraph,p.`order_num` FROM article_upload a JOIN paragraph_upload p ON a.article_id = p.article_id where a.article_id =?";
      test.SelectTable(this.sql, this.select_column);
      while (test.rs.next()) {
        this.paragraph.add(test.rs.getString("paragraph"));
      }
    }
    else if(kind.equals("wecan") || kind.equals("wecan_summary"))
    {
    	 this.sql = "SELECT replace(content,'’','\\'') as text FROM organization_text_item where task_id=?";
         test.SelectTable(this.sql, this.select_column);
        // int i = 0;
         while (test.rs.next()) {
        	// if(i!=0)//第一項結果不加 因為wecan中的第一項是title
           this.paragraph.add(test.rs.getString("text"));
           //i++;
         }
    }
    test.Close();
  }
  
  public List<String> getParagraph()
  {
    return this.paragraph;
  }
  
  public void setParagraph(List<String> paragraph)
  {
    this.paragraph = paragraph;
  }
  
  public List<String> getChin_paragraph()
  {
    return this.chin_paragraph;
  }
  
  public void setChin_paragraph(List<String> chin_paragraph)
  {
    this.chin_paragraph = chin_paragraph;
  }
}
