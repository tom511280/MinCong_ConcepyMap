package db_connect;

import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class deleteTable
{
	private sqlQuery test = new sqlQuery();
	public void setIpInfo(String Ip,String User,String PassWord){
		  test.setIpInfo(Ip, User, PassWord);
	  }
  public void paragraph(String article_id, String kind)
    throws SQLException
  {
    
    if (kind.equals("Panorama")) {
      test.anySQL("DELETE FROM concept_paragraph  WHERE article_id=?");
    } else if (kind.equals("Article_Upload")) {
      test.anySQL("DELETE FROM concept_paragraph_upload  WHERE article_id=?");
    }
    test.pst.setString(1, article_id);
    int count = test.pst.executeUpdate();
    if (count != 0) {
      System.err.println("已刪除了 " + count + " 筆記錄。");
    }
  }
  
  public void phrase(String article_id, String kind)
    throws SQLException
  {
    sqlQuery test = new sqlQuery();
    if (kind.equals("Panorama")) {
      test.anySQL("DELETE FROM keyphrase_paragraph WHERE article_id=?");
    } else if (kind.equals("Article_Upload")) {
      test.anySQL("DELETE FROM keyphrase_paragraph_upload WHERE article_id=?");
    }else if (kind.equals("wecan")) {
        test.anySQL("DELETE FROM keyphrase_paragraph WHERE task_id=?");
     }
    else if(kind.equals("wecan_summary"))
    {
    	test.anySQL("DELETE FROM summary WHERE task_id=?");
    }
    test.pst.setString(1, article_id);
   // test.pst.executeUpdate();
    int count = test.pst.executeUpdate();
    if (count != 0) {
      System.err.println("已刪除了 " + count + " 筆記錄。");
    }
  }
}
