package db_connect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class selectKeyphrase
{
  private ArrayList<String> select_column = new ArrayList();
  private String sql = "";
  private List<String> center = new ArrayList();
  private List<String> second = new ArrayList();
  private List<String> entity = new ArrayList();
  
  private sqlQuery test = new sqlQuery();
  public void setIpInfo(String Ip,String User,String PassWord){
	  test.setIpInfo(Ip, User, PassWord);
  }
  public void paraKeyphrase(String article_id, String kind, String para)
    throws SQLException
  {
    this.select_column.add(article_id);
    String table_kind = "";
    if (kind.equals("Panorama")) {
      table_kind = "keyphrase_paragraph";
    } else if (kind.equals("Article_Upload")) {
      table_kind = "keyphrase_paragraph_upload";
    }
    this.sql = 
      ("SELECT keyphrases from " + table_kind + " WHERE article_id = ? AND `order` = 0");
    test.SelectTable(this.sql, this.select_column);
    if (test.rs.next()) {
      this.center.add(test.rs.getString("keyphrases"));
    }
    this.select_column.add(para);
    this.sql = ("SELECT keyphrases, entity from " + table_kind + " WHERE article_id = ? AND `order` = ?");
    test.SelectTable(this.sql, this.select_column);
    if (test.rs.next())
    {
      String[] keyphrases = test.rs.getString("keyphrases").split("@@");
      String[] entity_type = test.rs.getString("entity").split("@@");
      for (int i = 0; i < keyphrases.length; i++)
      {
        this.second.add(keyphrases[i]);
        this.entity.add(entity_type[i]);
      }
    }
    test.Close();
  }
  
  public List<String> getCenter()
  {
    return this.center;
  }
  
  public void setCenter(List<String> center)
  {
    this.center = center;
  }
  
  public List<String> getSecond()
  {
    return this.second;
  }
  
  public void setSecond(List<String> second)
  {
    this.second = second;
  }
  
  public List<String> getEntity()
  {
    return this.entity;
  }
  
  public void setEntity(List<String> entity)
  {
    this.entity = entity;
  }
}
