package db_connect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class sqlQuery extends db_connect{
	public ResultSet rs = null;
	int count = 0;		

	public void SelectTable(String sql,List<String> select_column)
	{		
		try 
	    { 
			this.sql = sql;
			mysql_connect();			
			pst = con.prepareStatement(sql);
			for(int i=0;i<select_column.size();i++)
				pst.setString(i+1, select_column.get(i));
			rs = pst.executeQuery();
			
	    } 
	    catch(SQLException sqlException) 
	    { 
	    	System.out.println("DropDB Exception :" + sqlException.toString()); 
	    }
		catch(NullPointerException e){
			System.out.println("NullPointerException");
		}
	    finally 
	    { 	    	
	    	//Close(); 
	    }
	}
	
	public void anySQL(String sqlExpression)
	{
		try { 
			mysql_connect();			
			pst = con.prepareStatement(sqlExpression);		    
	    } 
	    catch(SQLException e) { 
	    	System.out.println("DropDB Exception :" + e.toString());
	    } 
		catch(NullPointerException e){
			System.out.println("NullPointerException");
		}
	    finally { 
	    	//Close(); 
	    }		
	}
	
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}	
	public ResultSet getRs() {
		return rs;
	}
}
