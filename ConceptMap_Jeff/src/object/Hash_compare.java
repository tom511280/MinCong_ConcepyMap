package object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Hash_compare {
  private ArrayList<String>Sresult = new ArrayList<String>();
  private ArrayList<Integer>Iresult = new ArrayList<Integer>();
  private ArrayList<Double>Dresult = new ArrayList<Double>();
  public void SDhash_compare(HashMap<String,Double> hash)
  {
	  
	  
	  ArrayList<Entry<String, Double>> arrayList = new ArrayList<Map.Entry<String,Double>>(hash.entrySet());
		Collections.sort(arrayList, new Comparator<Map.Entry<String, Double>>(){
		    public int compare(Map.Entry<String, Double> map1, Map.Entry<String,Double> map2) {
		        return ((map2.getValue() - map1.getValue() == 0) ? 0 : (map2.getValue() - map1.getValue() > 0) ? 1 : -1);
		    }
		});  
		  for (Entry<String, Double> entry : arrayList) 
		  {
			  Sresult.add(entry.getKey());
			  Dresult.add(entry.getValue());
		  }
  }
  public void IDhash_compare(HashMap<Integer,Double> hash)
  {
	  
	  
	  ArrayList<Entry<Integer, Double>> arrayList = new ArrayList<Map.Entry<Integer,Double>>(hash.entrySet());
		Collections.sort(arrayList, new Comparator<Map.Entry<Integer, Double>>(){
		    public int compare(Map.Entry<Integer, Double> map1, Map.Entry<Integer,Double> map2) {
		        return ((map2.getValue() - map1.getValue() == 0) ? 0 : (map2.getValue() - map1.getValue() > 0) ? 1 : -1);
		    }
		});  
		  for (Entry<Integer, Double> entry : arrayList) 
		  {
			  Iresult.add(entry.getKey());
			  Dresult.add(entry.getValue());
		  }
  }
  public ArrayList<String> getKeyArray()
  {
	return Sresult;
  }
  public ArrayList<Integer> getIKeyArray()
  {
	return Iresult;
  }
  public ArrayList<Double> getValueArray()
  {
	return Dresult;
  }
}
