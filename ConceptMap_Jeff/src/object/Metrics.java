package object;

public class Metrics {
	private double H_M;//人工設定取幾個比較
	private double C_M;//對的關鍵詞數量
	private double G_M;//人工給定的關鍵詞數量
	//Percision
	public double getPercision() {
		return (double)C_M/H_M;
	}
	//Recall
	public double getRecall() {
		return (double)C_M/G_M;
	}
	//F1measure
	public double getF1measure() {
		double f1measure = (double)(2*getPercision()*getRecall())/(getPercision()+getRecall());
		if(getPercision() == 0.0 && getRecall() == 0.0)
			return 0.0;
		else
		    return f1measure;
	}
	//其他參數設置
	public void setH_M(double H_M) {//人為給定取幾個比較
		this.H_M = H_M;
	}
	public void setC_M(double C_M) {//系統取出對的數量
		this.C_M = C_M;
	}
	public void setG_M(double G_M) {//黃金標準數量
		this.G_M = G_M;
	}
}
