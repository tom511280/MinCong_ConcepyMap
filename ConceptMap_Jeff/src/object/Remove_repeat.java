package object;

import java.util.HashMap;

import Preprocess.StemWords;

public class Remove_repeat {

	private HashMap<String, Double> keyphrase_hash = new HashMap<String, Double>();
	private HashMap<String, Double> Clean_keyphrase_hash = new HashMap<String, Double>();
	private StemWords stem = new StemWords();
	public void process()
	{
		HashMap<String, String> keyphrase_compare = new HashMap<String, String>();
		for(String keyphrase : keyphrase_hash.keySet())
		{
			String clean_keyphrase = "";
			String[]tmp = keyphrase.split(" ");
			for(int i = 0;i < tmp.length;i++)
			{
				clean_keyphrase+= stem.Stem(tmp[i].trim().toLowerCase())+" ";
			}
			if(!keyphrase_compare.containsKey(clean_keyphrase))
			{
				Clean_keyphrase_hash.put(keyphrase, keyphrase_hash.get(keyphrase));
				keyphrase_compare.put(clean_keyphrase, keyphrase);
			}
				
		}
		//keyphrase_hash = Clean_keyphrase_hash;
		
	}
	public HashMap<String, Double> getkeyphrase_hash()
	{
		return Clean_keyphrase_hash;
	}
	public void setkeyphrase_hash(HashMap<String, Double> keyphrase_hash)
	{
		this.keyphrase_hash = keyphrase_hash;
	}
	
}
