package object;

import java.io.File;
import java.util.HashMap;


//讀取的資料可能包含某一資料夾底下的檔名或者某一個文件
public class Read_object {
	private String Path;
	private String FolderPath;
	//************************************************
	//設定和取得單篇文章路徑
	public void setpath(String Path)
	{
		this.Path = Path;
	}
	public String getpath()
	{
		return Path;
	} 
	//************************************************
	//設定和取得資料夾路徑
	public void setFolderPath(String FolderPath)
	{
		this.FolderPath = FolderPath;
	}
	public String getFolderPath()
	{
		return FolderPath;
	} 
}
