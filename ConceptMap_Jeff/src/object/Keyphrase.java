package object;

public class Keyphrase {//定義"keyphrase"這個類別
	private String phrase_clean;
	private String phrase_original;
	private double score;

	//************************************************
	//設定和取得phrase
	public void setOr(String phrase_original) {//
		this.phrase_original = phrase_original;
	}
	public String getOr() {
		return phrase_original;
	}
	
	//************************************************
	//設定和取得stem後的phrase
	public void setCl(String phrase_clean) {
		this.phrase_clean = phrase_clean;
	}
	public String getCl() {
		return phrase_clean;
	}
	
	//************************************************
	//設定和取得score
	public void setscore(double score) {
		this.score = score;
	}
	public double getscore() {
		return score;
	}
}
