package object;

import java.util.ArrayList;
import java.util.HashMap;

public class Export_object {
	private HashMap<String, Double> keyphrase = new HashMap<String, Double>();//關鍵字結果
	private ArrayList<HashMap<String, Double>> final_pagerank_hash = new ArrayList<HashMap<String, Double>>();//迭代結果
	private HashMap<String, HashMap<String, Integer>> edge_weight = new HashMap<String, HashMap<String, Integer>>();//字圖
	private ArrayList<String> LDA_order = new ArrayList<String>();
	private ArrayList<String> LDA_article = new ArrayList<String>();//LDA 輸出格式
	private ArrayList<String> ContentList;//輸出純文字
	private String content = "";
	
	private String dbname;
	private String filename;
	//************************************************
	//設定和取得關鍵詞列表
	public void setkeyphraseHash(HashMap<String, Double> keyphrase)//設定關鍵詞列表
	{
		this.keyphrase = keyphrase;
	}
	public HashMap<String, Double> getkeyphraseHash()
	{
		return keyphrase;
	}
	//************************************************
	//設定和取得TPR的迭代最終結果
	public void setIteration(ArrayList<HashMap<String, Double>> final_pagerank_hash)
	{
		this.final_pagerank_hash = final_pagerank_hash;
	}
	public ArrayList<HashMap<String, Double>> getIteration()
	{
		return final_pagerank_hash;
	}
	//************************************************
	//設定和取得字圖
	public void setWordGraph(HashMap<String, HashMap<String, Integer>> edge_weight)
	{
		this.edge_weight = edge_weight;
	}
	public HashMap<String, HashMap<String, Integer>> getWordGraph()
	{
		return edge_weight;
	}
	//************************************************
	//LDA要輸出DB的檔案順序
	public void setLDA_order(ArrayList<String> LDA_order)
	{
		this.LDA_order = LDA_order;
	}
	public ArrayList<String> getLDA_order()
	{
		return LDA_order;
	}
	//************************************************
	//LDA要輸出的單篇文章
	public void setSLDA_article(String content)
	{
		this.content = content;
	}
	public String getSLDA_article()
	{
		return content;
	}
	//LDA要輸出的文章
	public void setLDA_article(ArrayList<String> LDA_article)
	{
		this.LDA_article = LDA_article;
	}
	public ArrayList<String> getLDA_article()
	{
		return LDA_article;
	}
	//************************************************
	//LDA要輸出的文章
	public void setContentList(ArrayList<String> ContentList)
	{
		this.ContentList = ContentList;
	}
	public ArrayList<String> getContentList()
	{
		return ContentList;
	}
	//************************************************
	//設定檔案輸出DB名稱
	public void setdbname(String dbname)
	{
		this.dbname = dbname;
	}
	public String getdbname()
	{
			return dbname;
	}
	//************************************************
	//設定檔案輸出名稱
	public void setFileName(String filename)
	{
		this.filename = filename;
	}
	public String getFileName()
	{
		return filename;
	}
	
}
