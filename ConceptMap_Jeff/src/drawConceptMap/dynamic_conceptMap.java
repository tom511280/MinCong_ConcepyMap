package drawConceptMap;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxCellRenderer.CanvasFactory;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxGraph;
import db_connect.selectKeyphrase;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import org.xml.sax.SAXException;

public class dynamic_conceptMap
{
  private String fileName;
  private String para;
  private String kind;
  private int phraseNum;
  private List<String> center = new ArrayList();
  private List<String> second = new ArrayList();
  private List<String> entity = new ArrayList();
  
  public static void main(String[] args)
    throws IOException, ClassNotFoundException, RowsExceededException, WriteException, XPathExpressionException, SAXException, ParserConfigurationException, SQLException
  {}
  
  public boolean conceptMapSetting(String fileName, String kind, String para, int phraseNum)
    throws FileNotFoundException, DocumentException, SQLException
  {
    this.fileName = fileName;
    this.para = para;
    this.kind = kind;
    this.phraseNum = phraseNum;
    
    searchConcept();
    Map();
    return true;
  }
  
  public void searchConcept()
    throws SQLException
  {
    selectKeyphrase paraKeyphrase = new selectKeyphrase();
    paraKeyphrase.paraKeyphrase(this.fileName, this.kind, this.para);
    this.center = paraKeyphrase.getCenter();
    for (int i = 0; (i < this.phraseNum) && (i < paraKeyphrase.getSecond().size()); i++)
    {
      this.second.add((String)paraKeyphrase.getSecond().get(i));
      this.entity.add((String)paraKeyphrase.getEntity().get(i));
    }
  }
  
  public void Map()
    throws FileNotFoundException, DocumentException
  {
    mxGraph graph = new mxGraph();
    Object parent = graph.getDefaultParent();
    graph.getModel().beginUpdate();
    try
    {
      Object v1 = graph.insertVertex(parent, null, this.center.get(0), 0.0D, 0.0D, 1.0D, 1.0D, "strokeColor=red;fontSize=30; fillColor=pink");
      if ((this.center.get(0) != null) && (!((String)this.center.get(0)).equals("null"))) {
        graph.insertVertex(parent, null, "(" + (String)this.center.get(0) + ")", 0.0D, 30.0D, 1.0D, 1.0D, "strokeColor=red;fontSize=20;fillColor=pink;fontColor=blue");
      }
      mxCell[] v2 = new mxCell[this.second.size()];
      int radius = 0;
      int i = 0;
      do
      {
        v2[i] = ((mxCell)graph.insertVertex(parent, null, "(" + (i + 1) + ") " + (String)this.second.get(i), 350.0D * Math.cos(radius * 3.141592653589793D / 180.0D), 
          350.0D * Math.sin(radius * 3.141592653589793D / 180.0D), 2.0D, 2.0D, "fontSize=30;fillColor=orange"));
        if ((this.entity.get(i) != null) && (!((String)this.entity.get(i)).equals("null"))) {
          graph.insertVertex(parent, null, "(" + (String)this.entity.get(i) + ")", 350.0D * Math.cos(radius * 3.141592653589793D / 180.0D), 
            350.0D * Math.sin(radius * 3.141592653589793D / 180.0D) + 30.0D, 2.0D, 2.0D, "fontSize=20;fillColor=orange;fontColor=blue");
        }
        radius += 360 / this.second.size();
        Object localObject1 = graph.insertEdge(v2[i], null, null, v1, v2[i], "strokeColor=red;fontColor=black;fontSize=30");i++;
        if (i >= this.second.size()) {
          break;
        }
      } while (radius <= 360);
    }
    finally
    {
      graph.getModel().endUpdate();
    }
    mxRectangle bounds = graph.getGraphBounds();
    
    Document document = new Document(new Rectangle((float)bounds.getWidth(), (float)bounds.getHeight()));
    String year = "default";
    try
    {
      year = this.fileName.substring(0, 4);
    }
    catch (Exception localException) {}
    String bin = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\RTW\\dynamicConceptMapFile\\" + this.kind + "\\" + year;
    
    File f = new File(bin);
    f.mkdir();
    
    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(bin + "\\" + this.fileName + "_P" + this.para + "_paraConceptMap" + ".pdf"));
    document.open();
    final PdfContentByte cb = writer.getDirectContent();
    
    mxGraphics2DCanvas canvas = (mxGraphics2DCanvas)mxCellRenderer.drawCells(graph, null, 1.0D, null, new mxCellRenderer.CanvasFactory()
    {
      public mxICanvas createCanvas(int width, int height)
      {
        Graphics2D g2 = cb.createGraphics(width, height);
        return new mxGraphics2DCanvas(g2);
      }
    });
    canvas.getGraphics().dispose();
    document.close();
    System.err.println(this.fileName + " 文章之paraConcept map 已輸出pdf檔");
  }
}
