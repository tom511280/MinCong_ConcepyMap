package drawConceptMap;

import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import NER.demo.Entity;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.canvas.mxICanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.util.mxCellRenderer.CanvasFactory;
import com.mxgraph.view.mxGraph;

public class conceptMap {
	private String fileName;
	private String ConceptMapPath;
	private String kind;
	private ArrayList<Entity> concept1 = new ArrayList<Entity>();
	private ArrayList<Integer> para_no = new ArrayList<Integer>();
	private ArrayList<ArrayList<Entity>> concept2 = new ArrayList<ArrayList<Entity>>();
	private int picNum;
	private int secondNum;
	private int thirdNum;
	private int allNum;
//	private String bin= "RelatedDocuments\\"+"ConceptMap";
	final int largeParNum = 6;	
	
	
	public conceptMap(String fileName, String ConceptMapPath, ArrayList<Integer>para_no, ArrayList<Entity> concept1, ArrayList<ArrayList<Entity>> concept2,int secondNum,int thirdNum) throws FileNotFoundException, DocumentException{
		
		this.ConceptMapPath = ConceptMapPath;
		this.fileName = fileName;
		this.kind = kind;
		this.concept1 = concept1;
		this.para_no = para_no;
		if(secondNum==0)
			secondNum = 3;
		if(thirdNum==0)
			thirdNum = 3;
		this.secondNum = secondNum;
		this.thirdNum = thirdNum;
		allNum = this.secondNum + this.thirdNum;
		picNum = (int)Math.ceil((double)(concept2.size())/largeParNum);//段落數
		this.concept2 = conceptCount(concept2);
		this.ConceptMapPath = ConceptMapPath;
		System.out.println("會產生"+concept2.size()+"個段落，共"+picNum+"張概念圖");
		Map();
	}
	
	public ArrayList<ArrayList<Entity>> conceptCount(ArrayList<ArrayList<Entity>> concept2){
		ArrayList<ArrayList<Entity>> concept = new ArrayList<ArrayList<Entity>>();
		for(ArrayList<Entity> par : concept2){
			ArrayList<Entity> phrase = new ArrayList<Entity>();
			for(int i=0;i<par.size() && i<allNum;i++){
				phrase.add(par.get(i));
			}
			concept.add(phrase);
		}
		return concept;
	}
	
	public void Map() throws FileNotFoundException, DocumentException {
		int num = 0, c_num = 0, c_num2 = largeParNum;
		while(num<picNum){//執行每段劃出第二層的動作 大於段數
			mxGraph graph = new mxGraph();
			Object parent = graph.getDefaultParent();
			graph.getModel().beginUpdate();
		
		try {
			String[] entity1_split = concept1.get(0).entity.split(" ");int fontSize1 = 30;
			if(entity1_split.length>2)
				fontSize1 = 25;
			Object v1 = graph.insertVertex(parent, null, concept1.get(0).entity, 0, 0, 1, 1, "strokeColor=red;fontSize="+fontSize1+";fillColor=pink");
			//if(concept1.get(0).entity_type!=null && !concept1.get(0).entity_type.equals("null"))
				//graph.insertVertex(parent, null, "("+concept1.get(0).entity_type+")", 0, 30, 1, 1, "strokeColor=red;fontSize=20;fillColor=pink;fontColor=blue");//第二層和第三層字的權重
			int radius = 0;
			for(int i=c_num; i<concept2.size() && radius<=360 && i<c_num2; i++){
				if(concept2.get(i).isEmpty())
					concept2.get(i).add(new Entity("",null));				
				int secondShift = 0;
				if(secondNum==3)
					secondShift = -30;
				for(int j=0;j<secondNum/2;j++){
//					if(concept2.get(i).get(j).entity_type!=null && !concept2.get(i).get(j).entity_type.equals("null"))
//						secondShift = -60;
				}
				mxCell v2 = null;mxCell[] v22 = new mxCell[secondNum]; int shift1 = 0;
				//第二層概念
				for(int j=0;j<secondNum;j++){
					v22[j] = (mxCell) graph.insertVertex(parent, null, "("+(j+1)+") "+concept2.get(i).get(j).entity, 350 * Math.cos(radius * Math.PI / 180),//第二層概念上標示關鍵詞排名(j+1)
								350 * Math.sin(radius * Math.PI / 180)+shift1+secondShift, 2,2,"fontSize=20;fillColor=orange");
					//if(concept2.get(i).get(j).entity_type!=null && !concept2.get(i).get(j).entity_type.equals("null")){
						//shift1+=30;
//						graph.insertVertex(parent, null, "("+concept2.get(i).get(j).entity_type+")", 350 * Math.cos(radius * Math.PI / 180),
//							350 * Math.sin(radius * Math.PI / 180)+shift1+secondShift, 2,2,"fontSize=10;fillColor=orange;fontColor=blue");			
				//	}
					shift1+=30;
				}
				v2 = v22[(secondNum-1)/2];
				
				Object e1 = null;
				if(!concept2.get(i).get(0).entity.equals(""))//如果該篇有關鍵字的話
					System.out.println("產生第"+para_no.get(i)+"段的概念");
					e1 = graph.insertEdge(v2, null, para_no.get(i)+1, v1, v2,"strokeColor=red;fontColor=black;fontSize=20");//段數標示(i+1)
				
				int radius2 = 0, j = secondNum;
				int lastnum = concept2.get(i).size() - secondNum;
					switch(lastnum){
					case 1: radius2 = 0;break;
					case 2: radius2 = -5;break;
					case 3: radius2 = -10;break;
				}
				while (radius2 < 360 && j < concept2.get(i).size()) {					
					String[] word = concept2.get(i).get(j).entity.split(" ");
					mxCell v3;
					//第三層概念
					if(word.length>1 && ((radius>=60 && radius<=120)) || (radius>=240 && radius<=300)){
						//不用分行, 因為phrase不會太長
						//System.out.print(word[0]+" $ ");
						//System.out.print(word[0]+" ");
						v3 = (mxCell) graph.insertVertex(parent, null, word[0] , 
							350 * Math.cos(radius * Math.PI / 180) + 350 * Math.cos((radius + 2 * radius2) * Math.PI / 180),
							350	* Math.sin(radius * Math.PI / 180) + 350 * Math.sin((radius + 2 * radius2) * Math.PI / 180),
							3,	3, "fontSize=20");
						for(int k=1,shift=30;k<word.length;k++){
							graph.insertVertex(parent,	null, word[k] , 
							350 * Math.cos(radius * Math.PI / 180) + 350 * Math.cos((radius + 2 * radius2) * Math.PI / 180),
							350	* Math.sin(radius * Math.PI / 180) + 350 * Math.sin((radius + 2 * radius2) * Math.PI / 180)+shift,
							3,	3, "fontSize=20");
							shift+=30;
						}
//						if(concept2.get(i).get(j).entity_type!=null && !concept2.get(i).get(j).entity_type.equals("null"))
//							graph.insertVertex(parent,	null, "("+concept2.get(i).get(j).entity_type+")" , 
//								350 * Math.cos(radius * Math.PI / 180) + 350 * Math.cos((radius + 2 * radius2) * Math.PI / 180),
//								350	* Math.sin(radius * Math.PI / 180) + 350 * Math.sin((radius + 2 * radius2) * Math.PI / 180)-30,
//								3,	3, "fontSize=10;fontColor=blue");
					}
					else{
						//要分行, 因為phrase太長
						//System.out.print(concept2.get(i).get(j).entity+" @ ");
						v3 = (mxCell) graph.insertVertex(parent, null, concept2.get(i).get(j).entity , 
							350 * Math.cos(radius * Math.PI / 180) + 350 * Math.cos((radius + 2 * radius2) * Math.PI / 180),
							350	* Math.sin(radius * Math.PI / 180) + 350 * Math.sin((radius + 2 * radius2) * Math.PI / 180),
							3,	3, "fontSize=20");
//						if(concept2.get(i).get(j).entity_type!=null && !concept2.get(i).get(j).entity_type.equals("null"))
//							graph.insertVertex(parent, null, "("+concept2.get(i).get(j).entity_type+")" , 
//								350 * Math.cos(radius * Math.PI / 180) + 350 * Math.cos((radius + 2 * radius2) * Math.PI / 180),
//								350	* Math.sin(radius * Math.PI / 180) + 350 * Math.sin((radius + 2 * radius2) * Math.PI / 180)+30,
//								3,	3, "fontSize=10;fontColor=blue");
					}
					graph.insertEdge(e1, null, null, v2, v3, "strokeColor=orange;fontColor=black;fontSize=20");

					j++;
					radius2 = radius2 + 10;//radius2 = radius2 + 360 / concept2.size();
				}
				radius = radius + 360 / (largeParNum);
			}
		} finally {
			graph.getModel().endUpdate();
		}

		mxRectangle bounds = graph.getGraphBounds();
		
		Document document = new Document(new Rectangle((float) (bounds.getWidth()), (float) (bounds.getHeight())));
		String year = "default";
		try{
			year = fileName.substring(0, 4);
		}
		catch(Exception e2){}
		//String bin = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\RTW\\conceptMapFile\\" + this.kind + "\\" + year;
		//String bin = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\RTW\\conceptMapFile\\" + year;
//		String bin = "C:\\Program Files\\Apache Software Foundation\\Tomcat 7.0\\webapps\\wecan102\\NEW_ConceptMap\\";
		              //C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps\wecan102\NEW_ConceptMap              
		//C:\Program Files\Apache Software Foundation\Tomcat 7.0\webapps\wecan102\NEW_ConceptMap 
		String bin = ConceptMapPath +"//" +fileName;
		File f = new File(bin);
		f.mkdir();
		String multFile ="";
		if(picNum>1)
			multFile = "-" + (num+1);
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(bin +"//" +fileName+"_conceptMap"+multFile+".pdf"));
		document.open();
		final PdfContentByte cb = writer.getDirectContent();

		mxGraphics2DCanvas canvas = (mxGraphics2DCanvas) mxCellRenderer.drawCells(graph, null, 1, null, new CanvasFactory() {
			public mxICanvas createCanvas(int width, int height) {
				Graphics2D g2 = cb.createGraphics(width, height);
				return new mxGraphics2DCanvas(g2);
			}
		});
		
		num++; c_num+=largeParNum; c_num2 = c_num2+largeParNum;
		
		canvas.getGraphics().dispose();
		document.close();
		System.err.println(ConceptMapPath + " 文章之concept map-"+num+"已輸出pdf檔");
	}
		
	}
}
