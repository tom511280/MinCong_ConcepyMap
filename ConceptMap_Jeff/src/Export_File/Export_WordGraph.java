package Export_File;

import java.io.IOException;
import java.util.HashMap;

@SuppressWarnings("rawtypes")
public class Export_WordGraph extends Export_File{
	@SuppressWarnings("unchecked")
	public Export_WordGraph(String FileName,String Export_Path,Boolean timebool,HashMap<String, HashMap<String, Integer>> Export_File) throws IOException{
		super(FileName, Export_Path, timebool, Export_File);
	}

	@SuppressWarnings("unchecked")
	@Override
	void Export() throws IOException {
		// TODO Auto-generated method stub
		System.err.println("此為輸出一篇文章WordGraph的程序");
		fw.write("**********文章Word_Graph結果報告**********"+"\r\n");
		int wi = 0;
		for(String key_hash : ((HashMap<String, HashMap<String, Integer>>) Export_File).keySet())
			{
			wi++;
			    fw.write("**********"+"\r\n");
				fw.write("Key: "+key_hash+"\r\n");
				for(String key : ((HashMap<String, HashMap<String, Integer>>) Export_File).get(key_hash).keySet())
				{
					fw.write("Value: "+key+": "+((HashMap<String, HashMap<String, Integer>>) Export_File).get(key_hash).get(key)+"\r\n");
				}
				fw.write("**********"+"\r\n");
			}
		fw.flush();
	    fw.close();
	}
}
