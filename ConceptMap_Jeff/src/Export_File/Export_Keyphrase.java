package Export_File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Hash_compare;

//此類別用來輸出一篇文章的Keyphrase
@SuppressWarnings("rawtypes")
public class Export_Keyphrase extends Export_File{
	@SuppressWarnings("unchecked")
	public Export_Keyphrase(String FileName,String Export_Path,Boolean timebool,HashMap<String, Double> Export_File) throws IOException{
		super(FileName, Export_Path ,timebool ,Export_File);
	}

	@SuppressWarnings("unchecked")
	@Override
	void Export() throws IOException {
		// TODO Auto-generated method stub
		System.err.println("此為輸出一篇文章Keyphrase的程序");
		//降序排序
		Hash_compare hash_compare = new Hash_compare();  
		hash_compare.SDhash_compare((HashMap<String, Double>)Export_File);
	    for (int i = 0;i < hash_compare.getKeyArray().size();i++) 
		{
			fw.write((i+1)+". "+hash_compare.getKeyArray().get(i) + " : " + hash_compare.getValueArray().get(i)+"\r\n");
		}
		fw.flush();
	    fw.close();
	}

}
