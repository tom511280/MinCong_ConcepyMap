package Export_File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import object.Hash_compare;

//此類別用來輸出一篇文章的LDA格式文檔 因為要餵入LDA的程式有一定的格式   詳見  svn://102 老師與旻璁/Panorama_LDA/system_output/API_web.htm
public class Export_SingleLDA extends Export_File{
	@SuppressWarnings("unchecked")
	public Export_SingleLDA(String FileName,String Export_Path,Boolean timebool,ArrayList<String> Export_File) throws IOException{
		super(FileName+"_LDA", Export_Path ,timebool ,Export_File);
	}

	@Override
	public
	void Export() throws IOException {
		// TODO Auto-generated method stub
		System.err.println("此為輸出一篇文章LDA格式文檔的程序");
		fw.write("1"+"\r\n");
		for(String content : (ArrayList<String>)Export_File){
			fw.write(content+" ");
		}
		fw.flush();
	    fw.close();
		
	}
}
