package Export_File;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

//此類別用來輸出ArrayList中的資料
@SuppressWarnings("rawtypes")
public class Export_ArrayList extends Export_File{
	@SuppressWarnings("unchecked")
	public Export_ArrayList(String FileName,String Export_Path,Boolean timebool,ArrayList<String> Export_File) throws IOException{
		super(FileName,Export_Path,timebool, Export_File);
	}
	@SuppressWarnings({ "unchecked" })
	@Override
	public void Export() throws IOException {
		// TODO Auto-generated method stub	
		System.err.println("此為輸出一個ArrayList的程序");
		for(String content : (ArrayList<String>)Export_File){
			fw.write(content+"\r\n");
		}
		fw.flush();
	    fw.close();
	}
}
