package Export_File;

import java.io.FileWriter;
import java.io.IOException;

//因為要輸出的東西有很多種，所以這裡使用泛型，如此就可以再不預先定義型態的情況下，撰寫或抽象一些方法，增加擴充性
public abstract class Export_File<T> {
	private final java.util.Date i = new java.util.Date();//取得時間日期資訊
	private final String time = "_"+String.valueOf(i.getYear()+1900)+
			                    "_"+String.valueOf(i.getMonth()+1)+
			                    "_"+String.valueOf(i.getDate());	
	
	private String Path;
	public T Export_File;
	public FileWriter fw  = null;
	
	//參數表:(輸出名稱,輸出路徑,是否檔名加上日期資訊,輸出資料)
	public Export_File(String FileName,String Export_Path,Boolean timebool,T Export_File) throws IOException{
		this.Export_File = Export_File;
			Path = Export_Path+"\\"+FileName;
		if(timebool==true)
			Path+= time;
		Path+= ".txt";
		fw = new FileWriter(Path);
		Export();
	    FinishMessage();
	}
	public void FinishMessage(){
		System.err.println(getPath()+ " 輸出完成。");
	}
	public String getPath(){
		return Path;
	}
	abstract void Export() throws IOException;
}
