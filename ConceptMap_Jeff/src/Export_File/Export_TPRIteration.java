package Export_File;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

//此類別用來輸出一篇文章在TPR中的Iteration資料
@SuppressWarnings("rawtypes")
public class Export_TPRIteration extends Export_File{
	@SuppressWarnings("unchecked")
	public Export_TPRIteration(String FileName,String Export_Path,Boolean timebool,ArrayList<HashMap<String, Double>> Export_File) throws IOException{
		super(FileName, Export_Path ,timebool ,Export_File);
	}

	@Override
	void Export() throws IOException {
		// TODO Auto-generated method stub
		System.err.println("此為輸出一篇文章TPR_Iteration的程序");
		fw.write("**********單字TPR_Iteration結果報告**********"+"\r\n");
		int topic = 0;
		for(HashMap<String, Double> key_hash : (ArrayList<HashMap<String, Double>>)Export_File)
			{
				fw.write("主題: "+topic+"------------------\r\n");
				int word_count = 0;
				for(String key : key_hash.keySet())
				{
					word_count++;
					fw.write(word_count+". "+key+": "+((ArrayList<HashMap<String, Double>>) Export_File).get(topic).get(key)+"\r\n");
				}
	 		topic++;
			}
		fw.flush();
	    fw.close();
	}
}
