package Topic_Parameter;

import LDA_Model.LDA_Model;
import Preprocess.dataPreprocess;
import Topic_Iteration.CTopic_Iteration;
import Topic_keyphraseExtract.CTopic_keyphraseExtract;
import Word_Graph.CWord_Graph;

//這個類別被用來放置萃取全文(Content)關鍵詞時的參數
public class CParameter extends Parameter{
	private CWord_Graph word_graph;
	private CTopic_Iteration topic_iteration;
	private CTopic_keyphraseExtract topic_keyphraseExtract;
	public CParameter(String FileName, dataPreprocess preprocess, LDA_Model lda_model){
		super(FileName,preprocess,lda_model);
	}
	//Word_Graph
	public void setCWord_Graph(CWord_Graph word_graph){
		this.word_graph = word_graph;
	}
	public CWord_Graph getCword_graph(){
		return word_graph;
	}
	
	//Topic_Iteration
	public void setCTopic_Iteration(CTopic_Iteration topic_iteration){
		this.topic_iteration = topic_iteration;
	}
	public CTopic_Iteration getCTopic_Iteration(){
		return topic_iteration;
	}
	
	//Topic_keyphraseExtract
	public void setCTopic_keyphraseExtract(CTopic_keyphraseExtract topic_keyphraseExtract){
		this.topic_keyphraseExtract = topic_keyphraseExtract;
	}
	public CTopic_keyphraseExtract getTopic_keyphraseExtract(){
		return topic_keyphraseExtract;
	}
}
