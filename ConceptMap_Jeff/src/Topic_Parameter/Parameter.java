package Topic_Parameter;

import LDA_Model.LDA_Model;
import Preprocess.dataPreprocess;
import Topic_Iteration.Topic_Iteration;
import Topic_keyphraseExtract.Topic_keyphraseExtract;
import Word_Graph.Word_Graph;

//一個主題方法內參數集合的類別
//參數表:(方法類型,預處理,LDA模型,字圖,迭代,關鍵字)
 abstract public class Parameter {
	private int Method,K;//方法總類 主題數
	private dataPreprocess preprocess;
	private LDA_Model lda_model;
	private String FileName;
	public Parameter(String FileName, dataPreprocess preprocess, LDA_Model lda_model){
		this.FileName = FileName;
		this.preprocess = preprocess;
		this.lda_model = lda_model;
		this.Method = Method;
	}
	public int getMethod(){
		return Method;
	}
	
	public void setK(int K){
		this.K = K;
	}
	public int getK(){
		return K;
	}
	
	//預處理
	public dataPreprocess getdataPreprocess(){
		return preprocess;
	}
	
	//LDA_Model
	public LDA_Model getLDA_Model(){
		return lda_model;
	}
}
