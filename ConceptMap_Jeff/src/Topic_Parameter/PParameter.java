package Topic_Parameter;

import LDA_Model.LDA_Model;
import Preprocess.dataPreprocess;
import Topic_Iteration.PTopic_Iteration;
import Topic_keyphraseExtract.PTopic_keyphraseExtract;
import Word_Graph.PWord_Graph;

//這個類別被用來放置萃取段落(paragraph)關鍵詞時的參數
public class PParameter extends Parameter{
	private PWord_Graph word_graph;
	private PTopic_Iteration topic_iteration;
	private PTopic_keyphraseExtract topic_keyphraseExtract;
	public PParameter(String FileName, dataPreprocess preprocess, LDA_Model lda_model){
		super(FileName,preprocess,lda_model);
	}
	
	//Word_Graph
	public void setPWord_Graph(PWord_Graph word_graph){
		this.word_graph = word_graph;
	}
	public PWord_Graph getPword_graph(){
		return word_graph;
	}
	
	//Topic_Iteration
	public void setPTopic_Iteration(PTopic_Iteration topic_iteration){
		this.topic_iteration = topic_iteration;
	}
	public PTopic_Iteration getTopic_Iteration(){
		return topic_iteration;
	}
	
	//Topic_keyphraseExtract
	public void setPTopic_keyphraseExtract(PTopic_keyphraseExtract topic_keyphraseExtract){
		this.topic_keyphraseExtract = topic_keyphraseExtract;
	}
	public PTopic_keyphraseExtract getPTopic_keyphraseExtract(){
		return topic_keyphraseExtract;
	}

}
