package Word_Graph;

import java.util.ArrayList;
import java.util.HashMap;

import Topic_Parameter.CParameter;
import Topic_Parameter.PParameter;

public class PWord_Graph extends Word_Graph{
	private ArrayList<HashMap<String, HashMap<String, Double>>> edge_weight = new ArrayList<HashMap<String, HashMap<String, Double>>>();
	public PWord_Graph(PParameter pparameter,int Windows_Size,int Interval){
		super(pparameter, Windows_Size, Interval);
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<String>> word_clean = insert();
		for(ArrayList<String> para : word_clean){
			ArrayList<ArrayList<String>> slide_windows = slide_windows(para);//進行滑動視窗取詞
			HashMap<String, HashMap<String, Double>> Paraedge_weight = find_weight(slide_windows);//建立字圖
			edge_weight.add(Paraedge_weight);
		}
	}
	
	public ArrayList<ArrayList<String>> insert(){//將字新增至序列
		ArrayList<ArrayList<String>> word_clean = new ArrayList<ArrayList<String>>();//放置文章內經過預處理後的單字序列
		for (ArrayList<String> para :  datapreprocess.getSentenceToWord()) {//因為bag of word 所以把每一段的文字都丟入同一序列
			ArrayList<String> para_word = new ArrayList<String>();
			for (String word : para) {
				para_word.add(word);//把在dataprocess處理過的所有段落單字存到段落陣列
		  }
			word_clean.add(para_word);
		}
		return word_clean;
	}
	
	
	@Override
	public void Show_WordGraph() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void Export_WordGraph() {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<HashMap<String, HashMap<String, Double>>> getPWordGraph() {
		return edge_weight;
	}
}
