package Word_Graph;

import java.util.ArrayList;
import java.util.HashMap;

import Preprocess.dataPreprocess;
import Topic_Parameter.Parameter;


abstract public class Word_Graph {
	public int Windows_Size;//視窗大小
	public int Interval;//視窗間隔
	public dataPreprocess datapreprocess;
	public Word_Graph(Parameter parameter,int Windows_Size,int Interval){//萃取全文關鍵詞使用的建構子
		this.Windows_Size = Windows_Size;
		this.Interval = Interval;
		this.datapreprocess = parameter.getdataPreprocess();
	}
	public ArrayList<ArrayList<String>> slide_windows(ArrayList<String> word_clean) {//建立滑動視窗
		ArrayList<ArrayList<String>> slide_windows = new ArrayList<ArrayList<String>>();//放置每個滑動視窗內的單字
		String input = "";
		double roung = Math.ceil(word_clean.size() / Interval);
		int start = 0;
		for (int i = 1; i <= roung; i++) {
			//String now_windoes = "";
			ArrayList<String> now_windoes = new ArrayList<String>();
			if (start + Windows_Size < word_clean.size()) {
				for (int j = start; j < start + Windows_Size; j++) {//還有足夠的視窗大小可以取
					now_windoes.add(word_clean.get(j));
				}

			} else {
				for (int z = start; z < word_clean.size(); z++) {//沒有足夠的視窗大小可以取
					now_windoes.add(word_clean.get(z));
				}
			}
			start += Interval;
				slide_windows.add(now_windoes);
		}
		return  slide_windows;
	}

	public HashMap<String, HashMap<String, Double>> find_weight(ArrayList<ArrayList<String>> slide_windows) {//計算權重
		HashMap<String, HashMap<String, Double>> edge_weight = new HashMap<String, HashMap<String, Double>>();//放置字圖
		for(ArrayList<String> content : slide_windows)//每個windows
		{
			int windows_position = 0;
			String key = "";
			HashMap<String, Double> edge_weight_sub = new HashMap<String, Double>();
			for(String text : content)//每個windows下的字
			{
				if(windows_position==0)
				{
					key = text;//找到key, 也就是第一個字
				}
				else
				{
					if(edge_weight.containsKey(key)==false)//如果之前hash沒有該key的資料
					{
						edge_weight_sub.put(text,(double) 1);//新增一筆連結資訊<連到的當前結點, 次數>
						edge_weight.put(key,edge_weight_sub);//把連結資訊放入key結點的hash<key,<連到的結點, 次數>>
						
					}
					else//如果key在hash已經有資料了
					{
						//System.out.println("hash之前已經有這個key了 : "+key);
						edge_weight_sub = edge_weight.get(key);//直接取得<key,<連到的結點, 次數>>下的<連到的結點, 次數>
						//System.out.println("else  "+edge_weight_sub);
						if(edge_weight_sub.containsKey(text)==false)//如果之前hash沒有key連到當前結點的資料 則新增一筆連結資訊到hash
							edge_weight_sub.put(text,(double) 1);
						else//如果以前已經有資料  則直接取出來+1
							edge_weight_sub.put(text,edge_weight_sub.get(text)+1);
					}
				}
				windows_position++;
			 }
	     }
		return edge_weight;
	}
	abstract public void execute();//執行程式
	abstract public void Show_WordGraph();//印出字圖
	abstract public void Export_WordGraph();//輸出字圖
}
