package Word_Graph;

import java.util.ArrayList;
import java.util.HashMap;

import Topic_Parameter.CParameter;
import Topic_Parameter.Parameter;

public class CWord_Graph extends Word_Graph{
	private HashMap<String, HashMap<String, Double>> edge_weight = new HashMap<String, HashMap<String, Double>>();//放置以全文為範圍的字圖
	public CWord_Graph(CParameter cparameter,int Windows_Size,int Interval){
		super(cparameter,Windows_Size, Interval);	
	}
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		ArrayList<String> word_clean = insert();//把預處理後的單字序列加入
		ArrayList<ArrayList<String>> slide_windows = slide_windows(word_clean);//進行滑動視窗取詞
		this.edge_weight = find_weight(slide_windows);//建立字圖
	}
	@Override
	public void Show_WordGraph() {
		// TODO Auto-generated method stub
		int key_count = 0;
		for(String key : edge_weight.keySet())
		{
			key_count++;
			System.out.println("節點"+key_count+": "+key);
			System.out.println("成員: "+edge_weight.get(key));
		}
	}
	
	public ArrayList<String> insert(){//將字新增至序列
		ArrayList<String> word_clean = new ArrayList<String>();//放置文章內經過預處理後的單字序列
		for (ArrayList<String> para :  datapreprocess.getSentenceToWord()) {//因為bag of word 所以把每一段的文字都丟入同一序列
			for (String word : para) {
				word_clean.add(word);//把在dataprocess處理過的所有段落單字存到段落陣列
		  }
		}
		return word_clean;
	}
	
	@Override
	public void Export_WordGraph() {
		// TODO Auto-generated method stub
		
	}

	public HashMap<String, HashMap<String, Double>> getCWordGraph() {
		return edge_weight;
	}
}
