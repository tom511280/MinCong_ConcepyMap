package factory;

import java.util.ArrayList;

import Preprocess.dataPreprocess;
import method.termLayer1.ASL_LC;
import method.termLayer1.lexicalChain;
import method.termLayer1.termScore;
import method.termLayer1.termFrequency;
import method.termLayer1.tf_Title;
import method.termLayer1.verbArgStructure;

/*central idea的簡單工廠模式*/
/*選擇想要進行的方法*/
public class termWeight {
	termScore ts = null;
	
	public termWeight(int methods, dataPreprocess preprocess){
		switch(methods){
			case 1://只計算tf
				ts = new termFrequency(preprocess);
				break;
			case 2://計算tf+title
				ts = new tf_Title(preprocess);
				break;
			case 3://計算文章中字在wordnet的分數權重
				ts = new lexicalChain(preprocess);
				break;
			case 4://計算文章中字在動詞參數結構的分數權重
				ts = new verbArgStructure(preprocess);
				break;
			case 5://計算各段落字，wordnet+動詞參數結構的分數權重
				ts = new ASL_LC(preprocess);
				break;
		}
	}
	
	public ArrayList<Double> getScore(){
		return ts.score();
	}
}
