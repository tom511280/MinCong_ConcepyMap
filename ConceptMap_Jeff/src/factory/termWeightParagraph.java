package factory;

import java.util.ArrayList;

import method.termLayer2.ASL_LC;
import method.termLayer2.lexicalChain;
import method.termLayer2.termFrequency;
import method.termLayer2.termScore;
import method.termLayer2.tf_ASL;
import method.termLayer2.verbArgStructure;
import Preprocess.dataPreprocess;

/*main idea of paragraph的簡單工廠模式*/
/*選擇想要進行的方法*/
public class termWeightParagraph {
	termScore ts = null;
	
	public termWeightParagraph(int methods, dataPreprocess preprocess){
		switch(methods){
			case 1://計算各段落字，頻率的分數權重
				ts = new termFrequency(preprocess);
				break;
			case 2://只計算各段落字在wordnet的分數權重
				ts = new lexicalChain(preprocess);
				break;
			case 3://只計算各段落字在動詞參數結構的分數權重
				ts = new verbArgStructure(preprocess);
				break;
			case 4://計算各段落字，wordnet+動詞參數結構的分數權重
				ts = new ASL_LC(preprocess);
				break;
			case 5://計算各段落字，wordnet+動詞參數結構的分數權重
				ts = new tf_ASL(preprocess);
				break;
		}
	}
	
	public ArrayList<ArrayList<Double>> getScore(){
		return ts.score();
	}
}
