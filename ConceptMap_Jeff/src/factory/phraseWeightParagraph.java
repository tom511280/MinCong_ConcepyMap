package factory;

import java.util.ArrayList;

import Preprocess.dataPreprocess;
import method.phraseLayer2.fromTermWeight;
import method.phraseLayer2.phraseScore;

/*central idea的簡單工廠模式*/
/*選擇想要進行的方法*/
public class phraseWeightParagraph {
	phraseScore ps = null;
	
	public phraseWeightParagraph(int methods, dataPreprocess preprocess){
	}
	
	public phraseWeightParagraph(int methods, dataPreprocess preprocess, ArrayList<ArrayList<Double>> termScore) {
		switch(methods){
		case 1://計算keyword在phrase的分數權重
			ps = new fromTermWeight(preprocess, termScore);
			break;
		}
	}
	
	public ArrayList<ArrayList<Double>> getScore(){
		return ps.score();
	}
}
