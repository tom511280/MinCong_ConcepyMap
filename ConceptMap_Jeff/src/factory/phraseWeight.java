package factory;

import java.util.ArrayList;

import NER.demo.Entity;
import Preprocess.dataPreprocess;
import method.phraseLayer1.fromTermWeight;
import method.phraseLayer1.phraseFrequency;
import method.phraseLayer1.phraseScore;

/*central idea的簡單工廠模式*/
/*選擇想要進行的方法*/
public class phraseWeight {
	phraseScore ps = null;
	
	public phraseWeight(int methods, dataPreprocess preprocess){
		switch(methods){
			case 1://只計算pf
				ps = new phraseFrequency(preprocess);
				break;
		}
	}
	
	public phraseWeight(int methods, dataPreprocess preprocess, ArrayList<Double>  termScore) {
		switch(methods){
			case 1://從單一字詞的全眾來計算phrase的權重
				ps = new fromTermWeight(preprocess, termScore);
				break;
			case 2://只計算pf
				ps = new phraseFrequency(preprocess);
				break;
		}
	}

	public Entity[] getPhrase(){
		return ps.phrases();
	}
	
	public ArrayList<Double> getScore(){
		return ps.score();
	}
}
