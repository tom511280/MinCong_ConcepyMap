package mainprocess;

import java.io.IOException;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import jgibblda.LDA;
import org.xml.sax.SAXException;
import LDA_Model.LDA_Model;
import Topic_MethodMenu.C_TPR;
import Topic_MethodMenu.P_TPR;


public class TopicProcess extends Process{
	private C_TPR ctpr;
    private P_TPR ptpr;
	public boolean process(ProcessCommand pc, int paraNum, int secondNum,
			int thirdNum) throws ClassNotFoundException, XPathExpressionException, IOException, SAXException, ParserConfigurationException, SQLException, InterruptedException{
		this.paraNum = paraNum;
		this.secondNum = secondNum;
		this.thirdNum = thirdNum;
		
		fileName = pc.FileName;
		kind = pc.Kind;
		ConceptMapPath = pc.ConceptMapPath;
		
//		preprocess.preprocess("\\document_file\\"+fileName,0,kind,pc.DB);
		try{
		preprocess.preprocess(pc,0);
	    LDA lDA = new LDA(fileName,pc.LDAPath,preprocess);
	    LDA_Model lda_model = new LDA_Model(pc.TWPath,pc.DTPath);
	    if(pc.DB==null){
			switch(pc.Kind){
			case "Content_Keyphrase_Extract" :
				System.out.println("jhjhj");
				ctpr = new C_TPR(fileName, preprocess,lda_model);
				ctpr.export();
				break;
			case "Paragraph_Keyphrase_Extract" :
				ptpr = new P_TPR(fileName, preprocess,lda_model);
				ptpr.export();
				break;
		    case "Both_Keyphrase_Extract" :
			    ctpr = new C_TPR(fileName, preprocess,lda_model);
			    ctpr.export();
			    ptpr = new P_TPR(fileName, preprocess,lda_model);
			    ptpr.export();
			    break;
		    case "ConceptMap" :
		    	ctpr = new C_TPR(fileName, preprocess,lda_model);
			    ptpr = new P_TPR(fileName, preprocess,lda_model);
			    this.concept1 = ctpr.getConceptMap_Cpreprocess();
			    this.concept2 = ptpr.getConceptMap_Ppreprocess();
			    Cut_Paragraph();
				DrawConcept();
			    break;
		    case "Summary" :
			    break;
			}
		}else{//目前wecan部分只開放線上產生概念圖
			ctpr = new C_TPR(fileName, preprocess,lda_model);
		    ptpr = new P_TPR(fileName, preprocess,lda_model);
		    this.concept1 = ctpr.getConceptMap_Cpreprocess();
		    this.concept2 = ptpr.getConceptMap_Ppreprocess();
		    Cut_Paragraph();
			DrawConcept();
			
		}
	    return true;
		}catch (Exception ex) {
			 return false;
		}
	}
}
