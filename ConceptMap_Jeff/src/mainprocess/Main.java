package mainprocess;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import mainprocess.ProcessCommand;
import mainprocess.SemanticProcess;
import mainprocess.TopicProcess;
import object.Read_object;

import org.xml.sax.SAXException;

import result_evaluation.Evaluation_DUCPerformance;
import result_evaluation.Evaluation_HulPerformance;
import result_evaluation.Evaluation_PAOPerformance;
import summary.document_summary;

import com.lowagie.text.DocumentException;

import Preprocess.SentenceDetection;
import Preprocess.dataPreprocess;
import Preprocess.readFile;
import Topic_MethodMenu.C_TPR;
import Topic_MethodMenu.P_TPR;
import db_connect.sqlObject;

public class Main {
	    public static double startTime;
		public static double endTime;
		public static double totTime;
		public static void main(String[] args) throws IOException,
				ClassNotFoundException, RowsExceededException, WriteException,
				XPathExpressionException, SAXException,
				ParserConfigurationException, SQLException, DocumentException, InterruptedException {
			
			ProcessCommand pc = new ProcessCommand();
			String command = "";
			Scanner scanner = new Scanner(System.in);
			
			System.out.println("請輸入產出：");
			System.out.println("1-->ConceptMap。\n2-->全文關鍵詞。\n3-->段落關鍵詞。\n4-->產出Summary 並存在DB中。");
		    command = scanner.next();
			
			
			startTime = System.currentTimeMillis();// 取得目前時間
			
			if (command.equals("1")) {//讀取檔案夾內文章產生概念圖
				
				
				//讀檔
				String FileName = "Finding His Calling";
				pc.FileName = FileName;
				pc.Source = "readfile";//ex: readfile or Panorama or readOnline
				pc.Kind = "ConceptMap";//ex: Content_keyphrase or Paragraph_Keyphrase_Extract or..
				pc.ConceptMapPath = "D:\\ConceptMap";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
				TopicProcess tp = new TopicProcess();
				tp.process(pc, 0, 0, 0);
				
				
				//線上
				/*String FileName = "readOnline_article";
				sqlObject sqlobj = new sqlObject();
				ArrayList<String>paragraph = new ArrayList<String>();
				paragraph.add("Inside a small motor home, Joanne Pierluissi raised her sleeve as nurse Mary Perez inserted a needle into the vein above her forearm, drawing blood into a tube for a diabetes test.");
				paragraph.add("Large numbers of mainland Chinese on student visas have stayed on to work and acquired rights of residency, and then applied to have their relatives come over in their wake. That is how the population of mainland Chinese in New Zealand has grown over the last several years. In contrast, Taiwanese comprise less than 1% of the total New Zealand population.");
				paragraph.add("According to the New Zealand census, in 2013 there were only 5,715 Taiwanese in New Zealand, 75% of whom lived in the capital of Auckland and its surrounding area. The next largest group, though only 507, lived in the Canterbury region of the South Island, mostly in the island’s largest city of Christchurch. ");
				pc.FileName = FileName;
				pc.online_text = paragraph;
				pc.Source = "readOnline";
				pc.Kind = "ConceptMap";
				pc.ConceptMapPath = "D:\\ConceptMap";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
	            pc.DB = sqlobj;
				
				TopicProcess tp = new TopicProcess();
				System.out.println(tp.process(pc, 0, 0, 0));*/
				
				//光華雜誌
				/*String FileName = "200301062";
				sqlObject sqlobj = new sqlObject();
				pc.FileName = FileName;
				pc.Source = "Panorama";
				pc.Kind = "ConceptMap";
				pc.ConceptMapPath = "D:\\ConceptMap";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
	            pc.DB = sqlobj;
				TopicProcess tp = new TopicProcess();
				System.out.println(tp.process(pc, 0, 0, 0));*/
				
			}
			else if(command.equals("2")){//讀取檔案夾內文章產生全文關鍵字
				
				
				String FileName = "AP890313-0198";
				pc.FileName = FileName;
				pc.Source = "readfile";//ex: readfile or Panorama or readOnline
				pc.Kind = "Content_Keyphrase_Extract";//ex: Content_keyphrase or Paragraph_Keyphrase_Extract or..
				//pc.ConceptMapPath = "D:\\ConceptMap";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
				TopicProcess tp = new TopicProcess();
				tp.process(pc, 0, 0, 0);
				
			}
			else if(command.equals("3")){//讀取檔案夾內文章產生段落關鍵字
				String FileName = "AP890313-0198";
				pc.FileName = FileName;
				pc.Source = "readfile";//ex: readfile or Panorama or readOnline
				pc.Kind = "Paragraph_Keyphrase_Extract";//ex: Content_keyphrase or Paragraph_Keyphrase_Extract or..
				//pc.ConceptMapPath = "D:\\ConceptMap";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
				TopicProcess tp = new TopicProcess();
				tp.process(pc, 0, 0, 0);
			}else if(command.equals("4")){//讀取DB文章形成Summary
				
				sqlObject sqlobj = new sqlObject();
				sqlobj.Ip = "140.125.84.74:3306/corpus";
				sqlobj.User = "root";
				sqlobj.PassWord = "3939889";
				
				String FileName = "200706008";
				pc.FileName = FileName;
				pc.Source = "Panorama";
				pc.Kind = "Summary";
	            pc.DTPath = "D:\\LDA_file\\"+FileName+"_LDA.txt.model-final.THETA";
	            pc.DB = sqlobj;
				SemanticProcess se = new SemanticProcess();
				se.process(pc, 0, 0, 0); 	
				
			}
			//計算程式執行時間
			endTime = System.currentTimeMillis();
			totTime = endTime - startTime;
			System.out.println();
			System.out.println("Using Time: " + totTime / 1000 + " sec");
			System.out.println("Using Time: " + totTime / 1000 /60);	
		}
		
}

