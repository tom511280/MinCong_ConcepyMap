package mainprocess;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.xml.sax.SAXException;

import summary.summary_reafile;

import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;
import com.lowagie.text.DocumentException;

import db_connect.deleteTable;
import db_connect.insertTable;
import drawConceptMap.conceptMap;
import edu.stanford.nlp.io.EncodingPrintWriter.out;
import NER.demo.Entity;
import Preprocess.SentenceDetection;
import Preprocess.StemWords;
import Preprocess.StopWord;
import Preprocess.dataPreprocess;

public class Process {
    NumberFormat nf = NumberFormat.getInstance();

//    public ProcessCommand pc;
	public String fileName;
	public int paraNum;
	public int secondNum;
	public int thirdNum;
	public String kind;//ex.conceptmap or summary
	public String source;// ex: readfile or Panorama or readOnline
	public boolean title_boolean;
    //以下為ConceptMap會用到的參數
	public ArrayList<Entity> concept1 = new ArrayList<Entity>();
	public ArrayList<String> excelText = new ArrayList<String>();// 存放要匯出excel的內容
	public HashMap<Integer, Integer> sentence_number_hash = new HashMap<Integer, Integer>();// 存放每段的句數
	public dataPreprocess preprocess = new dataPreprocess();
	public SentenceDetection sentencedetection = new SentenceDetection();
	public ArrayList<ArrayList<Entity>> concept2 = new ArrayList<ArrayList<Entity>>();
	public ArrayList<ArrayList<Entity>> concept3 = new ArrayList<ArrayList<Entity>>();//存放要存入conceptMap的段落的關鍵詞
	public ArrayList<ArrayList<Double>> similarity2 = new ArrayList<ArrayList<Double>>();
	public ArrayList<Integer> import_para = new ArrayList<Integer>();// 存放句子大於三段的段落
	public String ConceptMapPath;

	//以下為Summary會用到的參數
//	public ArrayList<String> sent = new ArrayList<String>();
//	public ArrayList<ArrayList<ArrayList<String>>> allpara_sentenceW = new ArrayList<ArrayList<ArrayList<String>>>();//文章中段落的句子的word
	public ArrayList<ArrayList<String>> allpara_sentenceT = new ArrayList<ArrayList<String>>();//文章中段落的句子的text
    public int keyword_number;//取top幾的關鍵字當成Summary篩選標準
    public ArrayList<String> Skeyword = new ArrayList<String>();//top全文關鍵字
    public ArrayList<HashMap<Integer,Integer>> sentence_TopWord = new ArrayList<HashMap<Integer,Integer>>();//存放每段每句中topword的數量
    public ArrayList<String> para_summary = new ArrayList<String>();//存放Summary相關資訊
    public ArrayList<String> summary_A = new ArrayList<String>();//存放每句摘要 用於輸出txt檔
    public HashMap<Integer,String> summary_H = new HashMap<Integer,String>();//存放每句摘要  用於insert進DB
	
	//將概念新增至DB
	public void ConceptInsert(String fileName,String kind) throws SQLException
	{
		deleteTable delete = new deleteTable();
	    delete.phrase(fileName, kind);
	    insertTable insertPhrase = new insertTable();
	    insertPhrase.phrase(fileName, kind, concept1, concept2, similarity2,import_para);
	}
	//去除少於三句段落
	public void Cut_Paragraph()
	{
		int t;
		if(title_boolean==true)
			t=1;
		else
			t=0;
			
		//ConceptMap的判斷方式:去除少於三句段落
 		if(kind.equals("ConceptMap")){
 	    //取得每段的句子數  並且將大於三句的段落編號存入import_para
 			
 				
 	 	for (int i = t; i < preprocess.getParagraph().size(); i++) {
 	 		if(sentencedetection.getSentence(preprocess.getParagraph().get(i)).size()>=3){
 	 			import_para.add(i);
 	 		}
 	 	}
 	   //取得大於三句段落的概念 並且存入concept3 
 	 	for (int j = t; j < concept2.size(); j++) {
 	 	 	if(import_para.contains(j+1)){
 	 	 		concept3.add(concept2.get(j));
 	 	 	}	
 	 	}
 	 	 	
 	 	}
 		//Summary的判斷方式:去除少於三句段落
 		else{
 			for (int i = t; i < preprocess.getParagraph().size(); i++) {
 	 	 		if(sentencedetection.getSentence(preprocess.getParagraph().get(i)).size()>=3)
 	 	 			import_para.add(i);
 	 	 	}
 		}
	}
	/******************************產生ConceptMap****************************************/
	//產生ConceptMap.pdf
	public void DrawConcept() throws FileNotFoundException{
		System.out.println(ConceptMapPath);
	    try {
			new conceptMap(fileName, ConceptMapPath, import_para, concept1,
					concept3, secondNum, thirdNum);;
		} catch (DocumentException e) {
			
		}
	}
	/******************************產生Summary*******************************************/
	//取得每個段落中的句子並做處理
	public void getSentence() throws ClassNotFoundException, IOException{
		StopWord stopword = new StopWord();
		StemWords stem = new StemWords();
		
		int t;
		if (title_boolean==true)
			t=1;
		else
			t=0;
			
		//int para_count = 0;
		for (int i = t;i < preprocess.getParagraph().size();i++) {//每個段落
			HashMap<Integer, Integer> para_sentence_hash = new HashMap<Integer, Integer>();//段落句子包含多少個topword
			ArrayList<ArrayList<String>> eachpara_sentenceW = new ArrayList<ArrayList<String>>();//每一段經過處理的句子的字
			ArrayList<String> eachpara_sentenceT = new ArrayList<String>();//每一段經過處理的句子的字

			//para_showdoc.add("第" + para_count + "段: " + each_para);
			int sentence_count = 0;
			for (String each_sentence : sentencedetection.getSentence(preprocess.getParagraph().get(i))) {
				/*if (title_boolean==true){//如果有title則第一段不加 因為第一段是title
					if(para_count > 0)
					{
				       eachpara_sentenceW.add(stopword.removeStopWord(preprocess.paraToWord(each_sentence)));//取得段落中句子經過處理的字
				       eachpara_sentenceT.add(each_sentence);//取得段落中句子經過處理的字串
					}
				}
				else
				{*/
					eachpara_sentenceW.add(stopword.removeStopWord(preprocess.paraToWord(each_sentence)));//Word的形式存每一句  一個位置為句中的單字形成的陣列
					eachpara_sentenceT.add(each_sentence);//String的形式存每一句 一個位置為每一句
				//}
				
				int NumberOfWord = 0;//句子中的關鍵字數
				para_sentence_hash.put(sentence_count, 0);//初始化每個句子中的關鍵字數為0
				for (String sentence_word : eachpara_sentenceW.get(sentence_count)) {//計算每個句子中的top關鍵字數
					for (String keyword : Skeyword){
						if (stem.Stem(sentence_word.toLowerCase()).equals(stem.Stem(keyword.toLowerCase())))
								NumberOfWord++;
					}
				}
				para_sentence_hash.put(sentence_count, NumberOfWord);//存放每個句子中前幾名的關鍵字字數
				sentence_count++;
			}
			//para_count++;
			sentence_TopWord.add(para_sentence_hash);
			allpara_sentenceT.add(eachpara_sentenceT);
			
		}
	}
	public void ExtractSummary() throws SQLException{
		System.out.println(allpara_sentenceT.size());
		System.out.println(sentence_TopWord.size());
		
		for(int i = 0;i<allpara_sentenceT.size();i++){
			if(import_para.contains(i)){
				//取出段落中最大權重分數的句子
				
//				int maxKey = (Collections.max(sentence_TopWord.get(i).values()));
				int maxKey = 0;
				
				//找出段落中最多關鍵字的句子
				 int maxValueInMap=(Collections.max(sentence_TopWord.get(i).values()));  // This will return max value in the Hashmap
			        for (Entry<Integer, Integer> entry : sentence_TopWord.get(i).entrySet()) {  // Itrate through hashmap
			            if (entry.getValue()==maxValueInMap) {
			            	maxKey = entry.getKey();     // Print the key with max value
			            }
			        }
			        
				System.out.println("第 "+ i +  "段,"+ "共"+allpara_sentenceT.get(i).size()+"句, " + "大於3句, 故此段取摘要 "+"取第"+maxKey+"句");
				para_summary.add("第 "+ i +  "段,"+ "共"+allpara_sentenceT.get(i).size()+"句, " + "大於3句, 故此段取摘要 "+"取第"+maxKey+"句");
				summary_H.put(i, allpara_sentenceT.get(i).get(maxKey));
				summary_A.add(allpara_sentenceT.get(i).get(maxKey));
			
			}
			else{
				para_summary.add("第 "+ i +  "段,"+ "共"+allpara_sentenceT.get(i).size()+ "句, " + "小於3句, 故此段不取摘要");
				System.out.println("第 "+ i +  "段,"+ "共"+allpara_sentenceT.get(i).size()+ "句, " + "小於3句, 故此段不取摘要");
			}
		}
		deleteTable delete = new deleteTable();
	    delete.phrase(fileName, source);
	    insertTable insertPhrase = new insertTable();
	    insertPhrase.summary_dbinsert(fileName, source, para_summary, summary_H,preprocess.getParagraph().get(0));
		
		
}
	public void SummaryOutput(String path) throws IOException{
		summary_reafile output = new summary_reafile();
		output.output_summary(path+fileName, String.valueOf(keyword_number),para_summary);
		output.output_summary(path+fileName, "paragraph", summary_A);
//		output.output_summary(fileName, "sentence", sent);
//		output.output_summary(fileName, "keyword_list", keyword_list_showdoc);
	}

}
