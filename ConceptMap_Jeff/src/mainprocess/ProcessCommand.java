package mainprocess;

import java.util.ArrayList;

import db_connect.sqlObject;


//此類別用來存放各種執行程式時的命令
public class ProcessCommand {
	public ArrayList<String>online_text = new ArrayList<String>();
	public String FileName;
	public String FilePath = "RelatedDocuments\\document_file";
	public String LDAPath = "D:\\LDA_file";
	public String Source = "readfile";//ex: readfile or DB
	public sqlObject DB;
	public String Kind = "ConceptMap";//ex: Content_keyphrase
	public boolean title_boolean = false;
	public String ConceptMapPath = "RelatedDocuments\\"+"ConceptMap";
	public String TWPath = "D:\\LDA_file\\model-final.twords";
	public String DTPath;
}
