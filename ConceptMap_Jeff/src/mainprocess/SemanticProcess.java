package mainprocess;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import db_connect.sqlObject;
import NER.demo.Entity;
import Preprocess.SortObject;
import Topic_MethodMenu.C_TPR;
import Topic_MethodMenu.P_TPR;
import factory.phraseWeight;
import factory.phraseWeightParagraph;
import factory.termWeight;
import factory.termWeightParagraph;

public class SemanticProcess extends Process{
	
	public boolean process(ProcessCommand pc, int paraNum, int secondNum,
			int thirdNum) throws ClassNotFoundException, XPathExpressionException, IOException, SAXException, ParserConfigurationException, SQLException{
		this.paraNum = paraNum;
		this.secondNum = secondNum;
		this.thirdNum = thirdNum;
		fileName = pc.FileName;
		source = pc.Source;
		kind = pc.Kind;
		title_boolean = pc.title_boolean;
		
		
		//try{
			switch(pc.Kind){
			case "Summary" :
				preprocess.preprocess(pc,0);
				Cut_Paragraph();
				CenterKeyword();
				getSentence();
				ExtractSummary();
				break;
		    case "ConceptMap" :
				preprocess.preprocess(pc,0);
				CenterIdea();
				ParagraphIdea();
				Cut_Paragraph();
				DrawConcept();
			    break;
			}
		//}catch (Exception ex) {
			 //return false;
		//}
			return true;
	}
//	public boolean process(ProcessCommand pc, int word_number) throws ClassNotFoundException, XPathExpressionException, IOException, SAXException, ParserConfigurationException, SQLException{
//		
//		return title_boolean;
//	}
	//使用建構子給定參數並實作(ConceptMap專用)
//	public SemanticProcess(ProcessCommand pc, int paraNum, int secondNum,
//			int thirdNum) throws ClassNotFoundException, XPathExpressionException, IOException, SAXException, ParserConfigurationException, SQLException{
//		this.paraNum = paraNum;
//		this.secondNum = secondNum;
//		this.thirdNum = thirdNum;
//		
//		fileName = pc.FileName;
//		source = pc.Source;
//		title_boolean = pc.title_boolean;
//		preprocess.preprocess(pc,0);
//		CenterIdea();
//		ParagraphIdea();
//		Cut_Paragraph();
//		DrawConcept();
//		
//	}
	//使用建構子給定參數並實作(Summary專用)
//	public SemanticProcess(ProcessCommand pc ,int keyword_number) throws ClassNotFoundException, IOException, XPathExpressionException, SAXException, ParserConfigurationException, SQLException{
//
//		fileName = pc.FileName;
//		source = pc.Source;
//		title_boolean = pc.title_boolean;
//		preprocess.preprocess(pc,0);
//		Cut_Paragraph();
//		CenterKeyword();
//		getSentence();
//		ExtractSummary();
//		
////		SummaryOutput("RelatedDocuments\\Summary\\");
//	}
	/******************************Summary*************************************/
	private void CenterKeyword()
	{
		/******************************CenterIdea*************************************/
		// 選擇計算term weight的方法
		termWeight tw = new termWeight(1, preprocess);
		ArrayList<Double> termScore = tw.getScore();

		// 將單一關鍵字依分數進行排序
		ArrayList<SortObject> List2 = new ArrayList<SortObject>();
		for (int i = 0; i < preprocess.getArticleWord_hash().size(); i++) {
			List2.add(new SortObject(preprocess.getArticleWord_hash().get(i),
					termScore.get(i)));
		}
		Collections.sort(List2, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
		int word_count = 0;
		for (SortObject object : List2) {
//			object.show();//顯示排序結果
			Skeyword.add(object.obj);
			word_count++;
			if (keyword_number == (word_count+1))
				break;
		
		}
	 }
	 /******************************ConceptMap*************************************/
	 private void CenterIdea()
	 {
			// 選擇計算term weight的方法
			termWeight tw = new termWeight(2, preprocess);
			ArrayList<Double> termScore = tw.getScore();

			// 將單一關鍵字依分數進行排序
			ArrayList<SortObject> List2 = new ArrayList<SortObject>();
			for (int i = 0; i < preprocess.getArticleWord_hash().size(); i++) {
				List2.add(new SortObject(preprocess.getArticleWord_hash().get(i),
						termScore.get(i)));
			}
			Collections.sort(List2, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
			ArrayList<String> keyword = new ArrayList<String>();
			ArrayList<Double> similarity = new ArrayList<Double>();
			for (SortObject object : List2) {
//				object.show();//顯示排序結果
				excelText.add(object.obj + "@@" + object.value);
				keyword.add(object.obj);
				similarity.add(object.value);
			}
//		 選擇計算phrase weight的方法
		phraseWeight pw = new phraseWeight(1, preprocess, termScore);
		Entity[] phrases = pw.getPhrase();
		ArrayList<Double> phraseScore1 = pw.getScore();

		// 將第一層的phrase依分數進行排序
		List2.clear();
		excelText.clear();
		for (int i = 0; i < phrases.length; i++) {
			List2.add(new SortObject(phrases[i], phraseScore1.get(i)));
		}
		ArrayList<String> keyphrase = new ArrayList<String>();
		Collections.sort(List2, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
		for (SortObject object : List2) {
//		    object.show();//顯示排序結果
			excelText.add(object.entity.entity + " : " + object.value);
			keyphrase.add(object.entity.entity);
//			System.out.print(object.entity.entity + " : " + object.value);
		}
		concept1.add(List2.get(0).entity);// 第1層main idea

    }
	 public void ParagraphIdea(){
		 /******************************ParagraphIdea*************************************/
			termWeightParagraph twp = new termWeightParagraph(1, preprocess);
			ArrayList<ArrayList<Double>> termScore2 = twp.getScore();
	//
			
			// 選擇計算各段落phrase weight的方法
			phraseWeightParagraph pwp = new phraseWeightParagraph(1, preprocess,
					termScore2);
			ArrayList<ArrayList<Double>> phraseScore2 = pwp.getScore();
	//
			excelText.clear();
			// 將單一關鍵字依分數進行排序
			for (int i = 0; i < termScore2.size(); i++) {
				ArrayList<String> secondKeyword = new ArrayList<String>();
				ArrayList<SortObject> List = new ArrayList<SortObject>();
				for (int j = 0; j < termScore2.get(i).size(); j++) {
					List.add(new SortObject(preprocess.getSentenceToWord_hash()
							.get(i).get(j), termScore2.get(i).get(j)));
				}
				Collections.sort(List, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
				for (SortObject object : List) {
					// object.show();//顯示排序結果
					excelText.add(object.obj + "@@" + object.value);
					secondKeyword.add(object.obj);
				}
				if (i < termScore2.size() - 1) {
					excelText.add("" + "@@" + "");
					excelText.add("第" + (i + 1) + "段");
				}
			}

			excelText.clear();
			// 將phrase依分數進行排序
			int noTitle = 0;// 針對摘要資料集若沒有title,i則從0開始
			if (phraseScore2.size() == 1)
				noTitle = -1;
			for (int i = 1 + noTitle; i < phraseScore2.size(); i++) {
				ArrayList<String> secondKeyphrase = new ArrayList<String>();
				ArrayList<SortObject> List = new ArrayList<SortObject>();
				ArrayList<Entity> concept = new ArrayList<Entity>();
				ArrayList<Double> psimilarity = new ArrayList<Double>();
				for (int j = 0; j < phraseScore2.get(i).size(); j++) {
					if (!preprocess.getPhrase_ner().get(i).get(j).equals(" ")) {
						List.add(new SortObject(preprocess.getPhrase_ner().get(i)
								.get(j), phraseScore2.get(i).get(j)));
					}
				}
				Collections.sort(List, SortObject.Comparators.VALUE);// 依照value(權重)進行排序
				for (SortObject object : List) {
					// object.show();//顯示排序結果
					excelText.add(object.entity.entity + "@@" + object.value);
					Entity entity = new Entity(object.entity.entity,
							String.valueOf(object.value));
					// concept.add(object.entity);
					concept.add(entity);
					psimilarity.add(object.value);
					secondKeyphrase.add(object.entity.entity);
//					System.out.println(object.entity.entity + "@@" + object.value);
				}
				if (i < phraseScore2.size() - 1) {
					excelText.add("" + "@@" + "");
					excelText.add("第" + (i + 1) + "段");
				}
				concept2.add(concept);
				similarity2.add(psimilarity);
			}
	 }
}
